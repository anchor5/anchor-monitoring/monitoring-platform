def apply(metric):
  if metric.tags["type"] == "VOL_UL":
    metric.fields["uplink_volume"]=metric.fields["value"]
  if metric.tags["type"] == "VOL_DL":
    metric.fields["downlink_volume"]=metric.fields["value"]
  if metric.tags["type"] == "GTP_U_LOSS":
    metric.fields["packet_loss"]=metric.fields["value"]/100
  if metric.tags["type"] == "RTT":
    metric.fields["round_trip_time"]=metric.fields["value"]
#  if metric.tags["type"] == "PDU_NUM":
#    metric.fields["active_pdu_sessions"]=metric.fields["value"]

#renaming nsiId tag in network_slice_subnet_uid
  metric.tags["network_slice_subnet_uid"]=metric.tags["nsiId"]
  metric.tags.pop("type")
  metric.fields.pop("value")
  return metric
