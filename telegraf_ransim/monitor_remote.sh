#!/bin/sh
while true; do
    echo "inotifywait -mq -e close_write $1 --format '%w%f:%f'" | ssh -i /home/telegraf/.ssh/telegraf -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=ERROR ubuntu@10.5.6.141 /bin/bash |
    while read file
    do
        mkdir -p /home/telegraf/input_files
        chmod -R 0777 /home/telegraf/input_files
        for file_to_copy in "$@"
        do
            scp -i /home/telegraf/.ssh/telegraf -q -o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@10.5.6.141:$file_to_copy /home/telegraf/input_files
        done
        python3 /home/telegraf/plugin.py
    done
done
