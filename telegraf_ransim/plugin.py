# example stdout with data_format influx
# telegraf_ransim_1  | tail,host=telegraf_ransim,indexUE=2,path=/home/telegraf/pipe_network_status remainingContactTime="344",indexgNB="0",distance="1038.01",ElevationAngle="73.24" 1646837708037529202
# telegraf_ransim_1  | tail,host=telegraf_ransim,indexUE=3,path=/home/telegraf/pipe_network_status distance="1073.51",ElevationAngle="66.9816",remainingContactTime="327",indexgNB="0" 1646837708038253586

from os import listdir
from os.path import join
import time

def parse_network_status_file(lines, timestamp):
    for line in lines:
        line = line[:-1] ## to strip \n character
        line_data = line.split(" ")
        if line_data[0].isnumeric():
            measurement = "ransim_network_status"
            if line_data[3] != "-1":
                tags="UE_index={},gNB_index={}".format(line_data[0],line_data[1])
                line_data[0] = float(line_data[0])
                line_data[1] = float(line_data[1])
                line_data[2] = float(line_data[2])
                line_data[3] = float(line_data[3])
                line_data[4] = float(line_data[4])

                fields="distance_UE_gNB={},elevation_angle_UE_gNB={},remaining_contact_time_UE_gNB={}".format(line_data[2],line_data[3],line_data[4])
                print(f"{measurement},{tags} {fields} {timestamp}",flush=True)


def parse_network_configuration_file(lines, timestamp):
    for line in lines:
        key, value = line.split(" ", 1) # separates the first word from the value
        value = value.rstrip()
        measurement = "ransim_network_configuration"
        if key == "numgNBs":
            ## try,except in case of Exceptions
            value = int(value)
            field = f'{key}={value}'
            influx_line_protocol = f"{measurement} {field} {timestamp}"
            print(influx_line_protocol, flush=True)
        elif key == "numTrafficProfiles":
            ## try,except in case of Exceptions
            value = int(value)
            field = f'{key}={value}'
            influx_line_protocol = f"{measurement} {field} {timestamp}"
            print(influx_line_protocol, flush=True)
        elif key == "numUEsPerTrafProf":
            values = value.split(" ")
            counter_ue = 0
            for index, element in enumerate(values):
                tags="traffic_profile={}".format(str(index))
                field=f'{key}={element}'
                influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                print(influx_line_protocol, flush=True)
                counter_ue += int(element)
            print(f"{measurement} totalNumUE={counter_ue} {timestamp}")
        elif key == "numActiveFrequencyBands":
            ## try,except in case of Exceptions
            value = int(value)
            field = f'{key}={value}'
            tags="plugin=/home/telegraf/plugin.py"
            influx_line_protocol = f"{measurement} {field} {timestamp}"
            print(influx_line_protocol, flush=True)
        elif key == "centralFrequencyBands":
            values = value.split(" ")
            for index, element in enumerate(values):
                tags="active_frequency_band={}".format(index)
                field=f'{key}={element}'
                influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                print(influx_line_protocol, flush=True)
        elif key == "bandwidthBands":
            values = value.split(" ")
            for index, element in enumerate(values):
                tags="active_frequency_band={}".format(index)
                field=f'{key}={element}'
                influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                print(influx_line_protocol, flush=True)
        elif key == "numCCsPerBand":
            numCcPerAfb = value.split(" ")
            for index, element in enumerate(numCcPerAfb):
                tags="active_frequency_band={}".format(index)
                field=f'{key}={element}'
                influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                print(influx_line_protocol, flush=True)
        elif key == "centralFrequencyCCs":
            values = value.split(" ")
            for indexAfb, numberCc in enumerate(numCcPerAfb):
                for indexCc in range(int(numCcPerAfb[indexAfb])):
                    tags="active_frequency_band={},component_carrier_index={}".format(indexAfb,indexCc)
                    central_frequency = values.pop(0)
                    field=f'{key}={central_frequency}'
                    influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                    print(influx_line_protocol, flush=True)
        elif key == "bandwidthCCs":
            values = value.split(" ")
            for indexAfb, numberCc in enumerate(numCcPerAfb):
                for indexCc in range(int(numCcPerAfb[indexAfb])):
                    tags="active_frequency_band={},component_carrier_index={}".format(indexAfb,indexCc)
                    central_frequency = values.pop(0)
                    field=f'{key}={central_frequency}'
                    influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                    print(influx_line_protocol, flush=True)
        elif key == "numBWPsPerCC":
            numBwpPerCc = value.split(" ")
            values = value.split(" ")
            for indexAfb, numberCc in enumerate(numCcPerAfb):
                for indexCc in range(int(numCcPerAfb[indexAfb])):
                    tags="active_frequency_band={},component_carrier_index={}".format(indexAfb,indexCc)
                    num_bwp = values.pop(0)
                    field=f'{key}={num_bwp}'
                    influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                    print(influx_line_protocol, flush=True)
        elif key == "centralFrequencyBWPs":
            values = value.split(" ")
            num_bwp_per_cc = [i for i in numBwpPerCc] ## need to deep copy
            for index_afb, num_cc_in_afb in enumerate(numCcPerAfb):
                for index_cc_in_afb in range(int(num_cc_in_afb)):
                    num_bwp_in_cc = num_bwp_per_cc.pop(0)
                    for index_bwp_in_cc in range(int(num_bwp_in_cc)):
                        tags="active_frequency_band={},component_carrier_index={},bandwidth_part_index={}".format(index_afb,index_cc_in_afb,index_bwp_in_cc)    
                        central_frequency = values.pop(0)
                        field=f'{key}={central_frequency}'
                        influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                        print(influx_line_protocol, flush=True)
        elif key == "bandwidthBWPs":
            values = value.split(" ")
            num_bwp_per_cc = [i for i in numBwpPerCc] ## need to deep copy
            for index_afb, num_cc_in_afb in enumerate(numCcPerAfb):
                for index_cc_in_afb in range(int(num_cc_in_afb)):
                    num_bwp_in_cc = num_bwp_per_cc.pop(0)
                    for index_bwp_in_cc in range(int(num_bwp_in_cc)):
                        tags="active_frequency_band={},component_carrier_index={},bandwidth_part_index={}".format(index_afb,index_cc_in_afb,index_bwp_in_cc)    
                        bandwidth = values.pop(0)
                        field=f'{key}={bandwidth}'
                        influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                        print(influx_line_protocol, flush=True)
        elif key == "numerologyBWPs":
            values = value.split(" ")
            num_bwp_per_cc = numBwpPerCc
            for index_afb, num_cc_in_afb in enumerate(numCcPerAfb):
                for index_cc_in_afb in range(int(num_cc_in_afb)):
                    num_bwp_in_cc = num_bwp_per_cc.pop(0)
                    for index_bwp_in_cc in range(int(num_bwp_in_cc)):
                        tags="active_frequency_band={},component_carrier_index={},bandwidth_part_index={}".format(index_afb,index_cc_in_afb,index_bwp_in_cc, flush=True)    
                        numerology = values.pop(0)
                        field=f'{key}={numerology}'
                        influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                        print(influx_line_protocol, flush=True)
        else:
            print("Error in parsing network configuration file, unknown key")


def parse_resource_allocation_file(lines, timestamp):
    for line in lines:
        line = line[:-1] ## to strip \n character
        line_data = line.split("\t")
        if line_data[0].isnumeric():
            measurement = "ransim_resource_configuration"
            if len(line_data) > 3: ## 5 is just in case traffic profile cardinality is 4 (need to be passed from command line interface) --> need to check if TF cardinality can be 2 or it is always > 2
                indexgNB = int(line_data[0])
                for indexTrafProf, numberAttachedUEPerTrafProf in enumerate(line_data[1:]):
                    value = int(numberAttachedUEPerTrafProf)
                    field = f'number_attached_UE={value}'
                    tags="traffic_profile_index={},gNB_index={}".format(indexTrafProf,indexgNB)
                    influx_line_protocol = f"{measurement},{tags} {field} {timestamp}"
                    print(influx_line_protocol, flush=True)
            else:
                tags="UE_index={}".format(line_data[0])
                print(f"{measurement},{tags} UE_traffic_profile={line_data[1]} {timestamp}")
                print(f"{measurement},{tags} gNB_to_attach={line_data[2]} {timestamp}")


def parse_ue_positioning_file(lines, timestamp):
    for line in lines:
        line = line[:-1] ## to strip \n character
        line_data = line.split(" ")
        if line_data[0].isnumeric():
            measurement = "ransim_ue_positioning"
            tags="UE_index={}".format(int(line_data[0]))
            fields="latitude={},longitude={},altitude={}".format(float(line_data[1]),float(line_data[2]),float(line_data[3]))
            print(f"{measurement},{tags} {fields} {timestamp}")


def parse_gnb_positioning_file(lines, timestamp):
    for line in lines:
        line = line[:-1] ## to strip \n character
        line_data = line.split(" ")
        if line_data[0].isnumeric():
            measurement = "ransim_gnb_positioning"
            tags="gNB_index={}".format(int(line_data[0]))
            fields="position_x={},position_y={},position_z={}".format(float(line_data[1]),float(line_data[2]),float(line_data[3]))
            print(f"{measurement},{tags} {fields} {timestamp}")

def produce_average_delivery_time_per_traffic_flow(line):
    line = line[:-2] ## to strip final " \n"
    line_data = line.split(" ")
    for index,datum in enumerate(line_data):
        measurement = "ransim_performance_results"
        tags="UE_index={}".format(index)
        fields="avg_delivery_time={}".format(float(datum))
        print(f"{measurement},{tags} {fields} {timestamp}")

def produce_average_jitter_per_traffic_flow(line):
    line = line[:-2] ## to strip final " \n"
    line_data = line.split(" ")
    for index,datum in enumerate(line_data):
        measurement = "ransim_performance_results"
        tags="UE_index={}".format(index)
        fields="avg_jitter={}".format(float(datum))
        print(f"{measurement},{tags} {fields} {timestamp}")

def produce_aggregated_throughput_per_satellite_channel(line):
    line = line[:-2] ## to strip final " \n"
    line_data = line.split(" ")
    for index,datum in enumerate(line_data):
        measurement = "ransim_performance_results"
        tags="gNB_index={}".format(index)
        fields="aggregated_throughput={}".format(float(datum))
        print(f"{measurement},{tags} {fields} {timestamp}")

def produce_cell_occupancy_per_satellite(line):
    line = line[:-2] ## to strip final " \n"
    line_data = line.split(" ")
    for index,datum in enumerate(line_data):
        measurement = "ransim_performance_results"
        tags="gNB_index={}".format(index)
        fields="cell_occupancy={}".format(float(datum))
        print(f"{measurement},{tags} {fields} {timestamp}")

def produce_cell_occupancy_per_satellite_per_carrier(lines):
    for active_frequency_band,line in enumerate(lines):
        line = line[:-2] ## to strip final " \n"
        line_data = line.split(" ")
        for index_gNB,datum in enumerate(line_data):
            measurement = "ransim_performance_results"
            tags="active_frequency_band={},gNB_index={}".format(active_frequency_band,index_gNB)
            fields="cell_occupancy={}".format(float(datum))
            print(f"{measurement},{tags} {fields} {timestamp}")

def parse_performance_results_file(lines,timestamp):
    number_of_lines=len(lines)
    produce_average_delivery_time_per_traffic_flow(lines[1])
    print("ransim_performance_results average_delivery_time={} {}".format(float(lines[3]),timestamp))
    produce_average_jitter_per_traffic_flow(lines[5])
    print("ransim_performance_results avg_jitter={} {}".format(float(lines[7]),timestamp))
    produce_aggregated_throughput_per_satellite_channel(lines[9])
    print("ransim_performance_results aggregated_throughput={} {}".format(float(lines[11]),timestamp))
    produce_cell_occupancy_per_satellite_per_carrier(lines[13:number_of_lines-4])
    produce_cell_occupancy_per_satellite(lines[number_of_lines-3])
    print("ransim_performance_results number_of_handovers={} {}".format(int(lines[number_of_lines-1]),timestamp))


if __name__ ==  "__main__":
    
    timestamp = int(float(time.time() * 10**9))
    
    path = "/home/telegraf/input_files"
    
    for filename in listdir(path):
        file = join(path, filename)
        timestamp = round(time.time_ns())
        with open(file,"r") as f:
            if file.endswith("networkConfiguration.txt"):
                lines = f.readlines() ## lines is a list of strings terminating each one with /n character
                parse_network_configuration_file(lines, timestamp)
            elif file.endswith("networkStatus.txt"):
                lines = f.readlines() ## lines is a list of strings terminating each one with /n character
                parse_network_status_file(lines, timestamp)
            elif file.endswith("uePositions.txt"):
                lines = f.readlines() ## lines is a list of strings terminating each one with /n character
                parse_ue_positioning_file(lines, timestamp)
            elif file.endswith("resourceAllocationPrevious.txt"):
                lines = f.readlines() ## lines is a list of strings terminating each one with /n character
                parse_resource_allocation_file(lines, timestamp)
            elif file.endswith("satellitePositions.txt"): ## gnb positioning file
                lines = f.readlines() ## lines is a list of strings terminating each one with /n character
                parse_gnb_positioning_file(lines, timestamp)
            else:
                lines = f.readlines() ## lines is a list of strings terminating each one with /n character
                parse_performance_results_file(lines,timestamp)