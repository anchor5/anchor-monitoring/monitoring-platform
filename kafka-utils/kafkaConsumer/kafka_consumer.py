import kafka
import time
import sys
from colorama import Fore, Style

'''
NOTE:
the script exects 2 CLI args:
  - the first one is the kafka host ip address: the value can be 'kafka' in the case in which the script is executed inside the kafka_consumer service in the multi-container docker application 
    of the ANChOR Monitoring Platform, otherwise it must be the Monitoring Platform VM IP address, e.g. 10.5.6.148 in the ANChOR testbed
  - the second one is the kafka exposed port: since inside the kafka docker service we defined two KAFKA_ADVERTISED_LISTENERS (INSIDE://kafka:9092,OUTSIDE://10.5.6.148:9093, see https://www.kaaiot.com/blog/kafka-docker for the theory)
    the value must be 9092 in the case the script is executed inside the kafka_consumer service in the multi-container docker application of the ANChOR Monitoring Platform, otherwise it must be 9093

Summarizing:
to run this script from outside the multi-container Docker application of the ANChOR Monitoring Platform:
    python3 kafka_consumer.py <monitoring_platform_host_ip> 9093

to run this script from inside the kafka_consumer container inside the ANChOR Monitoring Platform:

    docker exec -it <kafka_consumer_container_id> /bin/sh
    python3 kafka_consumer.py kafka 9092
'''

def main():

# the first argument can be 'kafka' in the case the script is executed inside a docker_container, otherwise must be the monitoring platform ip
  monitoring_platform_ip = sys.argv[1]
# the second argument must be 9092 in the case the script is executed inside a docker_container. otherwise it must be 9093
  kafka_exposed_port = sys.argv[2]

# Consumer to retrieve the list of topics
  topicConsumer = kafka.KafkaConsumer(group_id=None, bootstrap_servers=['{}:{}'.format(monitoring_platform_ip, kafka_exposed_port)], auto_offset_reset='earliest')
# the topics() method returns the set of the topic which the user is authorized to view
  print(Fore.GREEN + "\n*** KAFKA TOPICS ***", flush=True)

  topics = topicConsumer.topics()
  print(Style.RESET_ALL)

  for item in topics:
    print(item, end="\n")

  topic = input("\nEnter the topic to subscribe to: ")

# NOTE: one consumer per topic!
  consumer = kafka.KafkaConsumer(topic, group_id=None, bootstrap_servers=['{}:{}'.format(monitoring_platform_ip, kafka_exposed_port)], auto_offset_reset='earliest')

  print(Fore.GREEN + "\n*** KAFKA MESSAGES ***", flush=True)
  print(Style.RESET_ALL)
  while True:
      time.sleep(2)
      for msg in consumer:
          print(msg, flush=True)

if __name__ == '__main__':
    main()
