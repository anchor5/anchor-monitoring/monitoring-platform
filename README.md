# Monitoring Platform
![](docs/monitoring.png)

## Prerequisites
In order to run the Monitoring Platform the following software must be installed on the hosting machine:
 - docker

Once these software requirements have been satisfied, the hosting machine must be configured as the Docker Swarm manager node by running the command:
```sh
docker swarm init
```

## How to run the software

To run the monitoring platform (since the Config Manager expects the telegraf Docker services are named "*mp_telegraf_owm*" or "*mp_telegraf_nwdaf*" where *mp* is the name of the Docker stack deployed):
- Clone the repository and enter in the "monitoring-platform" directory
- Build locally the images required to run the monitoring platform services  with the commands:

```sh
docker build -t config-manager config-manager
docker build -t telegraf_ransim telegraf_ransim
```
- Create the directory to contain the data related to InfluxDB, Postgres and Prometheus and the local file to inspect the ConfigManager logs with the following commands:
```sh
mkdir -p data/{postgres,influxdb,prometheus}
sudo touch /var/log/configmanager.log
```
- Pull all the other Monitoring Platform Components images by running the command:
```sh
docker compose pull --ignore-pull-failures
```
- Run the multi Docker-container monitoring-platform application in **swarm-mode** with the command:
```sh
docker stack deploy -c docker-compose.yaml mp
```

The *config-manager* APIs allow to configure the different Telegraf services. These APIs are described at this link [link](https://gitlab.com/anchor5/anchor-interfaces/openapi/-/tree/main/Monitoring-Platform_Configuration-Manager_NBI).

Before starting the RanSim datasource, the host where the UNIGE RAN simulator is running must be able to run the *inotifywait* command in the *inotify-tools* package. The latter can be installed on Ubuntu using the command:
```sh
apt install inotify-tools
```
Moreover the telegraf public key must be registered inside the RAN simulator host machine, in order to allow the bash script running inside the telegraf\_ransim service to execute the *inotifywait* command in ssh while authenticating using the telegraf key. This can be done in Ubuntu by appending the content of the telegraf\_ransim/telegraf.pub file inside the /home/ubuntu/.root/authorized_keys file (the path is related to the user who is logging inside the VM, in this case we are logging as ubuntu user, so the path in which the key must be appended is the one mentioned).

The kafka-utils directory encompasses an example of kafka-consumer written using Python libraries.
The influxdb-utils directory encompasses a Postman collection containing the requests to query the InfluxDB TSDB filtering the data by measurement name. In order to exploit the collection, the working Postman environment must set two variables:
- *InfluxDB_ip* : must be valued with the Monitoring Platform host IP address
- *InfluxDB_port*: must be valued with the Monitoring Platform host port mapped to the port 8086 of the InfluxDB Docker container (by default the port 8086 of the InfluxDB docker container is mapped on port 8086 on the Monitoring Platform host)

### How to access the Monitoring Platform Web User Interfaces
All the Monitoring Platform components provides a Web Interface in order to interact with the data and to check the status of the related component. All these web User Interfaces are available at the Monitoring Platform hosting machine IP address specifying different port number.
 - The InfluxDB web User Interface is reachable on port 8086
 - The Kafka web User Interface is reachable on port 8080
 - The Prometheus web User Interface is reachable in port 9090 
 - The scraping endpoint provided by the telegraf_owm to the Prometheus service is reachable on port 9273
 - The scraping endpoint provided by the telegraf_nwdaf to the Prometheus service is reachable on port 9274
 - The scraping endpoint provided by the telegraf_starfish to the Prometheus service is reachable on port 9275
 - The scraping endpoint provided by the telegraf_ransim to the Prometheus service is reachable on port 9276

As an example, in order to connect to the InfluxDB web User Interface, supposing that the IP address of the Monitoring Platform hosting machine is 1.2.3.4, the user must open a web browser and connect to the URL *http://1.2.3.4:8086*

## How to test the software

In order to test the software and the configuration mechanism with randomly generated metrics, two stubs (one to emulate an NWDAF datasource and one implementing the Starfish datasource) have been created. In order to run the Monitoring Platform test deployment two additional images must be built:
```sh
docker build -t nwdaf_stub nwdaf_stub
docker build -t starfish_stub starfish_stub
```
Once these two images have been created, an ad hoc docker-compose file, named *docker-compose_test.yml* have been created in order to include also two additional services emulating the NWDAF and the Starfish datasources.

The *nwdaf_stub* is just an emulation providing measures about three network slice instances with **nsiIds** equal to 123, 456, 789. The stub is listening on port 5000 and since the stub is running as a docker container the IP address is automatically resolved by Docker providing an IP address equal to *nwdaf_stub*. The related OpenApi is available at this [link](https://gitlab.com/anchor5/anchor-interfaces/openapi/-/blob/main/5G_CN_NWDAF_API/openapi.yaml).

An example of NWDAF stubbed datasource addition request in CURL format is the following (where 1.2.3.4 is the Monitoring Platform hosting machine IP address):
```sh
curl --location --request POST 'http://1.2.3.4:8888/datasources/NWDAF' \
--header 'Content-Type: application/json' \
--data-raw '{
  "end-to-end-network-slice-uid": "966806d5-27f9-4692-b8ab-1c298933cb71",
  "network-slice-subnet-uid": "855795d4-27f9-4692-b8ab-1c298933cb71",
  "api-root": "http://nwdaf_stub:5000",
  "interval": "30s",
  "metrics": ["ul-volume","dl-volume","rtt","pdu-number","pkt-loss"],
  "topic-name": "nwdaf_855795d4-27f9-4692-b8ab-1c298933cb71"
}'
```

The *starfish_stub* is just an emulation of the Starfish platform API built ad-hoc for the ESA ANChOR project. The stub is listening on port 5001 and since the stub is running as a docker container the IP address is automatically resolved by Docker providing an IP address equal to *starfish_stub*. The Starfish Monitoring API is not OpenSource. 

An example of Starfish stubbed datasource addition request in CURL format is the following (where 1.2.3.4 is the Monitoring Platform hosting machine IP address):
```sh
curl --location --request POST 'http://1.2.3.4:8888/datasources/Starfish' \
--header 'Content-Type: application/json' \
--data-raw '{
  "end-to-end-network-slice-uid": "744684d4-27f9-4692-b8ab-1c298933cb71",
  "network-slice-subnet-uid": "411351d0-27f9-4692-b8ab-1c298933cb71",
  "api-root": "http://starfish_stub:5001/networkslice",
  "interval": "10s",
  "metrics": ["traffic_counters","traffic_formats","kpis"],
  "topic-name": "starfish_411351d0-27f9-4692-b8ab-1c298933cb71"
}'
```

The command to start the Monitoring Platform test deployment is the following:
```sh
docker stack deploy -c docker-compose_test.yml mp
```