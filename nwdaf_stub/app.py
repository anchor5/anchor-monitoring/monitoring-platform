from flask import Flask, request, jsonify
import random

app = Flask(__name__)

# code to emulate NITEL NWDAF behavior, the NITEL NWDAF exposes an interface not totally 3GPP compliant,
#@app.route('/nnwaf_analyticsinfo/v1/analytics',methods=['GET'])
@app.route('/analytics',methods=['GET'])
def get_analytics_NWDAF():
      args = request.args
      nsiId = args.get("nsiId")
      data = [
            {
                  "timeStamp": "2022-01-14T08:58:57.018Z",
                  "measures": [
                        {
                              "type": "RTT",
                              "value": random.randint(0,5)+random.random(),
                              "unit": "milliseconds"
                        },
                        {
                              "type": "VOL_UL",
                              "value": random.randint(100,200),
                              "unit": "Mbps"
                        },
                        {
                              "type": "VOL_DL",
                              "value": random.randint(50,150),
                              "unit": "Mbps"
                        },
                        {
                              "type": "GTP_U_LOSS",
                              "value": random.randint(0,100),
                              "unit": "%"
                        }
                  ],
                  "nsiId": nsiId
              }
      ]
      return jsonify(data)