#!/bin/bash
cp config/telegraf_owm/telegraf.conf.ini config/telegraf_owm/telegraf.conf
cp config/telegraf_nwdaf/telegraf.conf.ini config/telegraf_nwdaf/telegraf.conf
cp config/telegraf_ransim/telegraf.conf.ini config/telegraf_ransim/telegraf.conf
cp config/telegraf_starfish/telegraf.conf.ini config/telegraf_starfish/telegraf.conf
rm -r data/influxdb/* 
rm -r data/prometheus/*  
rm -r data/postgres/*
rm -r config/influxdb/* 
rm -r config/prometheus/host* 
rm -r config/prometheus/resolv.conf
