import connexion
import six
import uuid

from swagger_server.models.gateway import Gateway  # noqa: E501
from swagger_server.models.http_validation_error import HTTPValidationError  # noqa: E501
from swagger_server.models.network_slice import NetworkSlice  # noqa: E501
from swagger_server.models.new_network_slice import NewNetworkSlice  # noqa: E501
from swagger_server import util


def create_network_slice_platform_networkslice_post(body):  # noqa: E501
    """Create a network slice

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: Object
    """
    if connexion.request.is_json:
        body = NewNetworkSlice.from_dict(connexion.request.get_json())  # noqa: E501
        print("Network Slice Creation Body:\n"+ str(body),flush=True)
    return str(uuid.uuid4())


def delete_network_slice_platform_networkslice_delete(slice_uid):  # noqa: E501
    """Delete a network slice

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str

    :rtype: None
    """

    print("Requested deletion for slice = {}".format(slice_uid))

    return 'do some magic!'


def gateways_platform_gateways_get():  # noqa: E501
    """List gateways

     # noqa: E501


    :rtype: List[Gateway]
    """
    gateway0=Gateway.from_dict({"name": "gateway#0", "latitude": 40, "longitude": 10})
    gateway1=Gateway.from_dict({"name": "gateway#1","latitude": 40.01,"longitude": 10.01})

    return [gateway0,gateway1]


def network_slices_platform_networkslices_get():  # noqa: E501
    """List network slices

     # noqa: E501


    :rtype: List[NetworkSlice]
    """
    slice_1=NetworkSlice.from_dict({"name": "NS_Gold","quality": "gold","subnet_ip": "198.51.100.41","gateway_ip": "198.51.100.42","slice_uid": "5ba20b87-0ee6-47a3-a846-64ba2280ee82","creation_utc": "2022-06-21T12:04:34.020720"})
    slice_2=NetworkSlice.from_dict({"name": "NS_Silver","quality": "silver","subnet_ip": "198.51.100.43","gateway_ip": "198.51.100.44","slice_uid": "8b88d6ee-0e8e-4cad-98be-0edaee8b2942","creation_utc": "2022-06-21T12:16:36.943035"})

    return [slice_1,slice_2]

