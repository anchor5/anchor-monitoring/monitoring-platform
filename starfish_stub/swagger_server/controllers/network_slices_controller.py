import connexion
import six

from swagger_server.models.http_validation_error import HTTPValidationError  # noqa: E501
from swagger_server.models.kpis import Kpis  # noqa: E501
from swagger_server.models.network_slice_details import NetworkSliceDetails  # noqa: E501
from swagger_server.models.traffic_counters import TrafficCounters  # noqa: E501
from swagger_server.models.traffic_formats import TrafficFormats  # noqa: E501
from swagger_server import util
from flask import jsonify

import random


def details_networkslice_details_get(slice_uid):  # noqa: E501
    """Details

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str

    :rtype: NetworkSliceDetails
    """

    network_slice_details_dict = {}
    index=random.randint(0,1)
    network_slice_details_dict = { "gateway": "gateway#{}".format(str(index)), "terminal_id": "50:ed:94:00:15:4{}".format(str(index)) }
    
    return network_slice_details_dict


def get_modcod_networkslice_modcod_get(slice_uid):  # noqa: E501
    """Current modcod

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str

    :rtype: Object
    """
    modcod_list=['QPSK 1/2','QPSK 1/4', 'QPSK 5/6', '16APSK 3/4']
    index= random.randint(0,3)
    return modcod_list[index]


def kpis_networkslice_kpis_get(slice_uid, minutes=None):  # noqa: E501
    """KPIs

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str
    :param minutes: 
    :type minutes: int

    :rtype: Kpis
    """
    data = {
            "forward_snr": {
                  "min": random.randint(0,5),
                  "avg": random.randint(7,10),
                  "max": random.randint(11,15)
            },
            "return_snr": {
                  "min": random.randint(0,5),
                  "avg": random.randint(15,18),
                  "max": random.randint(20,22)
            },
            "return_ber": {
                  "min": random.randint(0,5),
                  "avg": random.randint(23,27),
                  "max": random.randint(30,35)
            },
            "return_frequency_error": {
                  "min": random.randint(0,5),
                  "avg": random.randint(4,6),
                  "max": random.randint(9,11)
            }
      }
    return jsonify(data)


def migrate_networkslice_migrate_put(slice_uid, gateway_name):  # noqa: E501
    """Migrate to a gateway

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str
    :param gateway_name: 
    :type gateway_name: str

    :rtype: None
    """
    return 'do some magic!'


def set_modcod_networkslice_modcod_put(slice_uid, modcod):  # noqa: E501
    """Change modcod

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str
    :param modcod: 
    :type modcod: str

    :rtype: None
    """
    return 'do some magic!'


def traffic_counters_networkslice_traffic_counters_get(slice_uid, minutes=None):  # noqa: E501
    """Traffic counters

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str
    :param minutes: 
    :type minutes: int

    :rtype: TrafficCounters
    """
    data = {
            "return_bytes": random.randint(800,1200),
            "forward_bytes": random.randint(300,700)
            }
    return jsonify(data)


def traffic_formats_networkslice_traffic_formats_get(slice_uid, minutes=None):  # noqa: E501
    """Traffic formats

     # noqa: E501

    :param slice_uid: 
    :type slice_uid: str
    :param minutes: 
    :type minutes: int

    :rtype: List[TrafficFormats]
    """
    data = [
            {
                  "direction": "forward",
                  "format": "format_fw",
                  "packets": random.randint(5,15),
                  "volume": random.randint(800,1200)
            },
            {
                  "direction": "return",
                  "format": "format_ret",
                  "packets": random.randint(15,25),
                  "volume": random.randint(3800,4200)
            }
    ]
    return jsonify(data)

