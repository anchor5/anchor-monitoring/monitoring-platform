import connexion
import six

from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server import util


def post_token():  # noqa: E501
    """get Token

     # noqa: E501


    :rtype: InlineResponse200
    """
    dict = {"access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJvcmNoZXN0cmF0b3IiLCJleHAiOjE2NjAzODc0NDh9.FL-3svYnWq3PQyECbjgzha-mjlSjUpI5IG7rpwBhdng", "token_type":"bearer"}
    return dict
    
