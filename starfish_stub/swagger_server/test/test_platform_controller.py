# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.gateway import Gateway  # noqa: E501
from swagger_server.models.http_validation_error import HTTPValidationError  # noqa: E501
from swagger_server.models.network_slice import NetworkSlice  # noqa: E501
from swagger_server.models.new_network_slice import NewNetworkSlice  # noqa: E501
from swagger_server.test import BaseTestCase


class TestPlatformController(BaseTestCase):
    """PlatformController integration test stubs"""

    def test_create_network_slice_platform_networkslice_post(self):
        """Test case for create_network_slice_platform_networkslice_post

        Create a network slice
        """
        body = NewNetworkSlice()
        response = self.client.open(
            '/platform/networkslice',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_network_slice_platform_networkslice_delete(self):
        """Test case for delete_network_slice_platform_networkslice_delete

        Delete a network slice
        """
        query_string = [('slice_uid', 'slice_uid_example')]
        response = self.client.open(
            '/platform/networkslice',
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_gateways_platform_gateways_get(self):
        """Test case for gateways_platform_gateways_get

        List gateways
        """
        response = self.client.open(
            '/platform/gateways',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_network_slices_platform_networkslices_get(self):
        """Test case for network_slices_platform_networkslices_get

        List network slices
        """
        response = self.client.open(
            '/platform/networkslices',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
