# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.http_validation_error import HTTPValidationError  # noqa: E501
from swagger_server.models.kpis import Kpis  # noqa: E501
from swagger_server.models.network_slice_details import NetworkSliceDetails  # noqa: E501
from swagger_server.models.traffic_counters import TrafficCounters  # noqa: E501
from swagger_server.models.traffic_formats import TrafficFormats  # noqa: E501
from swagger_server.test import BaseTestCase


class TestNetworkSlicesController(BaseTestCase):
    """NetworkSlicesController integration test stubs"""

    def test_details_networkslice_details_get(self):
        """Test case for details_networkslice_details_get

        Details
        """
        query_string = [('slice_uid', 'slice_uid_example')]
        response = self.client.open(
            '/networkslice/details',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_modcod_networkslice_modcod_get(self):
        """Test case for get_modcod_networkslice_modcod_get

        Current modcod
        """
        query_string = [('slice_uid', 'slice_uid_example')]
        response = self.client.open(
            '/networkslice/modcod',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_kpis_networkslice_kpis_get(self):
        """Test case for kpis_networkslice_kpis_get

        KPIs
        """
        query_string = [('slice_uid', 'slice_uid_example'),
                        ('minutes', 60)]
        response = self.client.open(
            '/networkslice/kpis',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_migrate_networkslice_migrate_put(self):
        """Test case for migrate_networkslice_migrate_put

        Migrate to a gateway
        """
        query_string = [('slice_uid', 'slice_uid_example'),
                        ('gateway_name', 'gateway_name_example')]
        response = self.client.open(
            '/networkslice/migrate',
            method='PUT',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_set_modcod_networkslice_modcod_put(self):
        """Test case for set_modcod_networkslice_modcod_put

        Change modcod
        """
        query_string = [('slice_uid', 'slice_uid_example'),
                        ('modcod', 'modcod_example')]
        response = self.client.open(
            '/networkslice/modcod',
            method='PUT',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_traffic_counters_networkslice_traffic_counters_get(self):
        """Test case for traffic_counters_networkslice_traffic_counters_get

        Traffic counters
        """
        query_string = [('slice_uid', 'slice_uid_example'),
                        ('minutes', 60)]
        response = self.client.open(
            '/networkslice/traffic_counters',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_traffic_formats_networkslice_traffic_formats_get(self):
        """Test case for traffic_formats_networkslice_traffic_formats_get

        Traffic formats
        """
        query_string = [('slice_uid', 'slice_uid_example'),
                        ('minutes', 60)]
        response = self.client.open(
            '/networkslice/traffic_formats',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
