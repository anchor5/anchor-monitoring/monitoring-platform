# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.network_slice_quality import NetworkSliceQuality  # noqa: F401,E501
from swagger_server import util


class NetworkSlice(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, name: str=None, quality: NetworkSliceQuality=None, subnet_ip: str=None, gateway_ip: str=None, slice_uid: str=None, creation_utc: datetime=None):  # noqa: E501
        """NetworkSlice - a model defined in Swagger

        :param name: The name of this NetworkSlice.  # noqa: E501
        :type name: str
        :param quality: The quality of this NetworkSlice.  # noqa: E501
        :type quality: NetworkSliceQuality
        :param subnet_ip: The subnet_ip of this NetworkSlice.  # noqa: E501
        :type subnet_ip: str
        :param gateway_ip: The gateway_ip of this NetworkSlice.  # noqa: E501
        :type gateway_ip: str
        :param slice_uid: The slice_uid of this NetworkSlice.  # noqa: E501
        :type slice_uid: str
        :param creation_utc: The creation_utc of this NetworkSlice.  # noqa: E501
        :type creation_utc: datetime
        """
        self.swagger_types = {
            'name': str,
            'quality': NetworkSliceQuality,
            'subnet_ip': str,
            'gateway_ip': str,
            'slice_uid': str,
            'creation_utc': datetime
        }

        self.attribute_map = {
            'name': 'name',
            'quality': 'quality',
            'subnet_ip': 'subnet_ip',
            'gateway_ip': 'gateway_ip',
            'slice_uid': 'slice_uid',
            'creation_utc': 'creation_utc'
        }
        self._name = name
        self._quality = quality
        self._subnet_ip = subnet_ip
        self._gateway_ip = gateway_ip
        self._slice_uid = slice_uid
        self._creation_utc = creation_utc

    @classmethod
    def from_dict(cls, dikt) -> 'NetworkSlice':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The NetworkSlice of this NetworkSlice.  # noqa: E501
        :rtype: NetworkSlice
        """
        return util.deserialize_model(dikt, cls)

    @property
    def name(self) -> str:
        """Gets the name of this NetworkSlice.


        :return: The name of this NetworkSlice.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this NetworkSlice.


        :param name: The name of this NetworkSlice.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def quality(self) -> NetworkSliceQuality:
        """Gets the quality of this NetworkSlice.


        :return: The quality of this NetworkSlice.
        :rtype: NetworkSliceQuality
        """
        return self._quality

    @quality.setter
    def quality(self, quality: NetworkSliceQuality):
        """Sets the quality of this NetworkSlice.


        :param quality: The quality of this NetworkSlice.
        :type quality: NetworkSliceQuality
        """
        if quality is None:
            raise ValueError("Invalid value for `quality`, must not be `None`")  # noqa: E501

        self._quality = quality

    @property
    def subnet_ip(self) -> str:
        """Gets the subnet_ip of this NetworkSlice.


        :return: The subnet_ip of this NetworkSlice.
        :rtype: str
        """
        return self._subnet_ip

    @subnet_ip.setter
    def subnet_ip(self, subnet_ip: str):
        """Sets the subnet_ip of this NetworkSlice.


        :param subnet_ip: The subnet_ip of this NetworkSlice.
        :type subnet_ip: str
        """
        if subnet_ip is None:
            raise ValueError("Invalid value for `subnet_ip`, must not be `None`")  # noqa: E501

        self._subnet_ip = subnet_ip

    @property
    def gateway_ip(self) -> str:
        """Gets the gateway_ip of this NetworkSlice.


        :return: The gateway_ip of this NetworkSlice.
        :rtype: str
        """
        return self._gateway_ip

    @gateway_ip.setter
    def gateway_ip(self, gateway_ip: str):
        """Sets the gateway_ip of this NetworkSlice.


        :param gateway_ip: The gateway_ip of this NetworkSlice.
        :type gateway_ip: str
        """
        if gateway_ip is None:
            raise ValueError("Invalid value for `gateway_ip`, must not be `None`")  # noqa: E501

        self._gateway_ip = gateway_ip

    @property
    def slice_uid(self) -> str:
        """Gets the slice_uid of this NetworkSlice.


        :return: The slice_uid of this NetworkSlice.
        :rtype: str
        """
        return self._slice_uid

    @slice_uid.setter
    def slice_uid(self, slice_uid: str):
        """Sets the slice_uid of this NetworkSlice.


        :param slice_uid: The slice_uid of this NetworkSlice.
        :type slice_uid: str
        """
        if slice_uid is None:
            raise ValueError("Invalid value for `slice_uid`, must not be `None`")  # noqa: E501

        self._slice_uid = slice_uid

    @property
    def creation_utc(self) -> datetime:
        """Gets the creation_utc of this NetworkSlice.


        :return: The creation_utc of this NetworkSlice.
        :rtype: datetime
        """
        return self._creation_utc

    @creation_utc.setter
    def creation_utc(self, creation_utc: datetime):
        """Sets the creation_utc of this NetworkSlice.


        :param creation_utc: The creation_utc of this NetworkSlice.
        :type creation_utc: datetime
        """
        if creation_utc is None:
            raise ValueError("Invalid value for `creation_utc`, must not be `None`")  # noqa: E501

        self._creation_utc = creation_utc
