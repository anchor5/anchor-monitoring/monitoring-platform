import connexion

from swagger_server.models.datasource_type import DatasourceType  # noqa: E501

import logic.owm_datasource_logic as owm_logic
import logic.nwdaf_datasource_logic as nwdaf_logic
import logic.starfish_datasource_logic as starfish_logic
import logic.ransim_datasource_logic as ransim_logic

from flask import Response

def add_datasource_by_type(body, type_):  # noqa: E501
    """Add a new datasource of type given in the path.

     # noqa: E501

    :param body: Configuration of the datasource to be added
    :type body: dict | bytes
    :param type: Datasource type.
    :type type: dict | bytes

    :rtype: int
    """
    if connexion.request.is_json:

        if type_ == DatasourceType.OPENWEATHERMAP:
            response = owm_logic.add_owm_datasource(connexion.request.get_json())

        elif type_ == DatasourceType.NWDAF:
            response = nwdaf_logic.add_nwdaf_datasource(connexion.request.get_json())

        elif type_ == DatasourceType.STARFISH:
            response = starfish_logic.add_starfish_datasource(connexion.request.get_json())

        else:
            response = ransim_logic.add_ransim_datasource(connexion.request.get_json())

    return response

def delete_all_datasources():  # noqa: E501
    """Deletes all the Datasources currently active inside the Monitoring Platform

     # noqa: E501


    :rtype: List[str]
    """
    
    owm_deleted_datasources=owm_logic.delete_all_owm_datasources()
    nwdaf_deleted_datasources=nwdaf_logic.delete_all_nwdaf_datasources()
    starfish_deleted_datasources=starfish_logic.delete_all_starfish_datasources()
    ransim_deleted_datasources=ransim_logic.delete_all_ransim_datasources()
    
    ## for each datasource type invoke the delete all method and append the IDs
    
    ## TODO: change this check
    #if not deleted_datasources:
    #    return Response("No active datasources inside Monitoring Platform",status=404)
    
    return Response(status=200)


def delete_datasources_by_type(type_):  # noqa: E501
    """Deletes all the Datasources currently active inside the Monitoring Platform

     # noqa: E501

    :param type: Datasource type.
    :type type: dict | bytes

    :rtype: List[str]
    """

    if type_ == DatasourceType.OPENWEATHERMAP:
        response = owm_logic.delete_all_owm_datasources()

    elif type_ == DatasourceType.NWDAF:
        response = nwdaf_logic.delete_all_nwdaf_datasources()

    elif type_ == DatasourceType.STARFISH:
        response = starfish_logic.delete_all_starfish_datasources()

    else:
        response = ransim_logic.delete_all_ransim_datasources()

    return response


def delete_typed_datasource_by_id(type_, id_):  # noqa: E501
    """Deletes the datasource with identifier id.

     # noqa: E501

    :param type: Datasource type.
    :type type: dict | bytes
    :param id: Identifier of the datasource to deal with.
    :type id: int

    :rtype: int
    """
    if type_ == DatasourceType.OPENWEATHERMAP:
        response = owm_logic.delete_owm_datasource(id_)
    
    elif type_ == DatasourceType.NWDAF:
        response = nwdaf_logic.delete_nwdaf_datasource(id_)

    elif type_ == DatasourceType.STARFISH:
        response = starfish_logic.delete_starfish_datasource(id_)

    else:
        response = ransim_logic.delete_ransim_datasource(id_)

    return response


def get_all_datasources():  # noqa: E501
    """Retrieves information about all the datasources.

     # noqa: E501


    :rtype: List[Object] --> List of Lists of datasources
    """

    return Response(
            "List of the active datasources inside the monitoring platform:\nOpenWeatherMap: {}\nNWDAF: {}\nStarfish: {}\nRanSim: {}".format(
                owm_logic.get_all_owm_datasources(), 
                nwdaf_logic.get_all_nwdaf_datasources(), 
                starfish_logic.get_all_starfish_datasources(), 
                ransim_logic.get_all_ransim_datasources()),
            status=200)


def get_datasources_by_type(type_):  # noqa: E501
    """Returns all the datasources of a given type.

     # noqa: E501

    :param type: Datasource type.
    :type type: dict | bytes

    :rtype: List[Object]
    """
    print(type_) 
    if (type_ == DatasourceType.OPENWEATHERMAP):
        datasources = owm_logic.get_all_owm_datasources()

    elif (type_ == DatasourceType.NWDAF):
        datasources = nwdaf_logic.get_all_nwdaf_datasources()

    elif (type_ == DatasourceType.STARFISH):
        datasources = starfish_logic.get_all_starfish_datasources()
    
    #else:
    elif (type_ == DatasourceType.RANSIM):
        datasources = ransim_logic.get_all_ransim_datasources()

    # Response Building
    if datasources:
        return Response(
            "List of {} datasources Found:\n{}\n".format(type_, datasources),
            status=200)
    else:
        return Response(
            "No {} datasources Found\n".format(type_),
            status=404)


def get_typed_datasource_by_id(type_, id_):  # noqa: E501
    """get a single datasource by id.

     # noqa: E501

    :param type: Datasource type.
    :type type: dict | bytes
    :param id: Identifier of the datasource to deal with.
    :type id: int

    :rtype: DatasourcesTypeBody
    """
    if (type_ == DatasourceType.OPENWEATHERMAP):
        datasource = owm_logic.get_owm_datasource_by_id(id_)

    elif (type_ == DatasourceType.NWDAF):
        datasource = nwdaf_logic.get_nwdaf_datasource_by_id(id_)

    elif (type_ == DatasourceType.STARFISH):
        datasource = starfish_logic.get_starfish_datasource_by_id(id_)

    else:
        datasource = ransim_logic.get_ransim_datasource_by_id(id_)

    # Response Building
    if datasource is None:
        return Response(
            "{} datasource with id {} Not Found\n".format(type_, id_),
            status=404)
    else:
        return Response(
            "{}".format(datasource),
            status=200)


def update_typed_datasource_by_id(body, type_, id_):  # noqa: E501
    """Updates configuration of the datasource with identifier specified in the path.

     # noqa: E501

    :param body: New Configuration of the datasource.
    :type body: dict | bytes
    :param type: Datasource type.
    :type type: dict | bytes
    :param id: Identifier of the datasource to deal with.
    :type id: int

    :rtype: DatasourcesTypeBody
    """
    
    if connexion.request.is_json:

        if type_ == DatasourceType.OPENWEATHERMAP:
            response = owm_logic.update_owm_datasource_by_id(id_, connexion.request.get_json())

        elif (type_ == DatasourceType.NWDAF):
            response = nwdaf_logic.update_nwdaf_datasource_by_id(id_, connexion.request.get_json())

        elif (type_ == DatasourceType.STARFISH):
           response = starfish_logic.update_starfish_datasource_by_id(id_, connexion.request.get_json())
    
        else:
            response = ransim_logic.update_ransim_datasource_by_id(id_, connexion.request.get_json())

    return response
