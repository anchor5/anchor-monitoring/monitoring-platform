# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.datasource_type import DatasourceType  # noqa: E501
from swagger_server.models.datasources_type_body import DatasourcesTypeBody  # noqa: E501
from swagger_server.models.type_id_body import TypeIdBody  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDatasourceController(BaseTestCase):
    """DatasourceController integration test stubs"""

    def test_add_datasource_by_type(self):
        """Test case for add_datasource_by_type

        Add a new datasource of type given in the path.
        """
        body = DatasourcesTypeBody()
        response = self.client.open(
            '/datasources/{type}'.format(type=DatasourceType()),
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_typed_datasource_by_id(self):
        """Test case for delete_typed_datasource_by_id

        Deletes the datasource with identifier id.
        """
        response = self.client.open(
            '/datasources/{type}/{id}'.format(type=DatasourceType(), id=56),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_all_datasources(self):
        """Test case for get_all_datasources

        Retrieves information about all the datasources.
        """
        response = self.client.open(
            '/datasources',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_datasources_by_type(self):
        """Test case for get_datasources_by_type

        Returns all the datasources of a given type.
        """
        response = self.client.open(
            '/datasources/{type}'.format(type=DatasourceType()),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_typed_datasource_by_id(self):
        """Test case for get_typed_datasource_by_id

        get a single datasource by id.
        """
        response = self.client.open(
            '/datasources/{type}/{id}'.format(type=DatasourceType(), id=56),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_typed_datasource_by_id(self):
        """Test case for update_typed_datasource_by_id

        Updates configuration of the datasource with identifier specified in the path.
        """
        body = TypeIdBody()
        response = self.client.open(
            '/datasources/{type}/{id}'.format(type=DatasourceType(), id=56),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
