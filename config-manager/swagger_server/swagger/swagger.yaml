openapi: 3.0.2
info:
  title: Monitoring_Platform Config_Manager NBI
  description: High level APIs in order to configure datasources in the ANChOR monitoring
    platform.
  contact:
    name: Leonardo Lossi
    email: l.lossi@nextworks.it
  version: 1.0.0
servers:
- url: http://configmanager:8888
tags:
- name: datasource
  description: Tag related to the configuration of the resourced monitored by the
    different datasources.
paths:
  /datasources:
    summary: Endpoint to request all the datasources active inside the monitoring
      platform.
    get:
      tags:
      - datasource
      summary: Retrieves information about all the datasources.
      operationId: get_all_datasources
      responses:
        "200":
          description: List of all the active datasources inside the monitoring platform.
          content:
            application/json:
              schema:
                type: array
                items:
                  oneOf:
                  - $ref: '#/components/schemas/owm-datasource'
                  - $ref: '#/components/schemas/nwdaf-datasource'
                  - $ref: '#/components/schemas/ransim-datasource'
                  - $ref: '#/components/schemas/starfish-datasource'
                x-content-type: application/json
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
    delete:
      tags:
      - datasource
      summary: Deletes all the Datasources currently active inside the Monitoring
        Platform
      operationId: delete_all_datasources
      responses:
        "200":
          description: List of the UUID of the datasources deleted from the Monitoring
            Platform
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                  format: uuid
                x-content-type: application/json
        "404":
          description: No active datasource found.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
  /datasources/{type}:
    summary: Path to deal with datasources
    get:
      tags:
      - datasource
      summary: Returns all the datasources of a given type.
      operationId: get_datasources_by_type
      parameters:
      - name: type
        in: path
        description: Datasource type.
        required: true
        style: simple
        explode: false
        schema:
          $ref: '#/components/schemas/datasource-type'
      responses:
        "200":
          description: Resources correctly retrieved.
          content:
            application/json:
              schema:
                type: array
                items:
                  oneOf:
                  - $ref: '#/components/schemas/owm-datasource'
                  - $ref: '#/components/schemas/nwdaf-datasource'
                  - $ref: '#/components/schemas/ransim-datasource'
                  - $ref: '#/components/schemas/starfish-datasource'
                x-content-type: application/json
        "400":
          description: Provided invalid datasource-type.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
    post:
      tags:
      - datasource
      summary: Add a new datasource of type given in the path.
      operationId: add_datasource_by_type
      parameters:
      - name: type
        in: path
        description: Datasource type.
        required: true
        style: simple
        explode: false
        schema:
          $ref: '#/components/schemas/datasource-type'
      requestBody:
        description: Configuration of the datasource to be added
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/datasources_type_body'
        required: true
      responses:
        "200":
          description: Datasource created. Returns the Datasource UID
          content:
            application/json:
              schema:
                type: integer
                x-content-type: application/json
        "400":
          description: Provided invalid datasource configuration.
        "404":
          description: Datasource-type not found.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
    delete:
      tags:
      - datasource
      summary: Deletes all the Datasources currently active inside the Monitoring
        Platform
      operationId: delete_datasources_by_type
      parameters:
      - name: type
        in: path
        description: Datasource type.
        required: true
        style: simple
        explode: false
        schema:
          $ref: '#/components/schemas/datasource-type'
      responses:
        "200":
          description: List of the UUID of the datasources deleted from the Monitoring
            Platform
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                  format: uuid
                x-content-type: application/json
        "404":
          description: No active datasource found for the givev Datasource-type.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
  /datasources/{type}/id/{id}:
    summary: Path to deal with single datasources.
    get:
      tags:
      - datasource
      summary: get a single datasource by id.
      operationId: get_typed_datasource_by_id
      parameters:
      - name: type
        in: path
        description: Datasource type.
        required: true
        style: simple
        explode: false
        schema:
          $ref: '#/components/schemas/datasource-type'
      - name: id
        in: path
        description: Identifier of the datasource to deal with.
        required: true
        style: simple
        explode: false
        schema:
          type: string
          format: uuid
      responses:
        "200":
          description: Datasources retrieved.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200'
        "400":
          description: Invalid UID provided.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
    put:
      tags:
      - datasource
      summary: Updates configuration of the datasource with identifier specified in
        the path.
      operationId: update_typed_datasource_by_id
      parameters:
      - name: type
        in: path
        description: Datasource type.
        required: true
        style: simple
        explode: false
        schema:
          $ref: '#/components/schemas/datasource-type'
      - name: id
        in: path
        description: Identifier of the datasource to deal with.
        required: true
        style: simple
        explode: false
        schema:
          type: string
          format: uuid
      requestBody:
        description: New Configuration of the datasource.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/id_id_body'
        required: true
      responses:
        "200":
          description: Configuration correctly updated.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200'
        "400":
          description: Provided invalid configuration.
        "404":
          description: Datasource not found.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
    delete:
      tags:
      - datasource
      summary: Deletes the datasource with identifier id.
      operationId: delete_typed_datasource_by_id
      parameters:
      - name: type
        in: path
        description: Datasource type.
        required: true
        style: simple
        explode: false
        schema:
          $ref: '#/components/schemas/datasource-type'
      - name: id
        in: path
        description: Identifier of the datasource to deal with.
        required: true
        style: simple
        explode: false
        schema:
          type: string
          format: uuid
      responses:
        "200":
          description: Datasource deleted.
          content:
            application/json:
              schema:
                type: integer
                x-content-type: application/json
        "404":
          description: Datasource not found.
      x-openapi-router-controller: swagger_server.controllers.datasource_controller
components:
  schemas:
    datasource-type:
      type: string
      description: |-
        Enum in order to list all the possible datasource type in ANChOR. Can be one of:
         - OpenweatherMap
         - NWDAF
         - Starfish
         - RanSim
      enum:
      - OpenWeatherMap
      - NWDAF
      - Starfish
      - RanSim
    owm-metrics:
      type: string
      description: "Possible metric that can be retrieved by the OWM datasource. Possible\
        \ values can be:  - cloudiness - humidity - pressure - raint - temperature\
        \ - wind-degrees - wind-speed"
      enum:
      - cloudiness
      - humidity
      - pressure
      - rain
      - temperature
      - wind-degrees
      - wind-speed
    nwdaf-metrics:
      type: string
      description: "Possible metrics that the nwdaf datasource can retrieve. Possible\
        \ values are: \n - ul-volume\n - dl-volume\n - rtt\n - pdu-number\n - pkt-loss"
      enum:
      - ul-volume
      - dl-volume
      - rtt
      - pdu-number
      - pkt-loss
    starfish-metrics:
      type: string
      description: "Possible metrics that the starfish datasource can retrieve. Possible\
        \ values are: - traffic_counters - traffic_formats - kpis"
      enum:
      - traffic_counters
      - traffic_formats
      - kpis
    ransim-metrics:
      type: string
      description: |-
        Possible metrics that the ransim datasource can retrieve. Possible values are (afb stands for "active frequency ands"):
         - ue_positioning
         - gnb_positioning
         - network_configuration
         - resource_configuration
         - network_status
         - performance_results
      enum:
      - ue_positioning
      - gnb_positioning
      - network_configuration
      - resource_configuration
      - network_status
      - performance_results
    datasource:
      type: object
      properties:
        datasource-type:
          $ref: '#/components/schemas/datasource-type'
      description: Info Model to deal with datasources
      discriminator:
        propertyName: datasource-type
    owm-editable-parameters:
      type: object
      properties:
        metrics:
          type: array
          description: "List of the weather metrics to be retrieved by this datasource.\
            \ If not specified, it will retrieve all the metrics."
          items:
            $ref: '#/components/schemas/owm-metrics'
        topic-name:
          type: string
          description: Kafka topic in which metrics of this datasource will be published.
            If empty the metrics will be published in a Kafka topic named according
            a predefined pattern.
      description: Parameters that can be modified inside a put request on an OWM
        datasource
    nwdaf-editable-parameters:
      type: object
      properties:
        api-root:
          type: string
          description: Ip address of the api-root to which the HTTP requests must
            be issued.
        interval:
          type: string
          description: The inter-request interval of the NWDAF datasource.
        metrics:
          type: array
          description: List of the metrics to be retrieved for this datasource.
          items:
            $ref: '#/components/schemas/nwdaf-metrics'
        topic-name:
          type: string
          description: Name of the topic in which the requested metrics retrieved
            by this datasource will be published. If empty the topic name will follow
            a common pattern.
      description: Parameters that can be modified inside a put request on an NWDAF
        datasource
    starfish-editable-parameters:
      type: object
      properties:
        api-root:
          type: string
          description: Ip address of the api-root to which the HTTP requests must
            be issued.
        interval:
          type: string
          description: The inter-request interval of the Starfish datasource.
        metrics:
          type: array
          description: List of the metrics to be retrieved by this datasource.
          items:
            $ref: '#/components/schemas/starfish-metrics'
        topic-name:
          type: string
          description: Name of the topic in which the requested metrics retrieved
            by this datasource will be published. If empty the topic name will follow
            a common pattern.
    ransim-editable-parameters:
      type: object
      properties:
        metrics:
          type: array
          description: List of the metrics to be retrieved for this datasource.
          items:
            $ref: '#/components/schemas/ransim-metrics'
        remote-output-file-path:
          type: string
          description: Path of the output file on the RAN simulator machine
        remote-input-files-path:
          type: array
          description: List of the paths of the input files on the RAN simulator machine
          items:
            type: string
        topic-name:
          type: string
          description: Name of the topic in which the requested metrics retrieved
            by this datasource will be published. If empty the topic name will follow
            a common pattern.
      description: Parameters that can be modified inside a put request on an RANSIM
        datasource
    owm-datasource:
      description: Info model of the OpenWeatherMap datasources. The interval is fixed
        to 1 h since the OWM updates its data with this time frequeny.
      allOf:
      - $ref: '#/components/schemas/datasource'
      - required:
        - city-uid
        type: object
        properties:
          id:
            type: string
            description: Identifier of the OWM datasource
            format: uuid
          city-uid:
            type: integer
            description: Identifier of the city whose current and forecast weather
              data must be retrieved.
          metrics:
            type: array
            description: "List of the weather metrics to be retrieved by this datasource.\
              \ If not specified, it will retrieve all the metrics."
            items:
              $ref: '#/components/schemas/owm-metrics'
          topic-name:
            type: string
            description: Kafka topic in which metrics of this datasource will be published.
              If empty the metrics will be published in a Kafka topic named according
              a predefined pattern.
    nwdaf-datasource:
      description: Info model related to the configuration of an NWDAF datasource
      allOf:
      - $ref: '#/components/schemas/datasource'
      - type: object
        properties:
          id:
            type: string
            description: Identifier of the NWDAF datasource
            format: uuid
          end-to-end-network-slice-uid:
            type: string
            description: The UID of the E2E network slice
          network-slice-subnet-uid:
            type: string
            description: "The UID of the network slice subnet in the NWDAF domain\
              \ (mapped in the nsiId in the request). If not specified, anyslice=true\
              \ 3GPP-compliant"
          flow-id:
            type: array
            description: IDs of the NS flows whose metrics must be retrieved.
            items:
              type: string
          api-root:
            type: string
            description: Ip address of the api-root to which the HTTP requests must
              be issued.
          interval:
            type: string
            description: The inter-request interval of the NWDAF datasource.
          metrics:
            type: array
            description: List of the metrics to be retrieved for this datasource.
            items:
              $ref: '#/components/schemas/nwdaf-metrics'
          topic-name:
            type: string
            description: Name of the topic in which the requested metrics retrieved
              by this datasource will be published. If empty the topic name will follow
              a common pattern.
    ransim-datasource:
      description: Info model related to the configuration of the RAN simulator datasource.
      allOf:
      - $ref: '#/components/schemas/datasource'
      - required:
        - remote-input-files-path
        - remote-output-file-path
        type: object
        properties:
          id:
            type: string
            description: Identifier of the RANSim datasource
            format: uuid
          remote-output-file-path:
            type: string
            description: Path of the output file on the RAN simulator machine
          remote-input-files-path:
            type: array
            description: List of the paths of the input files on the RAN simulator
              machine
            items:
              type: string
          metrics:
            type: array
            items:
              $ref: '#/components/schemas/ransim-metrics'
          topic-name:
            type: string
    starfish-datasource:
      description: Info model related to the configuration of a Starfish datasource
      allOf:
      - $ref: '#/components/schemas/datasource'
      - required:
        - end-to-end-network-slice-uid
        - network-slice-subnet-uid
        type: object
        properties:
          id:
            type: string
            description: Identifier of the Starfish datasource
            format: uuid
          api-root:
            type: string
            description: api-root to which the HTTP requests must be issued.
          interval:
            type: string
            description: The inter-request interval of the Starfish datasource.
          end-to-end-network-slice-uid:
            type: string
            description: The UID of the E2E network slice
          network-slice-subnet-uid:
            type: string
            description: The UID of the network slice subnet in the Starfish domain
          metrics:
            type: array
            description: List of the metrics to be retrieved for this datasource.
            items:
              $ref: '#/components/schemas/starfish-metrics'
          topic-name:
            type: string
            description: Name of the topic in which the requested metrics retrieved
              by this datasource will be published. If empty the topic name will follow
              a common pattern.
    datasources_type_body:
      discriminator:
        propertyName: datasource-type
        mapping:
          OpenWeatherMap: '#/components/schemas/owm-datasource'
          NWDAF: '#/components/schemas/nwdaf-datasource'
          RanSim: '#/components/schemas/ransim-datasource'
          Starfish: '#/components/schemas/starfish-datasource'
      anyOf:
      - $ref: '#/components/schemas/owm-datasource'
      - $ref: '#/components/schemas/nwdaf-datasource'
      - $ref: '#/components/schemas/ransim-datasource'
      - $ref: '#/components/schemas/starfish-datasource'
    inline_response_200:
      discriminator:
        propertyName: datasource_type
        mapping:
          OpenWeatherMap: '#/components/schemas/owm-datasource'
          NWDAF: '#/components/schemas/nwdaf-datasource'
          RanSim: '#/components/schemas/ransim-datasource'
          Starfish: '#/components/schemas/starfish-datasource'
      oneOf:
      - $ref: '#/components/schemas/owm-datasource'
      - $ref: '#/components/schemas/nwdaf-datasource'
      - $ref: '#/components/schemas/ransim-datasource'
      - $ref: '#/components/schemas/starfish-datasource'
    id_id_body:
      anyOf:
      - $ref: '#/components/schemas/owm-editable-parameters'
      - $ref: '#/components/schemas/nwdaf-editable-parameters'
      - $ref: '#/components/schemas/ransim-editable-parameters'
      - $ref: '#/components/schemas/starfish-editable-parameters'

