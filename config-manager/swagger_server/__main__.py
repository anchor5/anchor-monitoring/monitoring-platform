#!/usr/bin/env python3

import connexion
import logging

from swagger_server import encoder
from logic.db_creation import create_db_schema
import utils.configuration_mgmt as conf


def main():
    conf.init_configuration()
    # Remove all handlers associated with the root logger object.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(filename=conf.configuration['PATHS']['logfile_path'], encoding='utf-8', level=logging.DEBUG)
    print('Logger configured to write logs to {} file'.format(conf.configuration['PATHS']['logfile_path']))
    logging.info('Configuration loaded')
    create_db_schema()
    logging.info('Database schema created')
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'config-manager-apis'}, pythonic_params=True)
    app.run(port=8080)


if __name__ == '__main__':
    main()
