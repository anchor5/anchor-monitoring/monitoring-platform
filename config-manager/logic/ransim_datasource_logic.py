import logging
import filelock
import toml
import os.path
import uuid
from flask import Response
from sqlalchemy import select

from swagger_server.models.datasource_type import DatasourceType
from swagger_server.models.ransim_datasource import RansimDatasource  # noqa: E501
from swagger_server.models.ransim_metrics import RansimMetrics

import logic.db_creation as db
from db_models.ransim_datasource_db import RansimDatasourceDB

import utils.ransim_collector_configuration_mgmt as mgmt
import utils.metric_list_mgmt as lsmgmt
import utils.docker_service_mgmt as docker

import utils.configuration_mgmt as conf

def check_metrics_already_monitored_in_topic(session, metrics_to_check, topic_name_to_check):
    check_ransim_datasource_duplicate_list = session.query(RansimDatasourceDB).filter(RansimDatasourceDB.topic_name == topic_name_to_check)
    if check_ransim_datasource_duplicate_list:
        metrics_db_accumulator = []
        for ransim_datasource in check_ransim_datasource_duplicate_list:
            metrics_db_accumulator = list(set(metrics_db_accumulator + ransim_datasource.metrics))
        ## the datasources publishing in the topic_name topic are already monitoring at least the requested metrics
        return lsmgmt.check_list_subset(metrics_to_check,metrics_db_accumulator)
    else:
        return False


def check_metrics_already_monitored_in_updated_topic(session, datasource_id, metrics_to_check,topic_name_to_check):
    other_ransim_dataource_list = session.query(RansimDatasourceDB).filter(RansimDatasourceDB.id != datasource_id)
    if(other_ransim_dataource_list):
        metrics_db_accumulator = []
        for ransim_datasource in other_ransim_dataource_list:
            if ransim_datasource.topic_name == topic_name_to_check:
                metrics_db_accumulator = list(set(metrics_db_accumulator + ransim_datasource.metrics))
        ## the datasources publishing in the topic_name topic are already monitoring at least the requested metrics
        return lsmgmt.check_list_subset(metrics_to_check,metrics_db_accumulator)
    else:
        return False


def add_ransim_datasource(ransim_datasource_dict):
    with db.Session() as session:
        if "metrics" in ransim_datasource_dict.keys():
            # cleaning the metrics list from duplicates (in case 'metrics' is not specified, it will monitor all the Ransim metrics)
            ransim_datasource_dict['metrics'] = lsmgmt.remove_duplicates(ransim_datasource_dict['metrics'])
         
        if "topic-name" not in ransim_datasource_dict.keys() or ransim_datasource_dict["topic-name"] == "":
            ransim_datasource_dict['topic-name'] = "ransim"
        
        # creation of the new object to be inserted in the db
        new_ransim_datasource_db = RansimDatasourceDB(RansimDatasource.from_dict(ransim_datasource_dict))

#        if len(ransim_datasource_dict['remote-input-files-path']) < 3:
#            return Response(
#                "At least 4 input files path must be provided (ue-positioning, gnb-positioning, resource-configuration and network-configuration), cannot create the requested datasource\n",
#                status=400)
        
        if check_metrics_already_monitored_in_topic(session, new_ransim_datasource_db.metrics, new_ransim_datasource_db.topic_name):
            return Response(
                "The existent Ransim datasources publishing in topic {} are already monitoring the requested metrics, cannot create the requested datasource\n".format(new_ransim_datasource_db.topic_name),
                status=409)
        else:
            session.add(new_ransim_datasource_db)
            logging.info(new_ransim_datasource_db.topic_name)
            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                # critical section
                with filelock.FileLock("telegraf_ransim_conf.lock"):                
                    logging.info("Critical Section accessed")
                    ## NOTE: the configuration is updated with the right map value just in case topic_name is "" in case we want to exploit the default name pattern
                    new_config_dictionary = mgmt.add_ransim_datasource_to_config(new_ransim_datasource_db.metrics,new_ransim_datasource_db.topic_name,new_ransim_datasource_db.remote_output_file_path, new_ransim_datasource_db.remote_input_files_path)  
                    logging.info("Produced new telegraf configuration after addition, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            logging.info("Committed to DB")
                            service_updated_flag = docker.update_telegraf_ransim_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_ransim service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(new_ransim_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                            status=500)
            # if the configuration file does not exist
            else:
                return Response(
                    "Error Accessing the telegraf.conf file, check the path\n",
                     status=500)


def delete_ransim_datasource(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        ransim_datasource_db = session.query(RansimDatasourceDB).filter(RansimDatasourceDB.id == id).first()
        if ransim_datasource_db is not None:
            session.delete(ransim_datasource_db)
            ransim_datasources_list = session.query(RansimDatasourceDB)
            ## if there were more than one datasources --> find the metrics to be deleted from outputs, if any
            if ransim_datasources_list.count() >= 1:
                kafka_metrics_accumulator = []
                other_outputs_metrics_accumulator = []
                for ransim_datasource in ransim_datasources_list:
                    if ransim_datasource.id != id_:
                        if ransim_datasource.topic_name == ransim_datasource_db.topic_name:
                            kafka_metrics_accumulator = list(set(kafka_metrics_accumulator).union(set(ransim_datasource.metrics)))
                        else:
                            other_outputs_metrics_accumulator = list(set(other_outputs_metrics_accumulator).union(set(ransim_datasource.metrics)))

                kafka_metrics_to_delete = list(set(ransim_datasource_db.metrics)-set(kafka_metrics_accumulator))
                other_outputs_metrics_to_delete = list(set(ransim_datasource_db.metrics)-set(other_outputs_metrics_accumulator))

            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                # critical section
                with filelock.FileLock("telegraf_ransim_conf.lock"):                
                    logging.info("Critical Section accessed")
                    if ransim_datasources_list.count() >= 1:
                        new_config_dictionary = mgmt.delete_ransim_datasource_from_config(ransim_datasource_db.topic_name, kafka_metrics_to_delete, other_outputs_metrics_to_delete) 
                    else:
                        new_config_dictionary = mgmt.delete_outputs_ransim_datasource_from_config()
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            service_updated_flag = docker.update_telegraf_ransim_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_ransim service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(ransim_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                     status=500)
        # if the requested Ransim datasource has been not found                     
        return Response(
            "Ransim datasource with id {} Not Found\n".format(id_),
            status=404)

#at most one ransim datasource
def delete_all_ransim_datasources():
    with db.Session() as session:
        ransim_datasources_list = session.query(RansimDatasourceDB)
        if ransim_datasources_list is not None:
            ransim_datasources_db_uids = []
            for ransim_datasource_db in ransim_datasources_list:
                ransim_datasources_db_uids.append(ransim_datasource_db.id)
                session.delete(ransim_datasource_db)
                
                # check the configuration file existence to perform an eventual stop early
                if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                    # critical section
                    with filelock.FileLock("telegraf_ransim_conf.lock"):                
                        logging.info("Critical Section accessed")
                        new_config_dictionary = mgmt.delete_all_ransim_datasources_from_config()
                        logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                        try:
                            with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                file.write(toml.dumps(new_config_dictionary))
                                logging.info("Configuration wrote")
                                # the commit must be done just in case the configuration file is updated successfully
                                session.commit()
                                service_updated_flag = docker.update_telegraf_ransim_service()
                                if not service_updated_flag:
                                    return Response(
                                        "Error while restarting the mp_telegraf_ransim service\n",
                                        status=500)
                                else:
                                    return Response(
                                        "{}".format(ransim_datasources_db_uids),
                                        status=200)
                        except Exception as e:
                            logging.error(e)
                            return Response(
                                "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                status=500)
                else:
                    return Response(
                        "Error Accessing the telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                     status=500)
            # if the requested Ransim datasource has been not found                     
        return Response(
            "No Ransim datasources Found\n",
            status=404)

def get_all_ransim_datasources():
    statement = select(RansimDatasourceDB)
    with db.Session() as session:
        ransim_datasource_list =[]
        ransim_datasources_db_list = session.query(RansimDatasourceDB)
        # if there is at least one Ransim_datasource, creates a non empty list
        if ransim_datasources_db_list.first() is not None:
            for row in session.execute(statement):
                ransim_datasource_db = row._mapping['RansimDatasourceDB']
                ransim_datasource_list.append(RansimDatasource(DatasourceType.RANSIM, ransim_datasource_db.id, ransim_datasource_db.remote_output_file_path, ransim_datasource_db.remote_input_files_path, ransim_datasource_db.metrics, ransim_datasource_db.topic_name))
        
        return ransim_datasource_list


def get_ransim_datasource_by_id(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        ransim_datasource_db = session.query(RansimDatasourceDB).filter(RansimDatasourceDB.id == id).first()
        if ransim_datasource_db is not None:
            return RansimDatasource(DatasourceType.RANSIM, ransim_datasource_db.id, ransim_datasource_db.remote_output_file_path, ransim_datasource_db.remote_input_files_path, ransim_datasource_db.metrics, ransim_datasource_db.topic_name)    


def update_ransim_datasource_by_id(id_, ransim_datasource_dict):

    # check if the user is trying to change some Ransim-datasource fields not allowed
    if any (k in ransim_datasource_dict for k in ("id","datasource-type","remote-output-file-path","remote-input-files-path")):
        return Response(
            "Cannot update fields different from metrics and topic_name for Ransim datasources",
            status=400)
    if not(any(k in ransim_datasource_dict for k in ("metrics","topic-name"))):
        return Response(
            "None of the Ransim updatable parameters have been specified for updating the Ransim datasource",
            status=400)

    with db.Session() as session:
        id = uuid.UUID(id_)
        # query the db in order to find the Ransim Datasource with the requested id
        ransim_datasource_db = session.query(RansimDatasourceDB).filter(RansimDatasourceDB.id == id).first()
        
        if ransim_datasource_db is not None:

            # if topic-name is not present in the body, the field is left unchanged
            if "topic-name" in ransim_datasource_dict.keys():
                # if topic-name is empty the topic name follows the predefined pattern
                if not ransim_datasource_dict['topic-name']:
                    topic_name_to_check = "ransim"
                else:
                    topic_name_to_check = ransim_datasource_dict['topic-name']
            else:
                topic_name_to_check = ransim_datasource_db.topic_name

            # if metrics is not present in the body, the field is left unchanged
            if "metrics" in ransim_datasource_dict.keys():                
                # if the list of Ransim Metrics is empty, monitor all the possible metrics
                if not ransim_datasource_dict['metrics']:
                    metrics_to_check = [RansimMetrics.UE_POSITIONING, RansimMetrics.GNB_POSITIONING, RansimMetrics.NETWORK_CONFIGURATION, RansimMetrics.RESOURCE_CONFIGURATION, RansimMetrics.NETWORK_STATUS]
                else:
                 # remove duplicates from the metrics list
                    ransim_datasource_dict['metrics'] = lsmgmt.remove_duplicates(ransim_datasource_dict['metrics'])
                    metrics_to_check = ransim_datasource_dict['metrics']
            else:
                metrics_to_check = ransim_datasource_db.metrics

            ## if no othere datasource is present, updated --> TESTED TOTALLY
            if session.query(RansimDatasourceDB).filter(RansimDatasourceDB.id != id_).count() == 0:
                logging.info("No other datasource found, updating the only one")

                ransim_datasource_db.topic_name = topic_name_to_check
                ransim_datasource_db.metrics = metrics_to_check

                # check the configuration file existence to perform an eventual stop early
                if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                    # critical section
                    with filelock.FileLock("telegraf_ransim_conf.lock"):                
                        logging.info("Critical Section accessed")
                        new_config_dictionary = mgmt.update_single_datasource(topic_name_to_check, metrics_to_check)
                        logging.info("Produced new telegraf configuration after addition, writing it on the file")
                        try:
                            with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                file.write(toml.dumps(new_config_dictionary))
                                logging.info("Configuration wrote")
                                # the commit must be done just in case the configuration file is updated successfully
                                session.commit()
                                logging.info("Committed to DB")
                                service_updated_flag = docker.update_telegraf_ransim_service()
                                if not service_updated_flag:
                                    return Response(
                                        "Error while restarting the mp_telegraf_ransim service\n",
                                        status=500)
                                else:
                                    return Response(
                                        "{}".format(ransim_datasource_db), status=200)
                        except Exception as e:
                            logging.error(e)
                            return Response(
                                "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                status=500)
                # if the configuration file does not exist
                else:
                    return Response(
                        "Error Accessing the telegraf.conf file, check the path\n",
                        status=500)


            else: ## if this is not the only datasource
                logging.info("The datasource to update is not the only one existing inside the DB. Beginning the updating procedure.")

                ### check if the set of the metrics of all the ransim datasource with same topic name is equal to metrics to check in order to not have duplicated inside same topic
                if check_metrics_already_monitored_in_updated_topic(session, id_, metrics_to_check,topic_name_to_check):
                    return Response(
                        "The existent Ransim datasources publishing in topic {} are already monitoring the requested metrics, cannot update the datasource\n".format(topic_name_to_check),
                        status=409)
                else:
                    logging.info("The requested update is valid, modifying the DB and the configuration file.")
                    ransim_datasources_list = session.query(RansimDatasourceDB).filter(RansimDatasourceDB.id != id_)

                    if ransim_datasource_db.topic_name != topic_name_to_check:
                        logging.info("Requested topic_name change")

                        if not lsmgmt.check_list_equality_regardless_ordering(ransim_datasource_db.metrics, metrics_to_check):
                            logging.info("Requested also metrics change")

                            ## changes metrics and topic name
                            kafka_metrics_accumulator_old_output = []
                            kafka_metrics_accumulator_new_output = []
                            other_outputs_metrics_accumulator = []
                            for ransim_datasource in ransim_datasources_list:

                                if ransim_datasource.topic_name == ransim_datasource_db.topic_name:
                                    kafka_metrics_accumulator_old_output = list(set(kafka_metrics_accumulator_old_output).union(set(ransim_datasource.metrics)))
                                else:
                                    if ransim_datasource.topic_name == topic_name_to_check:
                                        kafka_metrics_accumulator_new_output = list(set(kafka_metrics_accumulator_new_output).union(set(ransim_datasource.metrics)))

                                other_outputs_metrics_accumulator = list(set(other_outputs_metrics_accumulator).union(set(ransim_datasource.metrics)))

                            logging.debug("kakfa_old_out_acc = {}".format(kafka_metrics_accumulator_old_output))
                            logging.debug("kafka_new_out_acc = {}".format(kafka_metrics_accumulator_new_output))
                            logging.debug("other_out_acc = {}".format(other_outputs_metrics_accumulator))

                            if len(other_outputs_metrics_accumulator) > len(metrics_to_check):
                                new_other_output_metrics = list(set(metrics_to_check).union(set(other_outputs_metrics_accumulator) - set(metrics_to_check)))
                            else:
                                new_other_output_metrics = list(set(other_outputs_metrics_accumulator).union(set(metrics_to_check) - set(other_outputs_metrics_accumulator)))

                            if not kafka_metrics_accumulator_old_output: ## if there are not other kafka publishing in the same old topic

                                if not kafka_metrics_accumulator_new_output: ## if there is not a kafka output publishing in the new topic --> TESTED

                                    logging.debug("mgmt.delete_old_and_create_new(ransim_datasource_db.topic_name, topic_name_to_check, metrics_to_check, new_other_output_metrics)")
                                    logging.debug(metrics_to_check)
                                    logging.debug(new_other_output_metrics)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.delete_old_and_create_new(ransim_datasource_db.topic_name, topic_name_to_check, metrics_to_check, new_other_output_metrics)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                                else: ## there is at least another kafka publishing in the same new topic --> TESTED
                                    if len(metrics_to_check) > len(kafka_metrics_accumulator_new_output):
                                        new_metrics_set = list(set(metrics_to_check).union(set(kafka_metrics_accumulator_new_output)-set(metrics_to_check)))
                                    else:
                                        new_metrics_set = list(set(kafka_metrics_accumulator_new_output).union(set(metrics_to_check)-set(kafka_metrics_accumulator_new_output)))

                                    logging.debug("delete_old_and_update_new(ransim_datasource_db.topic_name, topic_name_to_check, new_metrics_set, new_other_output_metrics)")
                                    logging.debug(new_metrics_set)
                                    logging.debug(new_other_output_metrics)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.delete_old_and_update_new(ransim_datasource_db.topic_name, topic_name_to_check, new_metrics_set, new_other_output_metrics)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.info(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                            else: ## there are other kafka publishing in the old topic
                                if not kafka_metrics_accumulator_new_output: ## if there is not a kafka output publishing in the new topic --> TESTED
                                    old_metrics_to_delete = list(set(ransim_datasource_db.metrics) - set(kafka_metrics_accumulator_old_output))

                                    logging.debug("mgmt.update_old_and_create_new(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, metrics_to_check, new_other_output_metrics)")
                                    logging.debug(metrics_to_check) ## to create the new one
                                    logging.debug(old_metrics_to_delete)
                                    logging.debug(new_other_output_metrics)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.update_old_and_create_new(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, metrics_to_check, new_other_output_metrics)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                                else: ## there is at least another output publishing in the new topic --> TESTED
                                    if len(metrics_to_check) > len(kafka_metrics_accumulator_new_output):
                                        new_metrics_set = list(set(metrics_to_check).union(set(kafka_metrics_accumulator_new_output)-set(metrics_to_check)))
                                    else:
                                        new_metrics_set = list(set(kafka_metrics_accumulator_new_output).union(set(metrics_to_check)-set(kafka_metrics_accumulator_new_output)))
                                    old_metrics_to_delete = list(set(ransim_datasource_db.metrics) - set(kafka_metrics_accumulator_old_output))
                                    if len(other_outputs_metrics_accumulator) > len(metrics_to_check):
                                        new_other_output_metrics = list(set(metrics_to_check).union(set(other_outputs_metrics_accumulator) - set(metrics_to_check)))
                                    else:
                                        new_other_output_metrics = list(set(other_outputs_metrics_accumulator).union(set(metrics_to_check) - set(other_outputs_metrics_accumulator)))

                                    logging.debug("update_old_and_update_new(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, new_metrics_set, new_other_output_metrics)")
                                    logging.debug(old_metrics_to_delete)
                                    logging.debug(new_metrics_set)
                                    logging.debug(new_other_output_metrics)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.update_old_and_update_new(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, new_metrics_set, new_other_output_metrics)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                        else:
                            ## changes just topic_name
                            logging.info("Requested just topic name change, not changing the metrics")
                            kafka_metrics_accumulator_old_output = []
                            kafka_metrics_accumulator_new_output = []

                            for ransim_datasource in ransim_datasources_list:
                                if ransim_datasource.topic_name == topic_name_to_check:
                                    kafka_metrics_accumulator_new_output = list(set(kafka_metrics_accumulator_new_output).union(set(ransim_datasource.metrics)))
                                else:
                                    if ransim_datasource.topic_name == ransim_datasource_db.topic_name:
                                        kafka_metrics_accumulator_old_output = list(set(kafka_metrics_accumulator_old_output).union(set(ransim_datasource.metrics)))

                            logging.debug("kakfa_old_out_acc = {}".format(kafka_metrics_accumulator_old_output))
                            logging.debug("kafka_new_out_acc = {}".format(kafka_metrics_accumulator_new_output))            

                            if not kafka_metrics_accumulator_old_output: ## if there are not other kafka publishing in the same old topic
                                if not kafka_metrics_accumulator_new_output: ## if there is not a kafka output publishing in the new topic --> TESTED
                                    logging.info("There are no datasources publishing in the old topic and no one publishing in the new one")

                                    logging.info("change_topic_name(ransim_datasource_db.topic_name, topic_name_to_check)")
                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.change_topic_name(ransim_datasource_db.topic_name, topic_name_to_check)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                                else: ## there is at least another kafka publishing in the same new topic  --> TESTED
                                    logging.info("There are no other datasources publishing in the old topic but at least one publishing in the new onw")
                                    if len(metrics_to_check) > len(kafka_metrics_accumulator_new_output):
                                        new_metrics_set = list(set(metrics_to_check).union(set(kafka_metrics_accumulator_new_output)-set(metrics_to_check)))
                                    else:
                                        new_metrics_set = list(set(kafka_metrics_accumulator_new_output).union(set(metrics_to_check)-set(kafka_metrics_accumulator_new_output)))

                                    logging.debug("delete_old_and_update_new(ransim_datasource_db.topic_name, topic_name_to_check, new_metrics_set)")
                                    logging.debug(new_metrics_set)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.delete_old_and_update_new(ransim_datasource_db.topic_name, topic_name_to_check, new_metrics_set)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                            else: ## there are other kafka publishing in the old topic
                                logging.info("There were other datasources publishing in the old topic")
                                old_metrics_to_delete = list(set(metrics_to_check) - set(kafka_metrics_accumulator_old_output)) 
                                if not kafka_metrics_accumulator_new_output: ## if there is not a kafka output publishing in the new topic --> TESTED

                                    logging.debug("update_old_and_create_new_just_topic(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, metrics_to_check)")
                                    logging.debug(old_metrics_to_delete)
                                    logging.debug(metrics_to_check)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.update_old_and_create_new_just_topic(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, metrics_to_check)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                                else: ## there is at least another output publishing in the new topic --> TESTED
                                    if len(metrics_to_check) > len(kafka_metrics_accumulator_new_output):
                                        new_metrics_set = list(set(metrics_to_check).union(set(kafka_metrics_accumulator_new_output)-set(metrics_to_check)))
                                    else:
                                        new_metrics_set = list(set(kafka_metrics_accumulator_new_output).union(set(metrics_to_check)-set(kafka_metrics_accumulator_new_output)))

                                    logging.debug("update_old_and_update_new_just_topic(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, new_metrics_set)")
                                    logging.debug(old_metrics_to_delete)
                                    logging.debug(new_metrics_set)

                                    # check the configuration file existence to perform an eventual stop early
                                    if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                                        # critical section
                                        with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                            logging.info("Critical Section accessed")
                                            new_config_dictionary = mgmt.update_old_and_update_new_just_topic(ransim_datasource_db.topic_name, topic_name_to_check, old_metrics_to_delete, new_metrics_set)
                                            logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                            try:
                                                with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                                    file.write(toml.dumps(new_config_dictionary))
                                                    logging.info("Configuration wrote")
                                                    # the commit must be done just in case the configuration file is updated successfully
                                                    session.commit()
                                                    logging.info("Committed to DB")
                                                    service_updated_flag = docker.update_telegraf_ransim_service()
                                                    if not service_updated_flag:
                                                        return Response(
                                                            "Error while restarting the mp_telegraf_ransim service\n",
                                                            status=500)
                                                    else:
                                                        return Response(
                                                            "{}".format(ransim_datasource_db), status=200)
                                            except Exception as e:
                                                logging.error(e)
                                                return Response(
                                                    "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                                    status=500)
                                    # if the configuration file does not exist
                                    else:
                                        return Response(
                                            "Error Accessing the telegraf.conf file, check the path\n",
                                            status=500)

                    else:  ## changes just metrics 
                        logging.info("Changing just metrics, not topic")
                        kafka_metrics_accumulator_old_output = []
                        other_outputs_metrics_accumulator = []
                        for ransim_datasource in ransim_datasources_list:

                            if ransim_datasource.topic_name == ransim_datasource_db.topic_name:
                                kafka_metrics_accumulator_old_output = list(set(kafka_metrics_accumulator_old_output).union(set(ransim_datasource.metrics)))

                            other_outputs_metrics_accumulator = list(set(other_outputs_metrics_accumulator).union(set(ransim_datasource.metrics)))

                        logging.debug("kafka_old_out_acc = {}".format(kafka_metrics_accumulator_old_output))
                        logging.debug("other_out_acc = {}".format(other_outputs_metrics_accumulator))

                        new_other_output_metrics = list(set(metrics_to_check).union(set(other_outputs_metrics_accumulator) - set(metrics_to_check)))
                        new_old_metrics_set = list(set(metrics_to_check).union(set(kafka_metrics_accumulator_old_output)-set(metrics_to_check)))

                        logging.debug("update_old_and_update_other_outputs(ransim_datasource_db.topic_name, new_old_metrics_set, new_other_output_metrics)")
                        logging.debug(new_old_metrics_set)
                        logging.debug(new_other_output_metrics)

                        # check the configuration file existence to perform an eventual stop early
                        if os.path.isfile(conf.configuration['PATHS']['telegraf_ransim_conf_path']):
                            # critical section
                            with filelock.FileLock("telegraf_ransim_conf.lock"):                
                                logging.info("Critical Section accessed")
                                new_config_dictionary = mgmt.update_old_and_update_other_outputs(ransim_datasource_db.topic_name, new_old_metrics_set, new_other_output_metrics)
                                logging.info("Produced new telegraf configuration after addition, writing it on the file")
                                try:
                                    with open(conf.configuration['PATHS']['telegraf_ransim_conf_path'],'w') as file:
                                        file.write(toml.dumps(new_config_dictionary))
                                        logging.info("Configuration wrote")
                                        # the commit must be done just in case the configuration file is updated successfully
                                        session.commit()
                                        logging.info("Committed to DB")
                                        service_updated_flag = docker.update_telegraf_ransim_service()
                                        if not service_updated_flag:
                                            return Response(
                                                "Error while restarting the mp_telegraf_ransim service\n",
                                                status=500)
                                        else:
                                            return Response(
                                                "{}".format(ransim_datasource_db), status=200)
                                except Exception as e:
                                    logging.error(e)
                                    return Response(
                                        "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_ransim_conf_path']),
                                        status=500)
                        # if the configuration file does not exist
                        else:
                            return Response(
                                "Error Accessing the telegraf.conf file, check the path\n",
                                status=500)
        else:
            # datasource not found
            return Response("Ransim datasource with id {} Not Found\n".format(id_),status=404)