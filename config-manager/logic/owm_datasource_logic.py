import logging
import filelock
import toml
import os.path
import uuid
from flask import Response

from swagger_server.models.datasource_type import DatasourceType
from swagger_server.models.owm_datasource import OwmDatasource  # noqa: E501
from swagger_server.models.owm_metrics import OwmMetrics
from sqlalchemy import select

import logic.db_creation as db
from db_models.owm_datasource_db import OwmDatasourceDB

import utils.owm_collector_configuration_mgmt as mgmt
import utils.metric_list_mgmt as lsmgmt
import utils.docker_service_mgmt as docker

import utils.configuration_mgmt as conf


def add_owm_datasource(owm_datasource_dict):
    with db.Session() as session:
        # execute query in order to understand id there is already a datasource monitoring the city_uid requested (independently from the topic name)
        owm_datasource_db = session.query(OwmDatasourceDB).filter(OwmDatasourceDB.city_uid == owm_datasource_dict['city-uid']).first()        
        # if the city is not monitored yet, add the datasource to the DB
        if owm_datasource_db is None:            
            if "metrics" in owm_datasource_dict.keys():
                # cleaning the metrics list from duplicates (in case 'metrics' is not specified, it will monitor all the OWM metrics)
                owm_datasource_dict['metrics'] = lsmgmt.remove_duplicates(owm_datasource_dict['metrics'])            
            if "topic-name" not in owm_datasource_dict.keys():
                owm_datasource_dict['topic-name'] = ""
            # creation of the new object to be inserted in the db
            new_owm_datasource_db = OwmDatasourceDB(OwmDatasource.from_dict(owm_datasource_dict))
            session.add(new_owm_datasource_db)
            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_owm_conf_path']):                
                # critical section
                with filelock.FileLock("telegraf_owm_conf.lock"):                
                    logging.info("Critical Section accessed")                
                    new_config_dictionary = mgmt.add_owm_datasource_to_config(str(new_owm_datasource_db.city_uid),new_owm_datasource_db.metrics,owm_datasource_dict['topic-name'])                    
                    logging.info("Produced new telegraf configuration after addition, writing it on the file")                    
                    try:
                        with open(conf.configuration['PATHS']['telegraf_owm_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()                            
                            logging.info("Committed to DB")
                            service_updated_flag = docker.update_telegraf_owm_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_owm service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(new_owm_datasource_db.id),
                                    status=200)            
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_owm_conf_path']),
                            status=500)                    
            # if the configuration file does not exist
            else:
                return Response(
                    "Error Accessing the OWM telegraf.conf file\n",
                     status=500)

        # if the city is already monitored returns a None object to the controller which will reply with a 4XX code
        return Response(
            "OpenWeatherMap datasource monitoring the requested city_uid is already present, please perform a put on the related id in order to modify it.\n",
            status=409)

def delete_owm_datasource(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        #check id the owm datasource with the requested id exists
        owm_datasource_db = session.query(OwmDatasourceDB).filter(OwmDatasourceDB.id == id).first()        
        # if the requested owm datasource exists
        if owm_datasource_db is not None:
            session.delete(owm_datasource_db)
            # check the configuration file existence to perform an eventual stop early
            #if os.path.isfile("config/telegraf_owm/telegraf.conf"):
            if os.path.isfile(conf.configuration['PATHS']['telegraf_owm_conf_path']):
                # critical section
                with filelock.FileLock("telegraf_owm_conf.lock"):                
                    logging.info("Critical Section accessed")
                    new_config_dictionary = mgmt.delete_owm_datasource_from_config(str(owm_datasource_db.city_uid))
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_owm_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            service_updated_flag = docker.update_telegraf_owm_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_owm service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(owm_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_owm_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_owm_conf_path']),
                     status=500)        
        # if the requested owm datasource has been not found                     
        return Response(
            "OpenWeatherMap datasource with id {} Not Found\n".format(id_),
            status=404)

def delete_all_owm_datasources():
    statement = select(OwmDatasourceDB)    
    with db.Session() as session:
        owm_datasources_db_list = session.query(OwmDatasourceDB)
        if owm_datasources_db_list is not None:
            deleted_owm_datasource_uids=[]
            for owm_datasource_db in owm_datasources_db_list:
                deleted_owm_datasource_uids.append(owm_datasource_db.id)
                session.delete(owm_datasource_db)
            if os.path.isfile(conf.configuration['PATHS']['telegraf_owm_conf_path']):
                with filelock.FileLock("telegraf_owm_conf.lock"):                
                    logging.info("Critical Section accessed")
                    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_owm_conf_path'])
                    new_config_dictionary = mgmt.delete_all_owm_datasources_from_config()
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_owm_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            service_updated_flag = docker.update_telegraf_owm_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_owm service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(deleted_owm_datasource_uids),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_owm_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_owm_conf_path']),
                     status=500)
        return Response(
            "No OpenWeatherMap datasources Found\n",
            status=404)
        
def get_all_owm_datasources(): 
    statement = select(OwmDatasourceDB)
    
    with db.Session() as session:
        owm_datasource_list =[]
        owm_datasources_db_list = session.query(OwmDatasourceDB)
        
        # if there is at least one owm_datasource, creates a non empty list
        if owm_datasources_db_list.first() is not None:
            for row in session.execute(statement):
                owm_datasource_db = row._mapping['OwmDatasourceDB']
                owm_datasource_list.append(OwmDatasource(DatasourceType.OPENWEATHERMAP, owm_datasource_db.id, owm_datasource_db.city_uid, owm_datasource_db.metrics, owm_datasource_db.topic_name))
        
        return owm_datasource_list

def get_owm_datasource_by_id(id_):
    
    with db.Session() as session:
        id = uuid.UUID(id_)
        owm_datasource_db = session.query(OwmDatasourceDB).filter(OwmDatasourceDB.id == id).first()
        if owm_datasource_db is not None:
            return OwmDatasource(DatasourceType.OPENWEATHERMAP, owm_datasource_db.id, owm_datasource_db.city_uid, owm_datasource_db.metrics, owm_datasource_db.topic_name)    

def update_owm_datasource_by_id(id_, owm_datasource_dict):

    # check if the user is trying to change some owm-datasource fields not allowed
    if any (k in owm_datasource_dict for k in ("id","datasource-type","city-uid")):
        return Response(
            "Cannot update fields different from metrics and topic_name for OpenWeatherMap datasources",
            status=400)
    if not(any(k in owm_datasource_dict for k in ("metrics","topic-name"))):
        return Response(
            "Neither metrics nor topic-name have been specified for updating the OpenWeatherMap datasource",
            status=400)

    with db.Session() as session:
        id = uuid.UUID(id_)
        # query the db in order to find the OWMDatasource with the requested id
        owm_datasource_db = session.query(OwmDatasourceDB).filter(OwmDatasourceDB.id == id).first()
        
        if owm_datasource_db is not None:
            
            # if metrics is not present in the body, the field is left unchanged
            if "metrics" in owm_datasource_dict.keys():
                
                # if the list of OWM Metrics is empty, monitor all the possible metrics
                if not owm_datasource_dict['metrics']:
                    owm_datasource_db.metrics = [OwmMetrics.CLOUDINESS, OwmMetrics.HUMIDITY, OwmMetrics.PRESSURE, OwmMetrics.RAIN, OwmMetrics.TEMPERATURE, OwmMetrics.WIND_DEGREES, OwmMetrics.WIND_SPEED]
                else:
                    # remove duplicates from the metrics list
                    owm_datasource_dict['metrics'] = lsmgmt.remove_duplicates(owm_datasource_dict['metrics'])
                    owm_datasource_db.metrics = owm_datasource_dict['metrics']
            
            # if topic-name is not present in the body, the field is left unchanged
            if "topic-name" in owm_datasource_dict.keys():
                
                # if topic-name is empty the topic name follows the predefined pattern
                if not owm_datasource_dict['topic-name']:
                    owm_datasource_db.topic_name = "weather_{}".format(owm_datasource_db.city_uid)
                    owm_datasource_dict['topic-name'] = ""
                else:
                    owm_datasource_db.topic_name = owm_datasource_dict['topic-name']
            

            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_owm_conf_path']):
                
                # critical section
                with filelock.FileLock("telegraf_owm_conf.lock"):                
                    logging.info("Critical Section accessed")

                    new_config_dictionary = mgmt.update_owm_datasource_from_config(str(owm_datasource_db.city_uid),owm_datasource_db.metrics,owm_datasource_db.topic_name)
                    
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                
                    try:
                        ## TODO: need of a mechanism (conf file) from which specify the same path of the isfile check above
                        ##       if the file does not exist, opening the file in 'w' mode will create it

                        with open(conf.configuration['PATHS']['telegraf_owm_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
            
                            service_updated_flag = docker.update_telegraf_owm_service()

                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_owm service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(owm_datasource_db), status=200)

                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}}\n".format(conf.configuration['PATHS']['telegraf_owm_conf_path']),
                            status=500)
        
        # datasource not found
        return Response(
            "OpenWeatherMap datasource with id {} Not Found\n".format(id_),
            status=404)