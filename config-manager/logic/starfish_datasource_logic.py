import logging
import filelock
import toml
import os.path
import uuid
from flask import Response
import requests

from sqlalchemy import select
import logic.db_creation as db
from db_models.starfish_datasource_db import StarfishDatasourceDB

from swagger_server.models.datasource_type import DatasourceType
from swagger_server.models.starfish_datasource import StarfishDatasource  # noqa: E501
from swagger_server.models.starfish_metrics import StarfishMetrics

import utils.metric_list_mgmt as lsmgmt
import utils.docker_service_mgmt as docker
import utils.common as common

import utils.starfish_collector_configuration_mgmt as mgmt

import utils.configuration_mgmt as conf


def get_starfish_api_token():
    return requests.post(
        '{}/token'.format(conf.configuration['APP']['starfish_default_api_root']),
        headers={'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded','Content-Length': '58'},
        data={'grant_type': 'password', 'username': conf.configuration['APP']['starfish_api_username'],
              'password': conf.configuration['APP']['starfish_api_password']}).json().get('access_token')


def add_starfish_datasource(starfish_datasource_dict):
    with db.Session() as session:

        if "topic-name" not in starfish_datasource_dict.keys():
            starfish_datasource_dict['topic-name'] = ""

        # just one Datasource for each network slice is allowed
        starfish_datasource_db = session.query(StarfishDatasourceDB).filter(
            StarfishDatasourceDB.network_slice_subnet_uid == starfish_datasource_dict["network-slice-subnet-uid"]).first()

        # if there is not a datasource monitoring the same slice, add the datasource to the DB
        if starfish_datasource_db is None:

            if "metrics" in starfish_datasource_dict.keys():
                # if it is empty it is managed inside the StarfishDatasourceDB constructor
                # cleaning the metrics list from duplicates (in case 'metrics' is not specified, it will monitor all the starfish metrics)
                starfish_datasource_dict['metrics'] = lsmgmt.remove_duplicates(starfish_datasource_dict['metrics'])
            else:
                starfish_datasource_dict['metrics'] = [StarfishMetrics.TRAFFIC_COUNTERS,
                                                       StarfishMetrics.TRAFFIC_FORMATS, StarfishMetrics.KPIS]

            # creation of the new object to be inserted in the db
            new_starfish_datasource_db = StarfishDatasourceDB(StarfishDatasource.from_dict(starfish_datasource_dict))
            session.add(new_starfish_datasource_db)

            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_starfish_conf_path']):
                # critical section
                with filelock.FileLock("telegraf_starfish_conf.lock"):
                    logging.info("Critical Section accessed")

                    # token request in case this datasource is the only one in the system
                    if session.query(StarfishDatasourceDB).count() == 1:
                        try:
                            if new_starfish_datasource_db.api_root.startswith("http://10.5.6.128:8000"):
                                api_token = get_starfish_api_token()
                                logging.debug("Obtained Starfish auth token: {}".format(api_token))
                            else:
                                api_token = "starfish_stub"
                            ## add with api_token
                            new_config_dictionary = mgmt.add_first_starfish_datasource_to_config(api_token, new_starfish_datasource_db.end_to_end_network_slice_uid, new_starfish_datasource_db.network_slice_subnet_uid, new_starfish_datasource_db.api_root,new_starfish_datasource_db.interval,new_starfish_datasource_db.metrics, starfish_datasource_dict['topic-name'])
                        except Exception as e:
                            logging.error(e)
                            return Response(
                                "Cannot Obtain Starfish Token. Check the request values and the configuration because of\n{}\n".format(e),
                                status=500)
                    else:
                        # add WITHOUT api_token in case there are already monitored slices
                        new_config_dictionary = mgmt.add_starfish_datasource_to_config(new_starfish_datasource_db.end_to_end_network_slice_uid, new_starfish_datasource_db.network_slice_subnet_uid, new_starfish_datasource_db.api_root, new_starfish_datasource_db.interval,new_starfish_datasource_db.metrics,starfish_datasource_dict['topic-name'])
                    logging.info("Produced new telegraf configuration after addition, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_starfish_conf_path'], 'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            logging.info("Committed to DB")
                            service_updated_flag = docker.update_telegraf_starfish_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_starfish service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(
                                        new_starfish_datasource_db.id),
                                    status=200)
                            
                            return Response(
                                    "{}".format(new_starfish_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\nReason: {}".format(
                                conf.configuration['PATHS']['telegraf_starfish_conf_path'], e),
                            status=500)

            # if the configuration file does not exist
            else:
                return Response(
                    "Error Accessing the telegraf.conf file, check the path\n",
                    status=500)
        else:
            # if the slice is already monitored returns a None object to the controller which will reply with a 4XX code
            return Response(
                "Starfish datasource monitoring the requested network-slice-subnet-uid is already present, please perform a put on the related id in order to modify it.\n",
                status=409)


def delete_starfish_datasource(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        starfish_datasource_db = session.query(StarfishDatasourceDB).filter(StarfishDatasourceDB.id == id).first()
        if starfish_datasource_db is not None:
            session.delete(starfish_datasource_db)
            # check the configuration file existence to perform an eventual stop early

            if os.path.isfile(conf.configuration['PATHS']['telegraf_starfish_conf_path']):

                # critical section
                with filelock.FileLock("telegraf_starfish_conf.lock"):
                    logging.info("Critical Section accessed")
                    new_config_dictionary = mgmt.delete_starfish_datasource_from_config(starfish_datasource_db.network_slice_subnet_uid)
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_starfish_conf_path'], 'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            service_updated_flag = docker.update_telegraf_starfish_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_starfish service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(starfish_datasource_db.id),
                                    status=200)
                            
                            return Response(
                                    "{}".format(new_starfish_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(
                                conf.configuration['PATHS']['telegraf_starfish_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the telegraf.conf file at {}\n".format(
                        conf.configuration['PATHS']['telegraf_starfish_conf_path']),
                    status=500)
        # if the requested starfish datasource has been not found                     
        return Response(
            "Starfish datasource with id {} Not Found\n".format(id_),
            status=404)


def delete_all_starfish_datasources():
    statement = select(StarfishDatasourceDB)
    
    with db.Session() as session:
        starfish_datasources_list =[]
        starfish_datasources_db_list = session.query(StarfishDatasourceDB)
        if starfish_datasources_db_list is not None:
            starfish_datasources_db_uids = []
            for starfish_datasource_db in starfish_datasources_db_list:
                starfish_datasources_db_uids.append(starfish_datasource_db.id)
                session.delete(starfish_datasource_db)
            if os.path.isfile(conf.configuration['PATHS']['telegraf_starfish_conf_path']):
                with filelock.FileLock("telegraf_starfish_conf.lock"):                
                    logging.info("Critical Section accessed")
                    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_starfish_conf_path'])
                    new_config_dictionary = mgmt.delete_all_starfish_datasources_from_config()
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_starfish_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            service_updated_flag = docker.update_telegraf_starfish_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_starfish service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(starfish_datasources_db_uids),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_starfish_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_starfish_conf_path']),
                     status=500)
        return Response(
            "No Starfish datasources Found\n",
            status=404)


def get_all_starfish_datasources():
    statement = select(StarfishDatasourceDB)
    with db.Session() as session:
        starfish_datasource_list = []
        starfish_datasources_db_list = session.query(StarfishDatasourceDB)
        # if there is at least one starfish_datasource, creates a non-empty list
        if starfish_datasources_db_list.first() is not None:
            for row in session.execute(statement):
                starfish_datasource_db = row._mapping['StarfishDatasourceDB']
                starfish_datasource_list.append(StarfishDatasource(DatasourceType.STARFISH, starfish_datasource_db.id,
                                                                   starfish_datasource_db.api_root,
                                                                   starfish_datasource_db.interval,
                                                                   starfish_datasource_db.end_to_end_network_slice_uid, 
                                                                   starfish_datasource_db.network_slice_subnet_uid,
                                                                   starfish_datasource_db.metrics,
                                                                   starfish_datasource_db.topic_name))

        return starfish_datasource_list


def get_starfish_datasource_by_id(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        starfish_datasource_db = session.query(StarfishDatasourceDB).filter(StarfishDatasourceDB.id == id).first()
        if starfish_datasource_db is not None:
            return StarfishDatasource(DatasourceType.STARFISH, starfish_datasource_db.id,
                                      starfish_datasource_db.api_root, starfish_datasource_db.interval,
                                      starfish_datasource_db.end_to_end_network_slice_uid, 
                                      starfish_datasource_db.network_slice_subnet_uid, starfish_datasource_db.metrics,
                                      starfish_datasource_db.topic_name)


def update_starfish_datasource_by_id(id_, starfish_datasource_dict):
    # check if the user is trying to change some starfish-datasource fields not allowed
    if any(k in starfish_datasource_dict for k in ("id", "datasource-type", "end-to-end-network-slice-uid", "network-slice-subnet-uid")):
        return Response(
            "Cannot update fields different from api-root, interval, metrics and topic_name for Starfish datasources",
            status=400)
    if not (any(k in starfish_datasource_dict for k in ("api-root", "interval", "metrics", "topic-name"))):
        return Response(
            "None of the Starfish editable parameters have been specified for updating the Starfish datasource, dict: {}".format(
                starfish_datasource_dict),
            status=400)

    with db.Session() as session:
        id = uuid.UUID(id_)
        # query the db in order to find the Starfish Datasource with the requested id
        starfish_datasource_db = session.query(StarfishDatasourceDB).filter(StarfishDatasourceDB.id == id).first()
        if starfish_datasource_db is not None:

            # if topic-name is not present in the body, the field is left unchanged
            if "topic-name" in starfish_datasource_dict.keys():
                # if topic-name is empty the topic name follows the predefined pattern
                if not starfish_datasource_dict['topic-name']:
                    starfish_datasource_db.topic_name = "starfish_{}".format(starfish_datasource_db.network_slice_id)
                else:
                    starfish_datasource_db.topic_name = starfish_datasource_dict['topic-name']

            # if metrics is not present in the body, the field is left unchanged
            if "metrics" in starfish_datasource_dict.keys():

                # if the list of starfish Metrics is empty, monitor all the possible metrics
                if not starfish_datasource_dict['metrics']:
                    starfish_datasource_db.metrics = [StarfishMetrics.TRAFFIC_COUNTERS, StarfishMetrics.TRAFFIC_FORMATS,
                                                      StarfishMetrics.KPIS]
                else:
                    # remove duplicates from the metrics list
                    starfish_datasource_dict['metrics'] = lsmgmt.remove_duplicates(starfish_datasource_dict['metrics'])
                    starfish_datasource_db.metrics = starfish_datasource_dict['metrics']

            # if api-root is not present in the body, the field is left unchanged
            if "api-root" in starfish_datasource_dict.keys():
                if starfish_datasource_dict['api-root']:
                    starfish_datasource_db.api_root = starfish_datasource_dict['api-root']
                else:
                    starfish_datasource_db.api_root = conf.configuration['APP']['starfish_default_api_root']

            # if interval is not present in the body, the field is left unchanged
            if "interval" in starfish_datasource_dict.keys():
                if not starfish_datasource_dict["interval"]:
                    starfish_datasource_db.interval = conf.configuration['APP']['starfish_default_interval']
                else:
                    starfish_datasource_db.interval = starfish_datasource_dict['interval']

            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_starfish_conf_path']):

                # critical section
                with filelock.FileLock("telegraf_starfish_conf.lock"):
                    logging.info("Critical Section accessed")
                    new_config_dictionary = mgmt.update_starfish_datasource_from_config(starfish_datasource_db.end_to_end_network_slice_uid, starfish_datasource_db.network_slice_subnet_uid, starfish_datasource_db.api_root, starfish_datasource_db.interval,starfish_datasource_db.metrics,starfish_datasource_db.topic_name)
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_starfish_conf_path'], 'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            session.commit()
                            service_updated_flag = docker.update_telegraf_starfish_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_starfish service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(starfish_datasource_db),
                                    status=200)
                            
                            return Response(
                                    "{}".format(new_starfish_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(
                                conf.configuration['PATHS']['telegraf_starfish_conf_path']),
                            status=500)
        # datasource not found
        return Response(
            "Starfish datasource with id {} Not Found\n".format(id_),
            status=404)
