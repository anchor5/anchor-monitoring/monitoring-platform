import logging
import filelock
import toml
import os.path
import uuid
from flask import Response
from sqlalchemy import select

from swagger_server.models.datasource_type import DatasourceType
from swagger_server.models.nwdaf_datasource import NwdafDatasource  # noqa: E501
from swagger_server.models.nwdaf_metrics import NwdafMetrics

import logic.db_creation as db
from db_models.nwdaf_datasource_db import NwdafDatasourceDB

import utils.nwdaf_collector_configuration_mgmt as mgmt
import utils.metric_list_mgmt as lsmgmt
import utils.docker_service_mgmt as docker
import utils.common as common

import utils.configuration_mgmt as conf


def add_nwdaf_datasource(nwdaf_datasource_dict):

    with db.Session() as session:
        if not nwdaf_datasource_dict["network-slice-subnet-uid"]:
            network_slice_subnet_uid = "anyslice"
            end_to_end_network_slice_uid = "anyslice"
        else:
            network_slice_subnet_uid = nwdaf_datasource_dict["network-slice-subnet-uid"]
            end_to_end_network_slice_uid = nwdaf_datasource_dict["end-to-end-network-slice-uid"]

        flow_id = []
        if "flow-id" not in nwdaf_datasource_dict.keys() or not nwdaf_datasource_dict["flow-id"]:
            flow_id += "anyflow"
        else:
            flow_id += nwdaf_datasource_dict["flow-id"]
        
        ## TODO: logic in order to manage flows [not already provided by the current NWDAF implementation]
        # execute query in order to understand id there is already a datasource monitoring the slice-id (or the anyslice)
        # and the flow-id (or anyflow) requested (independently from the topic name)
        nwdaf_datasource_db = session.query(NwdafDatasourceDB).filter(NwdafDatasourceDB.network_slice_subnet_uid == network_slice_subnet_uid).first()
        
        # if the slice and the flow are not monitored yet, add the datasource to the DB
        ## TODO: see how to manage eventually flows
        if nwdaf_datasource_db is None:

            if "metrics" in nwdaf_datasource_dict.keys():
                # cleaning the metrics list from duplicates (in case 'metrics' is not specified, it will monitor all the nwdaf metrics)
                nwdaf_datasource_dict['metrics'] = lsmgmt.remove_duplicates(nwdaf_datasource_dict['metrics'])
            
            if "topic-name" not in nwdaf_datasource_dict.keys():
                nwdaf_datasource_dict['topic-name'] = ""

            # creation of the new object to be inserted in the db
            new_nwdaf_datasource_db = NwdafDatasourceDB(NwdafDatasource.from_dict(nwdaf_datasource_dict))
            session.add(new_nwdaf_datasource_db)

            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']):
                # critical section
                with filelock.FileLock("telegraf_nwdaf_conf.lock"):                
                    logging.info("Critical Section accessed")
                    

                    ## NOTE: the configuration is updated with the right map value just in case topic_name is "" in case we want to exploit the default name pattern
                    new_config_dictionary = mgmt.add_nwdaf_datasource_to_config(new_nwdaf_datasource_db.end_to_end_network_slice_uid, new_nwdaf_datasource_db.network_slice_subnet_uid,new_nwdaf_datasource_db.flow_id,new_nwdaf_datasource_db.api_root,new_nwdaf_datasource_db.interval,new_nwdaf_datasource_db.metrics,nwdaf_datasource_dict['topic-name'])
                    
                    logging.info("Produced new telegraf configuration after addition, writing it on the file")

                    try:

                        with open(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")

                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            
                            logging.info("Committed to DB")
                            
                            service_updated_flag = docker.update_telegraf_nwdaf_service()

                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_nwdaf service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(new_nwdaf_datasource_db.id),
                                    status=200)
                            
                            return Response(
                                    "{}".format(new_nwdaf_datasource_db.id),
                                    status=200)

                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {} due to the following exception\n{}\n".format(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'],e),
                            status=500)        

            # if the configuration file does not exist
            else:
                return Response(
                    "Error Accessing the telegraf.conf file, check the path\n",
                     status=500)
        else: 
            # if the slice is already monitored returns a None object to the controller which will reply with a 4XX code
            return Response(
                "NWDAF datasource monitoring the requested slice_id is already present, please perform a put on the related id in order to modify it.\n",
                status=409)

def delete_nwdaf_datasource(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        nwdaf_datasource_db = session.query(NwdafDatasourceDB).filter(NwdafDatasourceDB.id == id).first()
        logging.debug("deleting: {}".format(nwdaf_datasource_db))
        if nwdaf_datasource_db is not None:
            session.delete(nwdaf_datasource_db)
            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']):

                # critical section
                with filelock.FileLock("telegraf_nwdaf_conf.lock"):                
                    logging.info("Critical Section accessed")
                    new_config_dictionary = mgmt.delete_nwdaf_datasource_from_config(nwdaf_datasource_db.network_slice_subnet_uid)
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            
                            service_updated_flag = docker.update_telegraf_nwdaf_service()
                            
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_nwdaf service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(nwdaf_datasource_db.id),
                                    status=200)
                            
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the NWDAF telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']),
                     status=500)
        
        # if the requested nwdaf datasource has been not found                     
        else:
            return Response(
            "NWDAF datasource with id {} Not Found\n".format(id_),
            status=404)

def delete_all_nwdaf_datasources():
    statement = select(NwdafDatasourceDB)
    
    with db.Session() as session:
        nwdaf_datasources_db_list = session.query(NwdafDatasourceDB)
        if nwdaf_datasources_db_list is not None:
            deleted_nwdaf_datasources_uids=[]
            for nwdaf_datasource_db in nwdaf_datasources_db_list:
                deleted_nwdaf_datasources_uids.append(nwdaf_datasource_db.id)
                session.delete(nwdaf_datasource_db)
            if os.path.isfile(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']):
                with filelock.FileLock("telegraf_nwdaf_conf.lock"):                
                    logging.info("Critical Section accessed")
                    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'])
                    new_config_dictionary = mgmt.delete_all_nwdaf_datasources_from_config()
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()

                            service_updated_flag = docker.update_telegraf_nwdaf_service()

                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_nwdaf service\n",
                                    status=500)
                            else:
                                return Response(
                                    "{}".format(deleted_nwdaf_datasources_uids),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the telegraf.conf file in {}\n".format(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']),
                            status=500)
            else:
                return Response(
                    "Error Accessing the telegraf.conf file at {}\n".format(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']),
                     status=500)
        return Response(
            "No NWDAF datasources Found\n",
            status=404)

def get_all_nwdaf_datasources():
    statement = select(NwdafDatasourceDB)
    with db.Session() as session:
        nwdaf_datasource_list =[]
        nwdaf_datasources_db_list = session.query(NwdafDatasourceDB)
        # if there is at least one nwdaf_datasource, creates a non empty list
        if nwdaf_datasources_db_list.first() is not None:
            for row in session.execute(statement):
                nwdaf_datasource_db = row._mapping['NwdafDatasourceDB']
                common.fix_json_array(nwdaf_datasource_db, "flow_id")
                nwdaf_datasource_list.append(NwdafDatasource(DatasourceType.NWDAF, nwdaf_datasource_db.id, nwdaf_datasource_db.end_to_end_network_slice_uid, nwdaf_datasource_db.network_slice_subnet_uid, nwdaf_datasource_db.flow_id, nwdaf_datasource_db.api_root, nwdaf_datasource_db.interval, nwdaf_datasource_db.metrics, nwdaf_datasource_db.topic_name))
        
        return nwdaf_datasource_list

def get_nwdaf_datasource_by_id(id_):
    with db.Session() as session:
        id = uuid.UUID(id_)
        nwdaf_datasource_db = session.query(NwdafDatasourceDB).filter(NwdafDatasourceDB.id == id).first()
        if nwdaf_datasource_db is not None:
            common.fix_json_array(nwdaf_datasource_db, "flow_id")
            return NwdafDatasource(DatasourceType.NWDAF, nwdaf_datasource_db.id, nwdaf_datasource_db.end_to_end_network_slice_uid, nwdaf_datasource_db.network_slice_subnet_uid, nwdaf_datasource_db.flow_id, nwdaf_datasource_db.api_root, nwdaf_datasource_db.interval, nwdaf_datasource_db.metrics, nwdaf_datasource_db.topic_name)    

def update_nwdaf_datasource_by_id(id_, nwdaf_datasource_dict):
    # check if the user is trying to change some nwdaf-datasource fields not allowed
    if any (k in nwdaf_datasource_dict for k in ("id","datasource-type","end-to-end-network-slice-uid", "network-slice-subnet-uid","flow-id")):
        return Response(
            "Cannot update fields different from api-root, interval, metrics and topic_name for NWDAF datasources",
            status=400)
    if not(any(k in nwdaf_datasource_dict for k in ("api-root","interval","metrics","topic-name"))):
        return Response(
            "None of the NWDAF updatable parameters have been specified for updating the NWDAF datasource",
            status=400)

    with db.Session() as session:
        id = uuid.UUID(id_)
        # query the db in order to find the NWDAF Datasource with the requested id
        nwdaf_datasource_db = session.query(NwdafDatasourceDB).filter(NwdafDatasourceDB.id == id).first()
        if nwdaf_datasource_db is not None:
                
            # if metrics is not present in the body, the field is left unchanged
            if "metrics" in nwdaf_datasource_dict.keys():
                
                # if the list of nwdaf Metrics is empty, monitor all the possible metrics
                if not nwdaf_datasource_dict['metrics']:
                    nwdaf_datasource_db.metrics = [NwdafMetrics.UL_VOLUME, NwdafMetrics.DL_VOLUME, NwdafMetrics.RTT, NwdafMetrics.PKT_LOSS, NwdafMetrics.PDU_NUMBER]
                else:
                 # remove duplicates from the metrics list
                    nwdaf_datasource_dict['metrics'] = lsmgmt.remove_duplicates(nwdaf_datasource_dict['metrics'])
                    nwdaf_datasource_db.metrics = nwdaf_datasource_dict['metrics']
            
            # if api-root is not present in the body, the field is left unchanged
            if "api-root" in nwdaf_datasource_dict.keys():
                if nwdaf_datasource_dict['api-root']:
                    nwdaf_datasource_db.api_root = nwdaf_datasource_dict['api-root']
                else:
                    nwdaf_datasource_db.api_root = conf.configuration['APP']['nwdaf_default_api_root']
            
            # if interval is not present in the body, the field is left unchanged
            if "interval" in nwdaf_datasource_dict.keys():
                if not nwdaf_datasource_dict["interval"]:
                    nwdaf_datasource_db.interval = conf.configuration['APP']['nwdaf_default_interval']
                else:
                    nwdaf_datasource_db.interval = nwdaf_datasource_dict['interval']

            # if topic-name is not present in the body, the field is left unchanged
            if "topic-name" in nwdaf_datasource_dict.keys():
                # if topic-name is empty the topic name follows the predefined pattern
                if not nwdaf_datasource_dict['topic-name']:
                    nwdaf_datasource_db.topic_name = "nwdaf_{}".format(nwdaf_datasource_db.slice_id)
                else:
                    nwdaf_datasource_db.topic_name = nwdaf_datasource_dict['topic-name']
            else:
                nwdaf_datasource_dict["topic-name"]=""
                
            # check the configuration file existence to perform an eventual stop early
            if os.path.isfile(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']):                
                # critical section
                with filelock.FileLock("telegraf_nwdaf_conf.lock"):                
                    logging.info("Critical Section accessed")
                    new_config_dictionary = mgmt.update_nwdaf_datasource_from_config(nwdaf_datasource_db.end_to_end_network_slice_uid, nwdaf_datasource_db.network_slice_subnet_uid, nwdaf_datasource_db.flow_id,nwdaf_datasource_db.api_root,nwdaf_datasource_db.interval,nwdaf_datasource_db.metrics,nwdaf_datasource_dict["topic-name"])
                    logging.info("Produced new telegraf configuration after deletion, writing it on the file")
                    try:
                        with open(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'],'w') as file:
                            file.write(toml.dumps(new_config_dictionary))
                            logging.info("Configuration wrote")
                            # the commit must be done just in case the configuration file is updated successfully
                            session.commit()
                            
                            service_updated_flag = docker.update_telegraf_nwdaf_service()
                            if not service_updated_flag:
                                return Response(
                                    "Error while restarting the mp_telegraf_nwdaf service\n",
                                    status=500)
                            else:
                                common.fix_json_array(nwdaf_datasource_db, "flow_id")
                                return Response(
                                    "{}".format(nwdaf_datasource_db), status=200)
                            
                            return Response(
                                    "{}".format(new_nwdaf_datasource_db.id),
                                    status=200)
                    except Exception as e:
                        logging.error(e)
                        return Response(
                            "Error Writing the NWDAF telegraf.conf file in {}}\n".format(conf.configuration['PATHS']['telegraf_nwdaf_conf_path']),
                            status=500)
        # datasource not found
        return Response(
            "NWDAF datasource with id {} Not Found\n".format(id_),
            status=404)
