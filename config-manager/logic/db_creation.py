from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData
from sqlalchemy.sql.sqltypes import ARRAY
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.dialects.postgresql import UUID
import uuid

import utils.configuration_mgmt as conf


def create_db_schema():
    # Session registry (helper object to establish user defined Session scopes)
    global Session

    # global_engine = create_engine('postgresql://postgres:nextworks@localhost/monitoring_platform')
    global_engine = create_engine("postgresql://{}:{}@{}/monitoring_platform".format(
        conf.configuration['DATABASE']['postgres_username'], conf.configuration['DATABASE']['postgres_password'],
        conf.configuration['DATABASE']['postgres_hostname']
    ))
    Session = scoped_session(sessionmaker(bind=global_engine))

    # DB creation (made if it has not been done yet)
    if not database_exists(global_engine.url):
        create_database(global_engine.url)

    metadata_obj = MetaData()

    owm_datasources_table = Table('owm_datasources', metadata_obj,
                                  Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
                                  Column('city_uid', Integer, nullable=False),
                                  Column('metrics', ARRAY(String)),
                                  Column('topic_name', String(32))
                                  )

    nwdaf_datasources_table = Table('nwdaf_datasources', metadata_obj,
                                    Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
                                    Column('end_to_end_network_slice_uid', String(64)),
                                    Column('network_slice_subnet_uid', String(64), nullable=False),
                                    Column('flow_id', String(32)),
                                    Column('api_root', String(64)),
                                    Column('interval', String(16)),
                                    Column('metrics', ARRAY(String)),
                                    Column('topic_name', String(64))
                                    )

    ransim_datasources_table = Table('ransim_datasources', metadata_obj,
                                     Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
                                     Column('metrics', ARRAY(String)),
                                     Column('topic_name', String(64)),
                                     Column('remote_output_file_path', String),
                                     Column('remote_input_files_path', ARRAY(String))
                                     )

    starfish_datasources_table = Table('starfish_datasources', metadata_obj,
                                       Column('id', UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
                                       Column('end_to_end_network_slice_uid', String(64)),
                                       Column('network_slice_subnet_uid', String(64)),
                                       Column('api_root', String(64)),
                                       Column('interval', String(16)),
                                       Column('metrics', ARRAY(String)),
                                       Column('topic_name', String(64))
                                       )

    ## users_table
    ## organization_table
    ## alert_rules_table (?)

    # commit to create all the objects in metadata_obj --> DDL sento to the db
    metadata_obj.create_all(global_engine)
