from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base
from sqlalchemy.types import ARRAY
from swagger_server.models.nwdaf_metrics import NwdafMetrics
import utils.configuration_mgmt as conf
from sqlalchemy.dialects.postgresql import UUID
import uuid

Base = declarative_base()

class NwdafDatasourceDB(Base):

   __tablename__ = 'nwdaf_datasources'
   id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
   end_to_end_network_slice_uid = Column(String)
   network_slice_subnet_uid = Column(String)
   flow_id = Column(ARRAY(String))
   api_root = Column(String)
   interval = Column(String)
   metrics = Column(ARRAY(String))
   topic_name = Column(String)
   
   
   
   def __repr__(self):
      return "<NwdafDatasource(end_to_end_network_slice_uid='%s', network_slice_subnet_uid='%s', flow_id='%s', api_root='%s', interval='%s', metrics='%s', topic_name='%s')>" % (self.end_to_end_network_slice_uid, self.network_slice_subnet_uid, self.flow_id, self.api_root, self.interval, self.metrics, self.topic_name)
   


   def __init__(self, NwdafDatasource):
      
      if NwdafDatasource.network_slice_subnet_uid:
         self.network_slice_subnet_uid = NwdafDatasource.network_slice_subnet_uid
         self.end_to_end_network_slice_uid = NwdafDatasource.end_to_end_network_slice_uid
      else:
         self.network_slice_subnet_uid = "anyslice"
         self.end_to_end_network_slice_uid = "anyslice"

      if NwdafDatasource.flow_id:
         self.flow_id = NwdafDatasource.flow_id
      else:
         self.flow_id = ["anyflow"]

      if NwdafDatasource.api_root:
         self.api_root = NwdafDatasource.api_root
      else:
         self.api_root = conf.configuration['APP']['nwdaf_default_api_root']

      # if interval is not specified, takes the default one
      if not NwdafDatasource.interval:
         self.interval = conf.configuration['APP']['nwdaf_default_interval'] ## default for NWDAF datasources
      else:
         self.interval = NwdafDatasource.interval
      
      # if the metrics list is empty, the datasource must monitor all the possible metrics
      if not NwdafDatasource.metrics:
         self.metrics = [NwdafMetrics.UL_VOLUME, NwdafMetrics.DL_VOLUME, NwdafMetrics.RTT, NwdafMetrics.PKT_LOSS, NwdafMetrics.PDU_NUMBER]
      else:
         self.metrics = NwdafDatasource.metrics
      
      # if the topic name is empty, the metrics of this datasource will be published in a Kafka topic named according a given pattern
      if NwdafDatasource.topic_name:
         self.topic_name = NwdafDatasource.topic_name
      else:
         self.topic_name = "nwdaf_{}".format(NwdafDatasource.end_to_end_network_slice_uid)
