from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base
from sqlalchemy.types import ARRAY
from swagger_server.models.owm_metrics import OwmMetrics
from sqlalchemy.dialects.postgresql import UUID
import uuid

Base = declarative_base()

class OwmDatasourceDB(Base):

   __tablename__ = 'owm_datasources'
   id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
   city_uid = Column(Integer)
   metrics = Column(ARRAY(String))
   topic_name = Column(String)

   
   
   def __repr__(self):
      return "<OwmDatasource(city_uid='%i', metrics='%s', topic_name='%s')>" % (self.city_uid, self.metrics, self.topic_name)

   
   
   def __init__(self, OwmDatasource):
      self.city_uid = OwmDatasource.city_uid
      
      # if the metrics list is empty, the datasource must monitor all the possible metrics
      if not OwmDatasource.metrics:
         self.metrics = [OwmMetrics.CLOUDINESS, OwmMetrics.HUMIDITY, OwmMetrics.PRESSURE, OwmMetrics.RAIN, OwmMetrics.TEMPERATURE, OwmMetrics.WIND_DEGREES, OwmMetrics.WIND_SPEED]
      else:
         self.metrics = OwmDatasource.metrics
      
      # if the topic name is empty, the metrics of this datasource will be published in a Kafka topic named according a given pattern
      if OwmDatasource.topic_name:
         self.topic_name = OwmDatasource.topic_name
      else:
         self.topic_name = "weather_{}".format(OwmDatasource.city_uid)