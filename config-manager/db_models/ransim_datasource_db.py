from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base
from sqlalchemy.types import ARRAY
from swagger_server.models.ransim_metrics import RansimMetrics
from sqlalchemy.dialects.postgresql import UUID
import uuid

Base = declarative_base()

class RansimDatasourceDB(Base):

   __tablename__ = 'ransim_datasources'
   id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
   metrics = Column(ARRAY(String))
   topic_name = Column(String)
   remote_output_file_path = Column(String)
   remote_input_files_path = Column(ARRAY(String))
   
   
   
   def __repr__(self):
      input_files_string = ""
      for index, path in enumerate(self.remote_input_files_path):
         input_files_string += path
         if index != len(self.remote_input_files_path)-1:
            input_files_string += ", "
      return "<RansimDatasource(metrics='%s', topic_name='%s', remote_output_file_path='%s', remote_input_files_path=[%s])>" % (self.metrics, self.topic_name, self.remote_output_file_path, input_files_string)
   


   def __init__(self, RansimDatasource):
      
      self.remote_output_file_path = RansimDatasource.remote_output_file_path
      self.remote_input_files_path = RansimDatasource.remote_input_files_path
      
      # if the topic name is empty, the metrics of this datasource will be published in a Kafka topic named according a given pattern
      self.topic_name = RansimDatasource.topic_name
      print(RansimDatasource.topic_name, flush=True)

      # if the metrics list is empty, the datasource must monitor all the possible metrics
      if not RansimDatasource.metrics:
         self.metrics = [RansimMetrics.UE_POSITIONING, RansimMetrics.GNB_POSITIONING, RansimMetrics.NETWORK_CONFIGURATION, RansimMetrics.RESOURCE_CONFIGURATION, RansimMetrics.NETWORK_STATUS, RansimMetrics.PERFORMANCE_RESULTS]
      else:
         self.metrics = RansimDatasource.metrics