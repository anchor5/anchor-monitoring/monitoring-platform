from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base
from sqlalchemy.types import ARRAY
from swagger_server.models.starfish_metrics import StarfishMetrics
import utils.configuration_mgmt as conf
from sqlalchemy.dialects.postgresql import UUID
import uuid


Base = declarative_base()

class StarfishDatasourceDB(Base):

   __tablename__ = 'starfish_datasources'
   id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
   end_to_end_network_slice_uid = Column(String)
   network_slice_subnet_uid = Column(String)
   api_root = Column(String)
   interval = Column(String)
   metrics = Column(ARRAY(String))
   topic_name = Column(String)
   
   
   
   def __repr__(self):
      return "<StarfishDatasource(end_to_end_network_slice_uid='%s', network_slice_subnet_uid='%s', api_root='%s', interval='%s', metrics='%s', topic_name='%s')>" % (self.end_to_end_network_slice_uid, self.network_slice_subnet_uid,self.api_root, self.interval, self.metrics, self.topic_name)
   

   def __init__(self, StarfishDatasource):
      
      self.end_to_end_network_slice_uid = StarfishDatasource.end_to_end_network_slice_uid
      self.network_slice_subnet_uid = StarfishDatasource.network_slice_subnet_uid

      if StarfishDatasource.api_root:
         self.api_root = StarfishDatasource.api_root
      else:
         self.api_root = conf.configuration['APP']['starfish_default_api_root']

      # if interval is not specified, takes the default one
      if not StarfishDatasource.interval:
         self.interval = conf.configuration['APP']['starfish_default_interval'] ## default for NWDAF datasources
      else:
         self.interval = StarfishDatasource.interval
      
      # if the metrics list is empty, the datasource must monitor all the possible metrics
      if not StarfishDatasource.metrics:
         self.metrics = [StarfishMetrics.TRAFFIC_COUNTERS, StarfishMetrics.TRAFFIC_FORMATS, StarfishMetrics.KPIS]
      else:
         self.metrics = StarfishDatasource.metrics
      
      # if the topic name is empty, the metrics of this datasource will be published in a Kafka topic named according a given pattern
      if StarfishDatasource.topic_name:
         self.topic_name = StarfishDatasource.topic_name
      else:
         self.topic_name = "starfish_{}".format(StarfishDatasource.end_to_end_network_slice_uid)
