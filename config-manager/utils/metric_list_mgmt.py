'''
Collection of functions to be used in order to deal with metrics lists
'''

from sqlalchemy import true


def remove_duplicates(input_list):
    return list(dict.fromkeys(input_list))

def check_list_equality_regardless_ordering(input_list_1, input_list_2):
    if set(input_list_1) == set(input_list_2):
        return True
    else:
        return False

def check_list_subset(input_list_1, input_list_2):
    if (set(input_list_1).issubset(set(input_list_2))):
        return True
    else:
        return False

def check_api_root_in_urls(input_url_list, api_root):
    for element in input_url_list:
        if element.startswith(api_root):
            return True
    return False

    
