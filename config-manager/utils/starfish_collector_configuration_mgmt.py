import toml
import logging
import uuid
from collections import OrderedDict
import utils.configuration_mgmt as conf

## since a new token has been requested from the orchestrator, need to change the token to all the input plugins
## NOTE: this way we can have a little downtime in the docker service update, the time between the token request and 
## the time in which the telegraf starfish with the new configuration starts effctively collecting data,
## POSSIBLE SOLUTION: the config manager requests a new token just for the first datasource --> all the subsequent
## datasources will take the same token --> [need to check if the interval is greater than the interval after which
## the starfish token expires] --> in this case need to update the logic

def create_json_http_details_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value):
    logging.debug('Starfish json HTTP input creation')
    urls = []
    urls.append('{}/networkslice/details?slice_uid={}'.format(api_root,network_slice_subnet_uid))
    headers_dict = {
        'Authorization' : 'Bearer {}'.format(api_token)
    }
    return OrderedDict(
        {
            'tagexclude': ['host'],
            'urls': urls,
            'method': 'GET',
            "headers": headers_dict,
            'data_format': 'json',
            'interval': interval,
            'name_override': 'starfish',
            'json_string_fields': ['gateway','terminal_id'],
            'tags': {
                'map': map_tag_value
            }
        }
    )

def create_json_http_modcod_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value):
    logging.debug('Starfish json HTTP input creation')
    urls = []
    urls.append('{}/networkslice/modcod?slice_uid={}'.format(api_root,network_slice_subnet_uid))
    headers_dict = {
        'Authorization' : 'Bearer {}'.format(api_token)
    }
    return OrderedDict(
        {
            'tagexclude': ['host'],
            'urls': urls,
            'method': 'GET',
            "headers": headers_dict,
            'data_format': 'value',
            'data_type': 'string',
            'interval': interval,
            'name_override': 'starfish',
            'tags': {
                'map': map_tag_value
            }
        }
    )

def create_json_v2_http_conf(api_token,network_slice_subnet_uid,api_root,interval,map_tag_value):
    logging.debug('Starfish json_v2 HTTP input creation')
    headers_dict = {
        'Authorization' : 'Bearer {}'.format(api_token)
    }
    return OrderedDict(
        {
            'tagexclude': ['host'],
            'urls': ['{}/networkslice/traffic_formats?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid)],
            'method': 'GET',
            "headers": headers_dict,
            'data_format': 'json_v2',
            'interval': interval,
            'tags': {
                'map': map_tag_value
            },
            'json_v2': [
                {
                    'measurement_name': 'starfish', 
                    'object': [
                        {
                            'path': '@this', 
                            'disable_prepend_keys': True, 
                            'tags': ['direction','format']
                        }
                    ]
                }
            ]
        }
    )

def create_json_http_conf(api_token, network_slice_subnet_uid, api_root, interval, metrics, map_tag_value):
    logging.debug('Starfish json HTTP input creation')
    urls = []
    for metric in metrics:
        if metric != "traffic_formats":
            urls.append('{}/networkslice/{}?slice_uid={}&minutes=1'.format(api_root,metric,network_slice_subnet_uid))
    headers_dict = {
        'Authorization' : 'Bearer {}'.format(api_token)
    }
    return OrderedDict(
        {
            'tagexclude': ['host'],
            'urls': urls,
            'method': 'GET',
            "headers": headers_dict,
            'data_format': 'json',
            'interval': interval,
            'name_override': 'starfish',
            'tags': {
                'map': map_tag_value
            }
        }
    )

def create_http_conf(api_token, network_slice_subnet_uid, api_root, interval, metrics, map_tag_value):
    http_plugin_to_add_list = []
    logging.debug('Starfish HTTP input creation')
    if "traffic_formats" in metrics:
        http_plugin_to_add_list.append(create_json_v2_http_conf(api_token,network_slice_subnet_uid,api_root,interval,map_tag_value))
    if "traffic_counters" in metrics or "kpis" in metrics:
        http_plugin_to_add_list.append(create_json_http_conf(api_token,network_slice_subnet_uid,api_root,interval,metrics,map_tag_value))
    return http_plugin_to_add_list

def create_kafka_conf(topic_name,map_tag_value):
    logging.debug('Kafka output creation')
    if map_tag_value != 'default':
        return dict({
            'brokers': ['kafka:9092'],  
            'client_id': 'telegraf_starfish',
            'topic': topic_name,
            'tagpass': {'map': [map_tag_value]},
            'tagexclude': ['map','url']
            })
    else:
        return dict({
            'brokers': ['kafka:9092'],
            'client_id': 'telegraf_starfish',   
            'tagpass': {'map': ['default']},
            'topic': 'starfish',
            'tagexclude': ['map','url'],
            'topic_suffix': {
                'keys': ['end_to_end_network_slice_uid'],
                'method': 'tags',
                'separator': '_' }
            })

def check_network_slice_uid_in_urls(urls,network_slice_subnet_uid):
    for element in urls:
        if(element.endswith(network_slice_subnet_uid + "&minutes=1") or element.endswith(network_slice_subnet_uid)):
            return True
    return False

def get_indices_network_slice_uid_in_urls(urls,network_slice_subnet_uid):
    logging.info("invoked\nurls={},\nnssuid={}".format(urls,network_slice_subnet_uid))
    url_index_list = [idx for idx, element in enumerate(urls) if (element.endswith(network_slice_subnet_uid + "&minutes=1") or element.endswith(network_slice_subnet_uid))]
    logging.info(url_index_list)
    assert(len(url_index_list)<=2)
    return url_index_list

def check_topic_name_change(map_tag, topic_name, network_slice_subnet_uid, kafka_plugin_list):
    if map_tag == "default" and topic_name != "starfish_{}".format(network_slice_subnet_uid):
        return True
    elif map_tag == "default" and topic_name == "starfish_{}".format(network_slice_subnet_uid):
        return False
    else:
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]==map_tag)]
        assert len(kafka_index_list) == 1
        if kafka_plugin_list[kafka_index_list[0]]['topic']==topic_name:
            return False
        else:
            return True

def check_interval_changed(new_interval, network_slice_subnet_uid, http_plugin_list):
    index_list = [idx for idx, element in enumerate(http_plugin_list) if (check_network_slice_uid_in_urls(element['urls'],network_slice_subnet_uid))]
    assert(len(index_list) >= 1)
    if http_plugin_list[index_list[0]]['interval'] == new_interval:
        return False
    return True

def check_api_root_changed(api_root, network_slice_subnet_uid, urls):
    for url in urls:
        if url.endswith(network_slice_subnet_uid + "&minutes=1"):
            if url.startswith(api_root):
                return False
            else:
                return True

def replace_api_root(api_root, network_slice_subnet_uid, http_plugin_list):
    for http_plugin in http_plugin_list:
        if check_network_slice_uid_in_urls(http_plugin['urls'],network_slice_subnet_uid):
            for index_url,url in enumerate(http_plugin['urls']):
                if url.endswith(network_slice_subnet_uid + "&minutes=1"):
                    metric=url.split("?")[0].rsplit('/',1)[1]
                    http_plugin['urls'][index_url] = api_root+"/"+metric+"?slice_uid={}&minutes=1".format(network_slice_subnet_uid)
    
def get_monitored_metrics_per_slice(network_slice_subnet_uid,http_plugin_list, indices_list):
    monitored_json_metrics = []
    for index in indices_list:
        urls = http_plugin_list[index]['urls']
        for url in urls:
            if url.endswith(network_slice_subnet_uid + "&minutes=1"):
                monitored_json_metrics.append(url.split("?")[0].rsplit('/',1)[1])
    return monitored_json_metrics

def update_http_plugins_by_metrics(network_slice_subnet_uid,api_root,http_plugin_list,indices_list,metrics):

    if len(indices_list) == 1:
        logging.debug("Just one existent HTTP input with same map tag value and interval")
        map_tag=http_plugin_list[indices_list[0]]['tags']['map']
        interval=http_plugin_list[indices_list[0]]['interval']
        api_token=http_plugin_list[indices_list[0]]['headers']['Authorization'].split()[1]
        monitored_metrics_set = set(get_monitored_metrics_per_slice(network_slice_subnet_uid,http_plugin_list, indices_list))

        if http_plugin_list[indices_list[0]]['data_format'] == 'json':
            requested_json_metrics_set = set(metrics) - {"traffic_formats"}
            json_metrics_to_add=list(requested_json_metrics_set-monitored_metrics_set-{"traffic_formats"})
            assert(len(json_metrics_to_add)<=1)
            json_metrics_to_delete=list(monitored_metrics_set-requested_json_metrics_set-{"traffic_formats"})
            assert(len(json_metrics_to_delete)<=2)
            
            if len(json_metrics_to_add)!=0:
                http_plugin_list[indices_list[0]]['urls'].append('{}/networkslice/{}?slice_uid={}&minutes=1'.format(api_root,json_metrics_to_add[0],network_slice_subnet_uid))
            
            if len(json_metrics_to_delete)!=0:
                if len(json_metrics_to_delete) == len(http_plugin_list[indices_list[0]]['urls']):
                    http_plugin_list.pop(indices_list[0])
                else:
                    for metric in json_metrics_to_delete:
                        http_plugin_list[indices_list[0]]['urls'].remove("{}/networkslice/{}?slice_uid={}&minutes=1".format(api_root,metric,network_slice_subnet_uid))

            ## since just the json_http plugin is present,if traffic_formats has been requested we need to add it
            if "traffic_formats" in metrics:
                json_v2_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (element['interval']==interval and element['tags']['map']==map_tag and element['data_format']=='json_v2')]
                assert(len(json_v2_indices_list)<=1)

                if not json_v2_indices_list:               
                    http_plugin_list.append(create_json_v2_http_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag))
                else:
                    http_plugin_list[json_v2_indices_list[0]]['urls'].append("{}/networkslice/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid))
        
        else:
            ## there is just json_v2 plugin, if traffic formats has been not requested need to check if the plugin must be deleted or we need just to update the datasource
            if "traffic_formats" not in metrics:
                if len(http_plugin_list[indices_list[0]]['urls'])==1:
                    assert(http_plugin_list[indices_list[0]]['urls'][0].endswith(network_slice_subnet_uid + "&minutes=1"))
                    http_plugin_list.pop(indices_list[0])
                else:
                    http_plugin_list[indices_list[0]]['urls'].remove("{}/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid))
            
            ## if at least one json metric has been requested, need to add also the json plugin
            if "kpis" in metrics or "traffic_counters" in metrics:
                
                json_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (element['interval']==interval and element['tags']['map']==map_tag and element['data_format']=='json')]
                assert(len(json_indices_list)<=1)

                if not json_indices_list:
                    http_plugin_list.append(create_json_http_conf(api_token, network_slice_subnet_uid, api_root, interval, metrics, map_tag))
                else:
                    for metric in metrics:
                        if metric != "traffic_formats":
                            http_plugin_list[json_indices_list[0]]['urls'].append("{}/networkslice/{}?slice_uid={}&minutes=1".format(api_root,metric,network_slice_subnet_uid))
    else:
        logging.debug("Two HTTP input (json and json_v2 data_format) with same map tag value and interval")
        monitored_metrics_set = set(get_monitored_metrics_per_slice(network_slice_subnet_uid,http_plugin_list, indices_list))
        for http_index in indices_list:
            if http_plugin_list[http_index]['data_format'] == 'json':
                requested_json_metrics_set = set(metrics) - {"traffic_formats"}
                json_metrics_to_add=list(requested_json_metrics_set-monitored_metrics_set-{"traffic_formats"})
                assert(len(json_metrics_to_add)<=1)
                json_metrics_to_delete=list(monitored_metrics_set-requested_json_metrics_set-{"traffic_formats"})
                assert(len(json_metrics_to_delete)<=2)

                if len(json_metrics_to_add)!=0:
                    http_plugin_list[http_index]['urls'].append('{}/networkslice/{}?slice_uid={}&minutes=1'.format(api_root,json_metrics_to_add[0],network_slice_subnet_uid))

                if len(json_metrics_to_delete)!=0:
                    if len(json_metrics_to_delete) == len(http_plugin_list[http_index]['urls']):
                        http_plugin_list.pop(http_index)
                        if len(indices_list)==2:
                            indices_list[1]-=1
                    else:
                        for metric in json_metrics_to_delete:
                            http_plugin_list[http_index]['urls'].remove("{}/networkslice/{}?slice_uid={}&minutes=1".format(api_root,metric,network_slice_subnet_uid))
            
            else: ## json_v2
                if "traffic_formats" not in metrics:
                    if len(http_plugin_list[http_index]['urls'])==1:
                        assert(http_plugin_list[http_index]['urls'][0].endswith(network_slice_subnet_uid + "&minutes=1"))
                        http_plugin_list.pop(http_index)
                        if len(indices_list)==2:
                            indices_list[1]-=1
                    else:
                        http_plugin_list[http_index]['urls'].remove("{}/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid))
        
def configure_http_based_on_metrics(api_token, network_slice_subnet_uid, api_root, interval, metrics, map_tag_value, http_index_list, http_plugin_list):
    ## if both the json and the json_v2 plugins are present
    if len(http_index_list) == 2:
        logging.debug('HTTP indices for insertion by custom map tag and interval found') 
        for index in http_index_list:
            http_plugin = http_plugin_list[index]
            if http_plugin['data_format'] == 'json_v2' and "traffic_formats" in metrics:
                http_plugin['urls'].append('{}/networkslice/traffic_formats?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid))
            else:
                if http_plugin['data_format'] == 'json':
                    if "traffic_counters" in metrics:
                        http_plugin['urls'].append('{}/networkslice/traffic_counters?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid))
                    if "kpis" in metrics:
                        http_plugin['urls'].append('{}/networkslice/kpis?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid))
    
    ## if just one of the two input plugins is present for the topic name requested with the interval requested -> need to understand which one (json or json_v2) and eventually add a new one (depending on requested metrics)
    elif len(http_index_list) == 1:
        ## case 1: json data format input plugin
        if http_plugin_list[http_index_list[0]]['data_format'] == 'json':
            ## need to check metrics that have been requested
            if "traffic_formats" in metrics:
                ## need to add a new http json_v2 plugin
                http_plugin_list.append(create_json_v2_http_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value))
            
            ## need to update the urls of the existent json plugin
            if "traffic_counters" in metrics:
                http_plugin_list[http_index_list[0]]['urls'].append('{}/networkslice/traffic_counters?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid))
            if "kpis" in metrics:
                http_plugin_list[http_index_list[0]]['urls'].append('{}/networkslice/kpis?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid))
            
        ## case 2: json_v2 data format input plugin
        else:
            if "traffic_formats" in metrics:
                ## need to add a new url to the urls list in the json_v2 plugin
                http_plugin_list[http_index_list[0]]['urls'].append('{}/networkslice/traffic_formats?slice_uid={}&minutes=1'.format(api_root,network_slice_subnet_uid))
            if "traffic_counters" in metrics or "kpis" in metrics:
                ## need to add a new http json plugin
                http_plugin_list.append(create_json_http_conf(api_token,network_slice_subnet_uid,api_root,interval,metrics,map_tag_value))
        
    ## there is not an http input plugin publishing in the same topic using the same interval
    else:
        logging.debug('HTTP input not found, adding one with custom map tag and the requested characteristics')
        http_plugin_list_to_add=create_http_conf(api_token,network_slice_subnet_uid,api_root,interval,metrics,map_tag_value)
        for plugin in http_plugin_list_to_add:
            http_plugin_list.append(plugin)
    
    return http_plugin_list

def delete_network_slice_uid_urls(network_slice_subnet_uid, urls):
    list_indices_to_delete = get_indices_network_slice_uid_in_urls(urls, network_slice_subnet_uid)
    for index in list_indices_to_delete:
        urls.pop(index)

def delete_network_slice_urls_and_empty_http(network_slice_subnet_uid,http_plugin_list):
    ## looking for eventual http input plugins in which the urls list contains the network_slice_subnet_uid (there can be one or two)
    slice_uid_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (check_network_slice_uid_in_urls(element['urls'],network_slice_subnet_uid))]
    assert (len(slice_uid_indices_list)>=1 and len(slice_uid_indices_list)<=2)
    ## need to check if one or both the plugins must be deleted, in this case we need also to check if the kafka output must be deleted
    for http_index in slice_uid_indices_list:
        ## getting indices of the network_slice_subnet_uid urls in the urls list, there can be one or two indices (in the json http input plugin, there can be traffic_counters and kpis)
        network_slice_subnet_uid_indices_in_urls = get_indices_network_slice_uid_in_urls(http_plugin_list[http_index]['urls'],network_slice_subnet_uid)
        assert(len(network_slice_subnet_uid_indices_in_urls)>=1 and len(network_slice_subnet_uid_indices_in_urls)<=2)
        ## check if the only endpoints in the urls list are the ones related to this network_slice_subnet_uid, if yes, need to delete the http input plugin
        if len(http_plugin_list[http_index]['urls']) == len(network_slice_subnet_uid_indices_in_urls):
            ## in this case we need to remove one or both the HTTP input plugin related to this network_slice_subnet_uid
            logging.debug("There are no other slice_uid in the urls list of the http input plugin\nThe HTTP plugin can be removed.")
            http_plugin_list.pop(http_index)
            ## in case of deletion the index of the other is decremented
            if len(slice_uid_indices_list)==2:
                slice_uid_indices_list[1]-=1
        else:
            logging.debug("There are urls related to other slice_uid in the urls list of the http input plugin\nThe HTTP plugin will not be removed.")
            slice_urls_indices = get_indices_network_slice_uid_in_urls(http_plugin_list[http_index]['urls'],network_slice_subnet_uid)
            for index in slice_urls_indices:
                http_plugin_list[http_index]['urls'].pop(index)

## function invoked just in case the one that is added is the first datasource -> no http or kafka plugin are present
def add_first_starfish_datasource_to_config(api_token, end_to_end_network_slice_uid, network_slice_subnet_uid, api_root, interval, metrics, topic_name): 
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_starfish_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    assert(len(http_plugin_list)==0)
    assert(len(kafka_plugin_list)==0)

    ## default pattern for topic name --> map = 'default'
    if not topic_name or topic_name=="starfish_{}".format(network_slice_subnet_uid):
        logging.debug('Default pattern for topic_name')
        map_tag_value = 'default'
        
    # if the topic name is a customized one
    else:
        logging.debug('Custom pattern for topic_name, creating uuid')  
        map_tag_value = str(uuid.uuid4())
            
    logging.debug('Adding Kafka output plugin')
    kafka_plugin_list.append(create_kafka_conf(topic_name,map_tag_value))
    logging.debug('Adding HTTP input plugins')
    http_plugin_list.append(create_json_http_details_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value))
    http_plugin_list.append(create_json_http_modcod_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value))
    http_plugin_list_to_add=create_http_conf(api_token,network_slice_subnet_uid,api_root,interval,metrics,map_tag_value)
    for http_plugin in http_plugin_list_to_add:
        http_plugin_list.append(http_plugin)

    logging.debug('Adding the value mapping in processor plugin')
    configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings'] = {} ## dict creation
    processor_enum_mappings = configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']
    processor_enum_mappings["{}/networkslice/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/traffic_counters?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/kpis?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/details?slice_uid={}".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/modcod?slice_uid={}".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid

    configuration_dict['processors']['enum'][0]['mapping'][1]['value_mappings'] = {} ## dict creation
    processor_enum_mappings = configuration_dict['processors']['enum'][0]['mapping'][1]['value_mappings']
    processor_enum_mappings["{}/networkslice/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/traffic_counters?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/kpis?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/details?slice_uid={}".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/modcod?slice_uid={}".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    
    ## the processor enum plugin configuration is not useful anymore since we are requesting data directly using the network_slice_subnet_uid
    return configuration_dict

## function to add a starfish datasource in the case there is at least another one (need to check kafka output, http input plugin interval and topic and which plugins are needed [json or json_v2 data format])
def add_starfish_datasource_to_config(end_to_end_network_slice_uid, network_slice_subnet_uid, api_root, interval, metrics, topic_name): 
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_starfish_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']
    assert(len(http_plugin_list)>=1)
    api_token = http_plugin_list[0]['headers']['Authorization'].split()[1]
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    assert(len(kafka_plugin_list)>=1)

    ## default pattern for topic name --> map = 'default'
    if not topic_name or topic_name=="starfish_{}".format(network_slice_subnet_uid):
        logging.debug('Default pattern for topic_name')
        # look for the eventual kafka output with map tag value default
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]=='default')]
        assert len(kafka_index_list) <= 1
        map_tag_value = 'default'
        
    # if the topic name is a customized one
    else:
        logging.debug('Custom pattern for topic_name')       
        # look for the eventual kafka output publishing in the same topic in order to retrieve the map tag value for the new datasource
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        assert len(kafka_index_list) <= 1
    
        # if there is not -> new map tag creation
        if not kafka_index_list:
            logging.debug('Kafka output for the new topic not found, creating new map tag value')
            map_tag_value=str(uuid.uuid4())
        # if there is another kafka output publishing in this topic, take its map tag value to be used for the new datasource
        else:
            logging.debug('Found kafka output with same topic_name={} at index {}, using the same map tag value'.format(topic_name, kafka_index_list[0]))
            map_tag_value = kafka_plugin_list[kafka_index_list[0]]['tagpass']['map'][0] # tagpass['map'] is an array

    ## TODO: need to add to the condition check also the interval!!
    http_details_index_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map']==map_tag_value and element['urls'][0].startswith("{}/networkslice/details?".format(api_root)))]
    assert len(http_details_index_list) <= 1
    if http_details_index_list:
        http_details_index= http_details_index_list[0]
        http_plugin_list[http_details_index]['urls'].append("{}/networkslice/details?slice_uid={}".format(api_root,network_slice_subnet_uid))
    else:
        http_plugin_list.append(create_json_http_details_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value))
    

    http_modcod_index_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map']==map_tag_value and element['urls'][0].startswith("{}/networkslice/modcod?".format(api_root)))]
    assert len(http_modcod_index_list) <= 1
    if http_modcod_index_list:
        http_modcod_index= http_modcod_index_list[0]
        http_plugin_list[http_modcod_index]['urls'].append("{}/networkslice/modcod?slice_uid={}".format(api_root,network_slice_subnet_uid))
    else:
        http_plugin_list.append(create_json_http_modcod_conf(api_token, network_slice_subnet_uid, api_root, interval, map_tag_value))


    # if there is already an output publishing in the same topic, look for an eventual http input plugin with same interval in order to add the URLs
    if kafka_index_list:
        
        # search http index [double, nullable] with this map tag value and same interval
        http_index_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map']==map_tag_value and element['interval']==interval)]
        assert len(http_index_list) <= 4

        http_plugin_list = configure_http_based_on_metrics(api_token, network_slice_subnet_uid, api_root, interval, metrics, map_tag_value, http_index_list, http_plugin_list)

    # if there is not a kafka output publishing in the same topic there is not also the related http input plugin --> need create both
    else:
        logging.debug('Adding Kafka output')
        kafka_plugin_list.append(create_kafka_conf(topic_name,map_tag_value))
        logging.debug('Adding HTTP input')

        http_plugin_list_to_add=create_http_conf(api_token,network_slice_subnet_uid,api_root,interval,metrics,map_tag_value)
        for plugin in http_plugin_list_to_add:
            http_plugin_list.append(plugin)
    
    logging.debug('Adding the value mapping in processor plugin')
    processor_enum_mappings = configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']
    processor_enum_mappings["{}/networkslice/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/traffic_counters?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/kpis?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/details?slice_uid={}".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
    processor_enum_mappings["{}/networkslice/modcod?slice_uid={}".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid

    processor_enum_mappings = configuration_dict['processors']['enum'][0]['mapping'][1]['value_mappings']
    processor_enum_mappings["{}/networkslice/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/traffic_counters?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/kpis?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/details?slice_uid={}".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid
    processor_enum_mappings["{}/networkslice/modcod?slice_uid={}".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid

    return configuration_dict

def delete_starfish_datasource_from_config(network_slice_subnet_uid):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_starfish_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']

    ## looking for eventual http input plugins in which the urls list contains the network_slice_subnet_uid (there can be one or two)
    slice_uid_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (check_network_slice_uid_in_urls(element['urls'],network_slice_subnet_uid))]
    print("slice uid indices list = {}".format(slice_uid_indices_list))
    assert (len(slice_uid_indices_list)>=3 and len(slice_uid_indices_list)<=4)

    http_indices_to_be_deleted = []
    ## need to check if one or both the plugins must be deleted, in this case we need also to check if the kafka output must be deleted
    for http_index in slice_uid_indices_list:
        ## getting indices of the network_slice_subnet_uid urls in the urls list, there can be one or two indices (in the json http input plugin, there can be traffic_counters and kpis)
        network_slice_subnet_uid_indices_in_urls = get_indices_network_slice_uid_in_urls(http_plugin_list[http_index]['urls'],network_slice_subnet_uid)
        assert(len(network_slice_subnet_uid_indices_in_urls)>=1 and len(network_slice_subnet_uid_indices_in_urls)<=2)

        ## check if the only endpoints in the urls list are the ones related to this network_slice_subnet_uid, if yes, need to delete the http input plugin
        if len(http_plugin_list[http_index]['urls']) == len(network_slice_subnet_uid_indices_in_urls):
            ## in this case we need to remove one or both the HTTP input plugin related to this network_slice_subnet_uid
            print("There are no other slice_uid in the urls list of the http input plugin\nThe HTTP plugin can be removed.")
            map_tag_value_deleted_http = http_plugin_list[http_index]['tags']['map']


            http_indices_to_be_deleted.append(http_index)
        else:
            ## in this case we need to remove one/two URLs from one/two plugins 
            print("There are urls related to other slice_uid in the urls list of the http input plugin\nThe HTTP plugin will not be removed.")
            #http_plugin_list = delete_network_slice_subnet_uid_urls(network_slice_subnet_uid, http_plugin_list[http_index]['urls'])
            delete_network_slice_uid_urls(network_slice_subnet_uid, http_plugin_list[http_index]['urls'])
    
    # eventually deletes http plugins with empty urls
    if http_indices_to_be_deleted:
        http_indices_to_be_deleted.sort(reverse=True)
        for index in http_indices_to_be_deleted:
            http_plugin_list.pop(index)
    
        # search the indices of the http inputs with same map_tag value (publishing in the same topic)
        http_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map']==map_tag_value_deleted_http)]

        if not http_indices_list:
            print("No other http input plugin is using the same map tag value, going to delete the related kafka output")
            # if no http input is using the same tag value, deletion of the kafka output related to it
            kafka_plugin_list = configuration_dict['outputs']['kafka']
            # search if there is already a kafka output for this topic name
            kafka_indices_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]==map_tag_value_deleted_http)]
            assert len(kafka_indices_list) == 1
            kafka_plugin_list.pop(kafka_indices_list[0])
            print("kafka output with index {} deleted".format(kafka_indices_list[0]))
    ###

    ## remove from the enum processor mapping the entries related to this slice
    print('Removing the value mapping in processor plugin')
    for enum_processor_mapping in configuration_dict['processors']['enum'][0]['mapping']: 
        for key in list(enum_processor_mapping['value_mappings'].keys()):
            if key.endswith(network_slice_subnet_uid + "&minutes=1") or key.endswith(network_slice_subnet_uid):
                del enum_processor_mapping['value_mappings'][key]

    return configuration_dict

def delete_all_starfish_datasources_from_config():
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_starfish_conf_path'])
    configuration_dict['inputs']['http']=[]
    configuration_dict['outputs']['kafka']=[]
    configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']={}
    configuration_dict['processors']['enum'][0]['mapping'][1]['value_mappings']={}
    return configuration_dict


def update_starfish_datasource_from_config(end_to_end_network_slice_uid, network_slice_subnet_uid, api_root, interval, metrics, topic_name): 
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_starfish_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']
    kafka_plugin_list = configuration_dict['outputs']['kafka']

    ## looking for http plugins having monitoring the network_slice_subnet_uid, there can be one or two
    index_list = [idx for idx, element in enumerate(http_plugin_list) if (check_network_slice_uid_in_urls(element['urls'],network_slice_subnet_uid))]
    assert (len(index_list)>=1 and len(index_list)<=2)

    ## now we need to check if the topic name is chaged in the PUT request
    old_map_tag_value = http_plugin_list[index_list[0]]['tags']['map']
    api_token = http_plugin_list[0]['headers']['Authorization'].split()[1]

    updated_topic_flag=check_topic_name_change(old_map_tag_value, topic_name, network_slice_subnet_uid, kafka_plugin_list)
    updated_interval_flag=check_interval_changed(interval, network_slice_subnet_uid, http_plugin_list)

    ## check if topic name or interval has been changed: --> in this case the metrics are moved away from the actual plugin
    if updated_topic_flag or updated_interval_flag:
        if updated_topic_flag:
            ## deleting all the urls related to this slice, checking the related http must be deleted and check in positive case if also the related kafka output must be deleted
            configuration_dict = delete_starfish_datasource_from_config(network_slice_subnet_uid)
            http_plugin_list = configuration_dict['inputs']['http']
            kafka_plugin_list = configuration_dict['outputs']['kafka']

            if not configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']:
                configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings'] = {} ## dict creation
            processor_enum_mappings = configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']
            processor_enum_mappings["{}/networkslice/traffic_formats?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
            processor_enum_mappings["{}/networkslice/traffic_counters?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
            processor_enum_mappings["{}/networkslice/kpis?slice_uid={}&minutes=1".format(api_root,network_slice_subnet_uid)] = network_slice_subnet_uid
        else:
            delete_network_slice_urls_and_empty_http(network_slice_subnet_uid,http_plugin_list)

    if updated_topic_flag:
        logging.debug("Requested topic-name change in update")
        ## getting the map tag of the new topic, NB in case topic-name="" --> starfish logic will pass to this function the real topic name starfosh_<slice-uid>
        if topic_name == "starfish_{}".format(network_slice_subnet_uid):
            kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]=='default')]
        else:
            kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        
        ## if no other output is publishing metrics in the new topic --> new map tag creation
        if not kafka_index_list:
            if topic_name == "starfish_{}".format(network_slice_subnet_uid):
                new_map_tag_value = "default"
            else:
                new_map_tag_value = str(uuid.uuid4())

            logging.debug("No kafka output existent for the new topic-name, adding both HTTP input and Kafka output")
            logging.debug('Adding a new Kafka output to publish in the requested new topic')
            kafka_plugin_list.append(create_kafka_conf(topic_name,new_map_tag_value))
            logging.debug('Adding the related HTTP input(s)')

            http_plugin_list_to_add=create_http_conf(api_token, network_slice_subnet_uid,api_root,interval,metrics,new_map_tag_value)
            for plugin in http_plugin_list_to_add:
                http_plugin_list.append(plugin)
                
        ## if there are kafka output publishing in the same new topic
        else:
            logging.debug("There are existent Kafka output for the new topic-name, getting the related map tag value")
            new_map_tag_value=kafka_plugin_list[kafka_index_list[0]]['tagpass']['map'][0]
        
            ## look for http plugins with same interval and same map tag value
            map_interval_http_plugin_index_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map'] == new_map_tag_value and element['interval']==interval)]
            assert(len(map_interval_http_plugin_index_list)>=0 and len(map_interval_http_plugin_index_list)<=2)

            http_plugin_list = configure_http_based_on_metrics(api_token, network_slice_subnet_uid, api_root, interval, metrics, new_map_tag_value, map_interval_http_plugin_index_list, http_plugin_list)
                   
    else:
        logging.debug("Topic name has not been changed in the update, need to check the eventual interval change")
        if updated_interval_flag:
            logging.debug("Requested an interval change")

            ## look for http plugins with same map tag as before (topic has not changed) and same new interval
            new_map_tag_http_plugin_index_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map'] == old_map_tag_value and element['interval']==interval)]
            assert(len(new_map_tag_http_plugin_index_list) >= 0 and len(new_map_tag_http_plugin_index_list) <= 2)

            #http_plugin_list = configure_http_based_on_metrics(api_token, network_slice_subnet_uid, api_root, interval, metrics, old_map_tag_value, new_map_tag_http_plugin_index_list, http_plugin_list)
            configuration_dict['inputs']['http'] = configure_http_based_on_metrics(api_token, network_slice_subnet_uid, api_root, interval, metrics, old_map_tag_value, new_map_tag_http_plugin_index_list, http_plugin_list)
        else:
            logging.debug("Also interval has not been updated, checking API root and metrics")
            ## in this case the slice must not change the http plugin in which it needs to stay (it depends from the metrics, there can be the need to delete the plugin data format)
            # we need to understand if the api-root has changed, if yes the urls must be changed (deleted and re added) according to the metrics --> pay attention to the deletion and leave empty urls

            ## index list contains the http plugins related to the network_slice_subnet_uid
            if check_api_root_changed(api_root, network_slice_subnet_uid, http_plugin_list[index_list[0]]['urls']):
                logging.debug("Updated API root, going to replace it in the existent url related to the network_slice_subnet_uid involved")
                ## function to update the api root in the URLs related to this slice_uid
                replace_api_root(api_root, network_slice_subnet_uid, http_plugin_list) ## check if the URLs are really updated

                logging.debug("Updating HTTP plugins according to the requested metrics")
                ## function to update the number of the http plugin (reduced or increased), according to the metrics
                update_http_plugins_by_metrics(network_slice_subnet_uid, api_root, http_plugin_list,index_list,metrics)
            
            else:
                logging.debug("Also API root has not been changed, Updating HTTP plugins according to the requested metrics")
                ## function to update the number of the http plugin (reduced or increased), according to the metrics
                update_http_plugins_by_metrics(network_slice_subnet_uid,api_root,http_plugin_list,index_list,metrics)
            
    return configuration_dict
