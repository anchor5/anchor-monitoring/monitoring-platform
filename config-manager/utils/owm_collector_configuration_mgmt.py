'''
File to define functions useful to update the telegraf.conf file for the telegraf_owm collector agent
The file is supposed to be placed at ./config/telegraf_owm/telegraf.conf

The fields that can be configured for the owm datasources are the city uid, the metrics and the topic name:
for each combination of requested metrics-topic_name there must be configured an owm input plugin

The mapping between the owm input plugin and the kafka output plugin is deduced from a tag 'map' added in the owm plugin,
set equal to the datasource id of the datasource to be created or, if the topic name is custom and the kafka output publishing to the topic
is already present, equal to the map field already present.

All the owm input plugin mapped to the topics with the predefined pattern for the topic name are mapped with 'map'='default',
all the custom named topics are mapped as stated above.

the kafka output with 'map'='default' MUST NEVER BE DELETED
'''
import logging
import toml
import uuid
import utils.metric_list_mgmt as lsmgmt

import utils.configuration_mgmt as conf

def create_owm_conf(city_uid,metrics,map_tag_value):
    logging.debug('OWM input creation')
    return dict({
        'app_id': conf.configuration['APP']['owm_app_id'],
        'base_url': 'http://api.openweathermap.org/',
        'city_id': [city_uid],
        'fetch': ['weather'],
        'fieldpass': metrics,
        'interval': '30s', # temp value, the correct one would be '1h'
        'lang': 'en',
        'response_timeout': '30s',
        'tagexclude': ['host', 'condition_icon'],
        'tags': {'map': map_tag_value},
        'units': 'metric'})


def create_kafka_conf(topic_name,map_tag_value):
    logging.debug('Kafka output creation')
    if map_tag_value != 'default':
        return dict({
            'brokers': ['kafka:9092'],  
            'client_id': 'telegraf',
            'topic': topic_name,
            'tagpass': {'map': [map_tag_value]},
            'tagexclude': ['map']
            })
    else:
        return dict({
            'brokers': ['kafka:9092'],
            'client_id': 'telegraf',   
            'tagpass': {'map': ['default']},
            'topic': 'weather',
            'topic_suffix': {
                'keys': ['city'],
                'method': 'tags',
                'separator': '_' },
            'tagexclude': ['map']
            })

def add_owm_datasource_to_config(city_uid,metrics,topic_name): 
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_owm_conf_path'])
    owm_plugin_list = configuration_dict['inputs']['openweathermap']
    kafka_plugin_list = configuration_dict['outputs']['kafka']

    # default pattern for topic name --> map = 'default'
    if not topic_name:
        logging.debug('Default pattern for topic_name')
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]=='default')]
        assert len(kafka_index_list) <= 1
        map_tag_value = 'default'
        
    # if the topic name is a customized one
    else:
        logging.debug('Custom pattern for topic_name')        
        # search if there is already a kafka output for the map tag value default
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        assert len(kafka_index_list) <= 1
    
        # if there is not the kafka output for rhis topic, new map tag creation, otherwise takes the one of the kafka output
        if not kafka_index_list:
            logging.debug('Kafka output not found, creating new map tag value')
            map_tag_value=str(uuid.uuid4())
            
        else:
            logging.debug('Found kafka output with same topic_name={} at index {}, using the same map tag value'.format(topic_name, kafka_index_list[0]))
            map_tag_value = kafka_plugin_list[kafka_index_list[0]]['tagpass']['map'][0] # tagpass['map'] is an array

    #if already output for topic name, look for an eventual input with same metric set in which insert the city_uid
    if kafka_index_list:
        # search owm index [unique, nullable] with this map value and same metrics
        owm_index_list = [idx for idx, element in enumerate(owm_plugin_list) if ((element['tags']['map']==map_tag_value) and lsmgmt.check_list_equality_regardless_ordering(element['fieldpass'],metrics))]
        assert len(owm_index_list) <= 1

        if owm_index_list:
            logging.debug('OWM index for insertion by custom map tag and metrics found: {}'.format(owm_index_list[0]))
            owm_plugin_list[owm_index_list[0]]['city_id'].append(city_uid)
        else:
            logging.debug('OWM input not found, adding one with custom map tag and the requested characteistcics')
            owm_plugin_list.append(create_owm_conf(city_uid,metrics,map_tag_value))

    # if there is not the kafka output there is not also the related owm input --> create both
    else:
        logging.debug('Adding Kafka output')
        kafka_plugin_list.append(create_kafka_conf(topic_name,map_tag_value))
        logging.debug('Adding OWM input')
        owm_plugin_list.append(create_owm_conf(city_uid,metrics,map_tag_value))

    return configuration_dict


# here we are assuming that the owm datasource monitoring the city id i exists (checked from the db)
# the check of the emptyness of the owm_list is made before invocking the method
def delete_owm_datasource_from_config(city_uid):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_owm_conf_path'])
    owm_plugin_list = configuration_dict['inputs']['openweathermap']

    index_list = [idx for idx, element in enumerate(owm_plugin_list) if (city_uid in element['city_id'])]
    assert len(index_list) == 1

    # no need to check the emptyness of the list (there MUST be at least one)
    assert len(owm_plugin_list[index_list[0]]['city_id']) >= 1
    if len(owm_plugin_list[index_list[0]]['city_id']) > 1:
        logging.debug('Other city uids present, no need to delete owm input')
        owm_plugin_list[index_list[0]]['city_id'].remove(city_uid)
    else: # just one city_uid monitored --> delete the owm input
        map_tag_value = owm_plugin_list[index_list[0]]['tags']['map']
        owm_plugin_list.pop(index_list[0]) 
        logging.debug("No other city_uid, owm input with map tag = {} deleted".format(map_tag_value))

        # search the indices of the owm inputs with same map_tag value
        owm_indices_list = [idx for idx, element in enumerate(owm_plugin_list) if (element['tags']['map']==map_tag_value)]

        if not owm_indices_list:
            logging.debug("No other owm input is using the sam map tag value, going to delete the related kafka output")
            # if no owm input is using the same tag value, deletion of the kafka output related to it
            kafka_plugin_list = configuration_dict['outputs']['kafka']
            # search if there is already a kafka output for this topic name
            kafka_indices_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]==map_tag_value)]
            assert len(kafka_indices_list) == 1

            kafka_plugin_list.pop(kafka_indices_list[0])
            logging.debug("kafka output with index {} deleted".format(kafka_indices_list[0]))
    
    return configuration_dict

def delete_all_owm_datasources_from_config():
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_owm_conf_path'])
    configuration_dict['inputs']['openweathermap']=[]
    configuration_dict['outputs']['kafka']=[]
    return configuration_dict


# the update of an owm datasource is done by deleting and re-adding it updated inside the configuration file (the functions above cannot be
# reused since they read the configuration file directly inside the body and since the eventually raised exception during the write of the
# file management is done in the owm datasource logic file)
def update_owm_datasource_from_config(city_uid,metrics,topic_name):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_owm_conf_path'])
    owm_plugin_list = configuration_dict['inputs']['openweathermap']
    kafka_plugin_list = configuration_dict['outputs']['kafka']

    index_list = [idx for idx, element in enumerate(owm_plugin_list) if (city_uid in element['city_id'])]
    assert len(index_list) == 1

    # no need to check the emptyness of the list (there MUST be at least one)
    assert len(owm_plugin_list[index_list[0]]['city_id']) >= 1
    
    if len(owm_plugin_list[index_list[0]]['city_id']) > 1:
        logging.debug('Other city uids present, no need to delete owm input')
        owm_plugin_list[index_list[0]]['city_id'].remove(city_uid)
    else: # just one city_uid monitored --> delete the owm input
        map_tag_value = owm_plugin_list[index_list[0]]['tags']['map']
        owm_plugin_list.pop(index_list[0]) 
        logging.debug("No other city_uid, owm input with map tag = {} deleted".format(map_tag_value))

        # search the indices of the owm inputs with same map_tag value
        owm_indices_list = [idx for idx, element in enumerate(owm_plugin_list) if (element['tags']['map']==map_tag_value)]

        if not owm_indices_list:
            logging.debug("No other owm input is using the sam map tag value, going to delete the related kafka output")
            # if no owm input is using the same tag value, deletion of the kafka output related to it
            # search if there is already a kafka output for this topic name
            kafka_indices_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]==map_tag_value)]
            assert len(kafka_indices_list) == 1

            kafka_plugin_list.pop(kafka_indices_list[0])
            logging.debug("kafka output with index {} deleted".format(kafka_indices_list[0]))
    
    #### Until here, datasource deletion, now the updated one must be added to the dict
    
    # default pattern for topic name --> map = 'default'
    if not topic_name:
        logging.debug('Default pattern for topic_name')
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]=='default')]
        assert len(kafka_index_list) <= 1
        map_tag_value = 'default'
        
    # if the topic name is a customized one
    else:
        logging.debug('Custom pattern for topic_name')
        logging.debug("topic name = {}".format(topic_name))
        # search if there is already a kafka output for the map tag value default
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        assert len(kafka_index_list) <= 1
    
        # if there is not the kafka output for rhis topic, new map tag creation, otherwise takes the one of the kafka output
        if not kafka_index_list:
            logging.debug('Kafka output not found, creating new map tag value')
            map_tag_value=str(uuid.uuid4())

        else:
            logging.debug('Found kafka output with same topic_name={} at index {}, using the same map tag value'.format(topic_name, kafka_index_list[0]))
            map_tag_value = kafka_plugin_list[kafka_index_list[0]]['tagpass']['map'][0] # tagpass['map'] is an array

    #if already output for topic name, look for an eventual input with same metric set in which insert the city_uid
    if kafka_index_list:
        # search owm index [unique, nullable] with this map value and same metrics
        owm_index_list = [idx for idx, element in enumerate(owm_plugin_list) if ((element['tags']['map']==map_tag_value) and lsmgmt.check_list_equality_regardless_ordering(element['fieldpass'],metrics))]
        assert len(owm_index_list) <= 1

        if owm_index_list:
            logging.debug('OWM index for insertion by custom map tag and metrics found: {}'.format(owm_index_list[0]))
            owm_plugin_list[owm_index_list[0]]['city_id'].append(city_uid)
        else:
            logging.debug('OWM input not found, adding one with custom map tag and the requested characteistcics')
            owm_plugin_list.append(create_owm_conf(city_uid,metrics,map_tag_value))

    # if there is not the kafka output there is not also the related owm input --> create both
    else:
        logging.debug('Adding Kafka output')
        kafka_plugin_list.append(create_kafka_conf(topic_name,map_tag_value))
        logging.debug('Adding OWM input')
        owm_plugin_list.append(create_owm_conf(city_uid,metrics,map_tag_value))
    
    return configuration_dict