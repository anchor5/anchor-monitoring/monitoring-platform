import toml
import uuid
from collections import OrderedDict

import utils.metric_list_mgmt as lsmgmt
import utils.configuration_mgmt as conf

def create_execd_command(remote_output_file_path, remote_input_files_path):
    command = ["/bin/sh","/home/telegraf/monitor_remote.sh"]
    command.append(remote_output_file_path)
    for filepath in remote_input_files_path:
        command.append(filepath)
    return command

def create_kafka_output(metrics,topic_name):
    return dict({
        'brokers': ['kafka:9092'],  
        'client_id': 'telegraf_nwdaf',
        'topic': topic_name,
        'namepass': metrics
    })

def create_prometheus_output(metrics):
    return dict({
        'listen': ':9276',  
        'metric_version': 2,
        'path': '/metrics',
        'collectors_exclude': ["gocollector","process"],
        'namepass': metrics
    })

def create_influxdb_output(metrics):
    return dict({
        'urls': ["http://influxdb:8086"],
        'token' : '$DOCKER_INFLUXDB_INIT_ADMIN_TOKEN',
        'organization': '$DOCKER_INFLUXDB_INIT_ORG',
        'bucket': '$DOCKER_INFLUXDB_INIT_BUCKET',
        'namepass': metrics
    })

def rename_metrics(metrics):
    namepass_list = []
    for metric in metrics:
        namepass_list.append("ransim_{}".format(metric))
    return namepass_list


def add_ransim_datasource_to_config(metrics, topic_name, remote_output_file_path, remote_input_files_path): 
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    metrics = rename_metrics(metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']

    ## in this case we do not use the map tag value since the default topic is named "ransim", without tag that dynamically create topics
    ## if there is already a kafka output plugin configured, add the difference between the requested metrics and the ones already monitored
    if kafka_plugin_list:
    
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        assert len(kafka_index_list) <= 1
        ## if there is already a kafka output with same topic_name, already checked that there is the need to add something to the metrics
        if(kafka_index_list):
            kafka_plugin_list[kafka_index_list[0]]['namepass'] = list(set(kafka_plugin_list[kafka_index_list[0]]['namepass']).union(set(metrics)))
        else:
            configuration_dict['outputs']['kafka'].append(create_kafka_output(metrics,topic_name))
        
        ## update also the other outputs namepass
        prometheus_output = configuration_dict['outputs']['prometheus_client'][0]
        influxdb_output = configuration_dict['outputs']['influxdb_v2'][0]
        prometheus_output['namepass'] = list(set(prometheus_output['namepass']).union(metrics))
        influxdb_output['namepass'] = list(set(influxdb_output['namepass']).union(set(metrics)))

    else:
    
        ## arriving here means this is the first round of the simulation --> need to pass all the files to be read
        configuration_dict['inputs']['execd'][0]['command'] = create_execd_command(remote_output_file_path, remote_input_files_path)
        
        configuration_dict['outputs']['kafka'].append(create_kafka_output(metrics,topic_name))
        configuration_dict['outputs']['prometheus_client'].append(create_prometheus_output(metrics))
        configuration_dict['outputs']['influxdb_v2'].append(create_influxdb_output(metrics))

    return configuration_dict

def delete_ransim_datasource_from_config(topic_name, kafka_metrics_to_delete, other_outputs_metrics_to_delete):

    kafka_metrics_to_delete = rename_metrics(kafka_metrics_to_delete)
    other_outputs_metrics_to_delete = rename_metrics(other_outputs_metrics_to_delete)
    
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    kafka_plugin_list = configuration_dict['outputs']['kafka']
    
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
    assert len(index_list) == 1

    ## if all the metrics inside a kafka output must be deleted --> delete the kafka output
    if set(kafka_plugin_list[index_list[0]]['namepass']) == set(kafka_metrics_to_delete):
        kafka_plugin_list.pop(index_list[0])
        if (len(kafka_plugin_list)==0):
            configuration_dict['outputs']['prometheus_client']= []
            configuration_dict['outputs']['influxdb_v2']= []
            return configuration_dict
    else:
        ## else pruning of the metrics to be deleted
        kafka_plugin_list[index_list[0]]['namepass'] = list(set(kafka_plugin_list[index_list[0]]['namepass'])-set(kafka_metrics_to_delete))        

    ## in each case need to prune from the other outputs the metrics not useful anymore
    prometheus_output = configuration_dict['outputs']['prometheus_client'][0]
    influxdb_output = configuration_dict['outputs']['influxdb_v2'][0]

    prometheus_output['namepass'] = list(set(prometheus_output['namepass'])-set(other_outputs_metrics_to_delete)) 
    influxdb_output['namepass'] = list(set(influxdb_output['namepass'])-set(other_outputs_metrics_to_delete))
        
    return configuration_dict

def delete_all_ransim_datasources_from_config():
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])
    configuration_dict['inputs']['execd'][0]['command'] = ""
    configuration_dict['outputs']['kafka'] = []
    configuration_dict['outputs']['prometheus_client'] = []
    configuration_dict['outputs']['influxdb_v2'] = []
    return configuration_dict

def delete_outputs_ransim_datasource_from_config():
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    configuration_dict['outputs']['kafka'] = []
    configuration_dict['outputs']['prometheus_client'] = []
    configuration_dict['outputs']['influxdb_v2'] = []

    return configuration_dict


def delete_old_and_create_new(old_topic_name, new_topic_name, new_kafka_output_metrics, new_other_outputs_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    new_other_outputs_metrics = rename_metrics(new_other_outputs_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1
    kafka_plugin_list.pop(index_list[0])

    kafka_plugin_list.append(create_kafka_output(new_kafka_output_metrics,new_topic_name))
    configuration_dict['outputs']['prometheus_client'][0]['namepass'] = new_other_outputs_metrics
    configuration_dict['outputs']['influxdb_v2'][0]['namepass'] = new_other_outputs_metrics

    return configuration_dict


def delete_old_and_update_new(old_topic_name, new_topic_name, new_kafka_output_metrics, new_other_outputs_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    new_other_outputs_metrics = rename_metrics(new_other_outputs_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1
    kafka_plugin_list.pop(index_list[0])

    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==new_topic_name)]
    assert len(index_list) == 1
    kafka_plugin_list[index_list[0]]['namepass'] = new_kafka_output_metrics

    configuration_dict['outputs']['prometheus_client'][0]['namepass'] = new_other_outputs_metrics
    configuration_dict['outputs']['influxdb_v2'][0]['namepass'] = new_other_outputs_metrics

    return configuration_dict


def update_old_and_create_new(old_topic_name, new_topic_name, old_kafka_output_metrics_to_delete, new_kafka_output_metrics, new_other_outputs_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    old_kafka_output_metrics_to_delete = rename_metrics(old_kafka_output_metrics_to_delete)
    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    new_other_outputs_metrics = rename_metrics(new_other_outputs_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1

    ## update the old kafka output metrics
    kafka_plugin_list[index_list[0]]['namepass'] = list(set(kafka_plugin_list[index_list[0]]['namepass']) - set(old_kafka_output_metrics_to_delete))
    
    kafka_plugin_list.append(create_kafka_output(new_kafka_output_metrics,new_topic_name))
    
    configuration_dict['outputs']['prometheus_client'][0]['namepass'] = new_other_outputs_metrics
    configuration_dict['outputs']['influxdb_v2'][0]['namepass'] = new_other_outputs_metrics

    return configuration_dict


def update_old_and_update_new(old_topic_name, new_topic_name, old_kafka_output_metrics_to_delete, new_kafka_output_metrics, new_other_outputs_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    old_kafka_output_metrics_to_delete = rename_metrics(old_kafka_output_metrics_to_delete)
    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    new_other_outputs_metrics = rename_metrics(new_other_outputs_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1

    ## update the old kafka output metrics
    kafka_plugin_list[index_list[0]]['namepass'] = list(set(kafka_plugin_list[index_list[0]]['namepass']) - set(old_kafka_output_metrics_to_delete))

    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==new_topic_name)]
    assert len(index_list) == 1
    
    ## update the new kafka output metrics
    kafka_plugin_list[index_list[0]]['namepass'] = new_kafka_output_metrics

    configuration_dict['outputs']['prometheus_client'][0]['namepass'] = new_other_outputs_metrics
    configuration_dict['outputs']['influxdb_v2'][0]['namepass'] = new_other_outputs_metrics

    return configuration_dict


def change_topic_name(old_topic_name, new_topic_name):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])
    for element in configuration_dict['outputs']['kafka']:
        if element['topic'] == old_topic_name:
            element['topic'] == new_topic_name
    return configuration_dict


def delete_old_and_update_new_just_topic(old_topic_name, new_topic_name, new_kafka_output_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1
    kafka_plugin_list.pop(index_list[0])

    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==new_topic_name)]
    assert len(index_list) == 1
    kafka_plugin_list[index_list[0]]['namepass'] = new_kafka_output_metrics

    return configuration_dict


def update_old_and_create_new_just_topic(old_topic_name, new_topic_name, old_kafka_output_metrics_to_delete, new_kafka_output_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    old_kafka_output_metrics_to_delete = rename_metrics(old_kafka_output_metrics_to_delete)
    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1

    ## update the old kafka output metrics
    kafka_plugin_list[index_list[0]]['namepass'] = list(set(kafka_plugin_list[index_list[0]]['namepass']) - set(old_kafka_output_metrics_to_delete))

    kafka_plugin_list.append(create_kafka_output(new_kafka_output_metrics,new_topic_name))

    return configuration_dict


def update_old_and_update_new_just_topic(old_topic_name, new_topic_name, old_kafka_output_metrics_to_delete, new_kafka_output_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    old_kafka_output_metrics_to_delete = rename_metrics(old_kafka_output_metrics_to_delete)
    new_kafka_output_metrics = rename_metrics(new_kafka_output_metrics)
    
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==old_topic_name)]
    assert len(index_list) == 1

    ## update the old kafka output metrics
    kafka_plugin_list[index_list[0]]['namepass'] = list(set(kafka_plugin_list[index_list[0]]['namepass']) - set(old_kafka_output_metrics_to_delete))

    index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==new_topic_name)]
    assert len(index_list) == 1
    kafka_plugin_list[index_list[0]]['namepass'] = new_kafka_output_metrics

    return configuration_dict


def update_old_and_update_other_outputs(old_topic_name, new_old_kafka_output_metrics, new_other_outputs_metrics):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])

    new_old_kafka_output_metrics = rename_metrics(new_old_kafka_output_metrics)
    new_other_outputs_metrics = rename_metrics(new_other_outputs_metrics)
    
    for kafka_output in configuration_dict['outputs']['kafka']:
        if kafka_output['topic'] == old_topic_name:
            kafka_output['namepass'] = new_old_kafka_output_metrics
    
    configuration_dict['outputs']['prometheus_client'][0]['namepass'] = new_other_outputs_metrics
    configuration_dict['outputs']['influxdb_v2'][0]['namepass'] = new_other_outputs_metrics

    return configuration_dict


def update_single_datasource(new_topic_name, new_metrics):

    new_metrics = rename_metrics(new_metrics)

    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_ransim_conf_path'])
    configuration_dict['outputs']['kafka'][0]['topic'] = new_topic_name
    configuration_dict['outputs']['kafka'][0]['namepass'] = new_metrics
    configuration_dict['outputs']['prometheus_client'][0]['namepass'] = new_metrics
    configuration_dict['outputs']['influxdb_v2'][0]['namepass'] = new_metrics
    return configuration_dict