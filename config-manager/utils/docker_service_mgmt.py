'''
There is also a Python SDK for Docker, since the objective in this case is not
orchestrating the containers but just update the configuration and make them
restart with zero-downtime I decided to use the os module in order to 
invoke the "docker service update --force mp_telegraf_owm"
NOTE: here I am supposing that the swarm is ran using the string 'mp' as the stack name,
i.e. by running the command "docker stack deploy -c docker-compos.yml mp"
'''
import logging
import subprocess

def update_telegraf_owm_service():
    logging.info("Running update_telegraf_owm_service()")
    cmd = "docker service update --force mp_telegraf_owm"
    try:
        process = subprocess.Popen(['sh', '-c', cmd], stdout=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError as e:
        logging.error(e)
        return False

def update_telegraf_nwdaf_service():
    logging.info("Running update_telegraf_nwdaf_service()")
    cmd = "docker service update --force mp_telegraf_nwdaf"
    try:
        process = subprocess.Popen(['sh', '-c', cmd], stdout=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError as e:
        logging.error(e)
        return False

def update_telegraf_ransim_service():
    logging.info("Running update_telegraf_ransim_service()")
    cmd = "docker service update --force mp_telegraf_ransim"
    try:
        process = subprocess.Popen(['sh', '-c', cmd], stdout=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError as e:
        logging.error(e)
        return False

def update_telegraf_starfish_service():
    logging.info("Running update_telegraf_starfish_service()")
    cmd = "docker service update --force mp_telegraf_starfish"
    try:
        process = subprocess.Popen(['sh', '-c', cmd], stdout=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError as e:
        logging.error(e)
        return False