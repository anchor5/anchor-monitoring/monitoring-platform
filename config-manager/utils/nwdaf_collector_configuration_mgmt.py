'''
File to define functions useful to update the telegraf.conf file for the telegraf_nwdaf collector agent
The file is supposed to be placed at ./config/telegraf_nwdaf/telegraf.conf

The fields that can be configured for the nwdaf datasources are the city uid, the metrics and the topic name:
for each combination of requested metrics-topic_name there must be configured an nwdaf input plugin

The mapping between the nwdaf input plugin and the kafka output plugin is deduced from a tag 'map' added in the nwdaf plugin,
set equal to the datasource id of the datasource to be created or, if the topic name is custom and the kafka output publishing to the topic
is already present, equal to the map field already present.

All the nwdaf input plugin mapped to the topics with the predefined pattern for the topic name are mapped with 'map'='default',
all the custom named topics are mapped as stated above.

the kafka output with 'map'='default' MUST NEVER BE DELETED
'''
import logging
import toml
import uuid
from collections import OrderedDict

import utils.metric_list_mgmt as lsmgmt
import utils.configuration_mgmt as conf

def create_tagdrop_array(requested_metrics):
    nwdaf_metrics = ["rtt", "ul-volume", "dl-volume", "pdu-number", "pkt-loss"]
    tagdrop_list = set(nwdaf_metrics)-set(requested_metrics)
    tagdrop_array = []
    for element in tagdrop_list:
        if element=="rtt":
            tagdrop_array.append("RTT")
        elif element=="ul-volume":
            tagdrop_array.append("UL_VOL")
        elif element=="dl-volume":
            tagdrop_array.append("DL_VOL")
        elif element=="pdu-number":
            tagdrop_array.append("PDU_NUM")
        else:
            tagdrop_array.append("PKT_LOSS")
    return tagdrop_array

def create_http_conf(network_slice_subnet_uid,flow_id,api_root,interval,metrics,map_tag_value):
    logging.debug('NWDAF HTTP input creation')
    # url building taking as reference NITEL NWDAF implementation
    url = []
    if network_slice_subnet_uid:
        url.append('{}/analytics?event-id=NETWORK_ANALYTICS&nsiId={}'.format(api_root, network_slice_subnet_uid))
    else:
        url.append('{}/analytics?event-id=NETWORK_ANALYTICS'.format(api_root))
    excluded_metrics = create_tagdrop_array(metrics)
    return OrderedDict(
        {
            'tagexclude': ['host'],
            'urls': url,
            'method': 'GET',
            'data_format': 'json_v2',
            'interval': interval,
            'tags': {
                'map': map_tag_value
            },
            'json_v2': [
                {
                    'measurement_name': 'nwdaf', 
                    'object': [
                        {
                            'path': '@this', 
                            'disable_prepend_keys': True, 
                            'excluded_keys': ['timeStamp'], 
                            'tags': ['nsiId', 'measures_type', 'measures_unit']
                        }
                    ]
                }
            ],
            'tagdrop': { 
                'type': excluded_metrics
            }
        }
    )


def create_kafka_conf(topic_name,map_tag_value):
    logging.debug('Kafka output creation')
    if map_tag_value != 'default':
        return dict({
            'brokers': ['kafka:9092'],  
            'client_id': 'telegraf_nwdaf',
            'topic': topic_name,
            'tagpass': {'map': [map_tag_value]},
            'tagexclude': ['map']
            })
    else:
        return dict({
            'brokers': ['kafka:9092'],
            'client_id': 'telegraf_nwdaf',   
            'tagpass': {'map': ['default']},
            'topic': 'nwdaf',
            'tagexclude': ['map'],
            'topic_suffix': {
                'keys': ['end_to_end_network_slice_uid'],
                'method': 'tags',
                'separator': '_' }
            })

def check_slice_id_in_urls(urls,network_slice_subnet_uid):
    for element in urls:
        if(element.endswith("nsiId={}".format(network_slice_subnet_uid))):
            return True
    return False

def index_slice_id_in_urls(urls,network_slice_subnet_uid):
    url_index_list = [idx for idx, element in enumerate(urls) if (element.endswith("nsiId={}".format(network_slice_subnet_uid)))]
    assert(len(url_index_list)<=1)
    return url_index_list

def add_nwdaf_datasource_to_config(end_to_end_network_slice_uid, network_slice_subnet_uid, flow_id, api_root, interval, metrics, topic_name): 
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']
    kafka_plugin_list = configuration_dict['outputs']['kafka']
    if not kafka_plugin_list:
        logging.debug("kafka output list empty, processor enum mapping dict creation")
        configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings'] = {} ## dict creation

    # default pattern for topic name --> map = 'default'
    if not topic_name:
        logging.debug('Default pattern for topic_name')
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]=='default')]
        assert len(kafka_index_list) <= 1
        map_tag_value = 'default'
        
    # if the topic name is a customized one
    else:
        logging.debug('Custom pattern for topic_name')        
        # search if there is already a kafka output for the map tag value default
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        assert len(kafka_index_list) <= 1
    
        # if there is not the kafka output for rhis topic, new map tag creation, otherwise takes the one of the kafka output
        if not kafka_index_list:
            logging.debug('Kafka output not found, creating new map tag value')
            map_tag_value=str(uuid.uuid4())
            
        else:
            logging.debug('Found kafka output with same topic_name={} at index {}, using the same map tag value'.format(topic_name, kafka_index_list[0]))
            map_tag_value = kafka_plugin_list[kafka_index_list[0]]['tagpass']['map'][0] # tagpass['map'] is an array

    #if already output for topic name, look for an eventual input with same metric set in which insert the slice subnet uid
    if kafka_index_list:
        # search http index [unique, nullable] with this map value and same metrics

        http_index_list = [idx for idx, element in enumerate(http_plugin_list) if ((element['tags']['map']==map_tag_value) and (lsmgmt.check_list_equality_regardless_ordering(element['tagdrop']['type'],create_tagdrop_array(metrics))) and (element['interval']==interval) and lsmgmt.check_api_root_in_urls(element['urls'],api_root))]
        assert len(http_index_list) <= 1

        if http_index_list:
            logging.debug('HTTP index for insertion by map tag (and metrics) found: {}'.format(http_index_list[0]))

            urls = http_plugin_list[http_index_list[0]]['urls']
            if network_slice_subnet_uid:
                urls.append('{}/analytics?event-id=NETWORK_ANALYTICS&nsiId={}'.format(api_root, network_slice_subnet_uid))
            else:
                urls.append('{}/analytics?event-id=NETWORK_ANALYTICS'.format(api_root))
        else:
            logging.debug('HTTP input not found, adding one with custom map tag and the requested characteristics')
            http_plugin_list.append(create_http_conf(network_slice_subnet_uid,flow_id,api_root,interval,metrics,map_tag_value))

    # if there is not the kafka output there is not also the related nwdaf input --> create both
    else:
        logging.debug('Adding Kafka output')
        kafka_plugin_list.append(create_kafka_conf(topic_name,map_tag_value))
        logging.debug('Adding HTTP input')
        http_plugin_list.append(create_http_conf(network_slice_subnet_uid,flow_id,api_root,interval,metrics,map_tag_value))

    logging.debug('Adding the value mapping in processor plugin')
    processor_enum_mappings = configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']
    processor_enum_mappings["{}/analytics?event-id=NETWORK_ANALYTICS&nsiId={}".format(api_root,network_slice_subnet_uid)] = end_to_end_network_slice_uid

    return configuration_dict

## here we are assuming that the nwdaf datasource monitoring the slice_id exists (checked from the db)
## TODO: change with also the flow id as parameter when it will be supported by the NWDAF implementation
def delete_nwdaf_datasource_from_config(network_slice_subnet_uid):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']

    index_list = [idx for idx, element in enumerate(http_plugin_list) if (check_slice_id_in_urls(element['urls'],network_slice_subnet_uid))]
    assert len(index_list) == 1

    # no need to check the emptyness of the list (there MUST be at least one)
    assert len(http_plugin_list[index_list[0]]['urls']) >= 1
    if len(http_plugin_list[index_list[0]]['urls']) > 1:
        logging.debug('Other sliceIds present, no need to delete http input')
        slice_id_index_in_urls = index_slice_id_in_urls(http_plugin_list[index_list[0]]['urls'],network_slice_subnet_uid)
        ## eliminare quello con stesso slice_id
        http_plugin_list[index_list[0]]['urls'].pop(slice_id_index_in_urls[0])

    else: # just one slice_uid monitored --> delete the nwdaf input
        map_tag_value = http_plugin_list[index_list[0]]['tags']['map']
        http_plugin_list.pop(index_list[0]) 
        logging.debug("No other slice_id, http input with map tag = {} deleted".format(map_tag_value))

        # search the indices of the http inputs with same map_tag value (publishing in the same topic)
        http_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map']==map_tag_value)]

        if not http_indices_list:
            logging.debug("No other http input is using the sam map tag value, going to delete the related kafka output")
            # if no http input is using the same tag value, deletion of the kafka output related to it
            kafka_plugin_list = configuration_dict['outputs']['kafka']
            # search if there is already a kafka output for this topic name
            kafka_indices_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]==map_tag_value)]
            assert len(kafka_indices_list) == 1

            kafka_plugin_list.pop(kafka_indices_list[0])
            logging.debug("kafka output with index {} deleted".format(kafka_indices_list[0]))
    
    logging.debug("Deleting entries for in processor plugin")
    for key in list(configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings'].keys()):
        if key.endswith(network_slice_subnet_uid):
            del configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings'][key]
    
    return configuration_dict

def delete_all_nwdaf_datasources_from_config():
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'])
    configuration_dict['inputs']['http']=[]
    configuration_dict['outputs']['kafka']=[]
    configuration_dict['processors']['enum'][0]['mapping'][0]['value_mappings']={}
    return configuration_dict

# the update of an nwdaf datasource is done by deleting and re-adding it updated inside the configuration file (the functions above cannot be
# reused since they read the configuration file directly inside the body and since the eventually raised exception during the write of the
# file management is done in the nwdaf datasource logic file)
def update_nwdaf_datasource_from_config(end_to_end_network_slice_uid,network_slice_subnet_uid,flow_id,api_root,interval,metrics,topic_name):
    configuration_dict = toml.load(conf.configuration['PATHS']['telegraf_nwdaf_conf_path'])
    http_plugin_list = configuration_dict['inputs']['http']
    kafka_plugin_list = configuration_dict['outputs']['kafka']

    index_list = [idx for idx, element in enumerate(http_plugin_list) if (check_slice_id_in_urls(element['urls'],network_slice_subnet_uid))]
    assert len(index_list) == 1

    # no need to check the emptyness of the list (there MUST be at least one)
    assert len(http_plugin_list[index_list[0]]['urls']) >= 1
    if len(http_plugin_list[index_list[0]]['urls']) > 1:
        logging.debug('Other sliceIds present, no need to delete http input')
        slice_id_index_in_urls = index_slice_id_in_urls(http_plugin_list[index_list[0]]['urls'],network_slice_subnet_uid)
        ## eliminare quello con stesso slice_id
        http_plugin_list[index_list[0]]['urls'].pop(slice_id_index_in_urls[0])

    else: # just one city_uid monitored --> delete the nwdaf input
        map_tag_value = http_plugin_list[index_list[0]]['tags']['map']
        http_plugin_list.pop(index_list[0]) 
        logging.debug("No other slice_id, http input with map tag = {} deleted".format(map_tag_value))

        # search the indices of the http inputs with same map_tag value (publishing in the same topic)
        http_indices_list = [idx for idx, element in enumerate(http_plugin_list) if (element['tags']['map']==map_tag_value)]

        if not http_indices_list:
            logging.debug("No other http input is using the sam map tag value, going to delete the related kafka output")
            # if no http input is using the same tag value, deletion of the kafka output related to it
            # search if there is already a kafka output for this topic name
            kafka_indices_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]==map_tag_value)]
            assert len(kafka_indices_list) == 1

            kafka_plugin_list.pop(kafka_indices_list[0])
            logging.debug("kafka output with index {} deleted".format(kafka_indices_list[0]))
    
    #### Until here, datasource deletion, now the updated one must be added to the dict
    
    # default pattern for topic name --> map = 'default'
    if not topic_name:
        logging.debug('Default pattern for topic_name')
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['tagpass']['map'][0]=='default')]
        assert len(kafka_index_list) <= 1
        map_tag_value = 'default'
        
    # if the topic name is a customized one
    else:
        logging.debug('Custom pattern for topic_name')
        logging.debug("topic name = {}".format(topic_name))
        # search if there is already a kafka output for the map tag value default
        kafka_index_list = [idx for idx, element in enumerate(kafka_plugin_list) if (element['topic']==topic_name)]
        assert len(kafka_index_list) <= 1
    
        # if there is not the kafka output for rhis topic, new map tag creation, otherwise takes the one of the kafka output
        if not kafka_index_list:
            logging.debug('Kafka output not found, creating new map tag value')
            map_tag_value=str(uuid.uuid4())

        else:
            logging.debug('Found kafka output with same topic_name={} at index {}, using the same map tag value'.format(topic_name, kafka_index_list[0]))
            map_tag_value = kafka_plugin_list[kafka_index_list[0]]['tagpass']['map'][0] # tagpass['map'] is an array

    #if already output for topic name, look for an eventual http input with same api_root,interval,metrics set in which insert the slice_id
    if kafka_index_list:
        # search nwdaf index [unique, nullable] with this map value and same metrics
        http_index_list = [idx for idx, element in enumerate(http_plugin_list) if ((element['tags']['map']==map_tag_value) and (lsmgmt.check_list_equality_regardless_ordering(element['tagdrop']['type'],create_tagdrop_array(metrics))) and (element['interval']==interval) and lsmgmt.check_api_root_in_urls(element['urls'],api_root))]
        assert len(http_index_list) <= 1

        if http_index_list:
            logging.debug('HTTP index for insertion by map tag,api_root,interval and metrics found: {}'.format(http_index_list[0]))
            if network_slice_subnet_uid:
                http_plugin_list[http_index_list[0]]['urls'].append('{}/analytics?event-id=NETWORK_ANALYTICS&nsiId={}'.format(api_root, network_slice_subnet_uid))
            else:
                http_plugin_list[http_index_list[0]]['urls'].append('{}/analytics?event-id=NETWORK_ANALYTICS'.format(api_root))
        else:
            logging.debug('HTTP input not found, adding one with custom map tag and the requested characteristics')
            http_plugin_list.append(create_http_conf(network_slice_subnet_uid,flow_id,api_root,interval,metrics,map_tag_value))

    # if there is not the kafka output there is not also the related http input --> create both
    else:
        logging.debug('Adding Kafka output')
        kafka_plugin_list.append(create_kafka_conf(topic_name,map_tag_value))
        logging.debug('Adding HTTP input')
        http_plugin_list.append(create_http_conf(network_slice_subnet_uid,flow_id,api_root,interval,metrics,map_tag_value))
    
    return configuration_dict
