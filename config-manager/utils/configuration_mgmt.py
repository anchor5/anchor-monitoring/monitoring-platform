import toml
import logging

def init_configuration():
    global configuration
    try:
        configuration = toml.load("configuration/config_manager.conf")

    except Exception as e:
        logging.error("Error reading the configuration file, occurred exception: {}".format(e))

